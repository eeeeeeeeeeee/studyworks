﻿using Newtonsoft.Json;

namespace GraphBuilder
{
    public class Graph
    {
        public class Edge
        {
            public Edge(int i0, int i1)
            {
                V0 = i0;
                V1 = i1;
            }
            [JsonProperty("V0")]
            public int V0 { get; set; }
            
            [JsonProperty("V1")]
            public int V1 { get; set; }
        }

        [JsonProperty("Edges")]
        public Edge[] Edges { get; set; }
        
        [JsonProperty("Cycle")]
        public int[] Cycle { get; set; }
    }
}