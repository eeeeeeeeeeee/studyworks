﻿using System;

using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

namespace GraphBuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = 10;
            Int32.TryParse(Console.ReadLine(), out n);

            if (n > 1001) n = 1000;
            if (n < 10) n = 10;
            
            List<int> vertex = new List<int>();
            for (int i = 0; i < n; i++)
            {
                vertex.Add(i);
            }

            Random r = new Random();
            for (int i = 0; i < n; i++)
            {
                int t = vertex[i];
                int j = r.Next(n);
                vertex[i] = vertex[j];
                vertex[j] = t;
            }
            
            vertex.Add(vertex[0]);

            int[][] a = new int[n][];
            for (int i = 0; i < n; i++)
            {
                a[i] = new int[n];
            }

            List<Graph.Edge> edges = new List<Graph.Edge>();
            for (int i = 0; i < n; i++)
            {
                edges.Add(new Graph.Edge(vertex[i], vertex[i+1]));
                a[vertex[i]][vertex[i+1]] = 1;
                List<int> repeats = new List<int>();
                repeats.Add(vertex[i+1]);
                for (int j = 0; j < n/2; j++)
                {
                    int k = r.Next(n);
                    if (repeats.Contains(vertex[k])) continue; 
                    repeats.Add(vertex[k]);
                    edges.Add(new Graph.Edge(vertex[i], vertex[k]));
                }
            }

            Graph g = new Graph();
            g.Edges = edges.OrderBy(edge => edge.V0).ToArray();
            g.Cycle = vertex.ToArray();

            string str = JsonConvert.SerializeObject(g);
            FileStream f = File.Open("c.json", FileMode.Create);
            StreamWriter streamWriter = new StreamWriter(f);
            streamWriter.Write(str);
            streamWriter.Close();
            f.Close();

        }
    }
}