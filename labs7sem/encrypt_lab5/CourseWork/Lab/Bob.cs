﻿using System;
using System.Collections.Generic;
using GraphBuilder;

namespace Lab
{
    public static class Bob
    {
        public static bool CheckAnswer1(int[] cycle, int[][] f, int[][] r, int[][] sourcef)
        {
            HashSet<int> chked = new HashSet<int>();
            int n = cycle.Length - 1;
            for (int i = 0; i < n; i++)
            {
                if (f[cycle[i]][cycle[i + 1]] != 1) return false;
                if ((r[cycle[i]][cycle[i + 1]] << 1) + f[cycle[i]][cycle[i + 1]] != sourcef[cycle[i]][cycle[i + 1]])
                    return false;
                chked.Add(cycle[i + 1]);
            }

            if (chked.Count != n) return false;
            return true;
        }

        public static bool CheckAnswer2(Graph g, Graph h, int[] repl)
        {
            int n = repl.Length;
            int[] vertex = new int[n + 1];

            for (int i = 0; i < n; i++)
            {
                vertex[repl[i]] = i;
            }

            List<Graph.Edge> fEdges = new List<Graph.Edge>(); 
            foreach (Graph.Edge edge in h.Edges)
            {
               fEdges.Add(new Graph.Edge(vertex[edge.V0], vertex[edge.V1]));
            }

            for (int i = 0; i < fEdges.Count; i++)
            {
                Graph.Edge edge1 = g.Edges[i];
                Graph.Edge edge2 = fEdges[i];

                if (edge1.V0 != edge2.V0 || edge2.V1 != edge1.V1) return false;
            }
            
            /*for (int i = 0; i < n; i++)
            {
                Console.WriteLine(repl[i] + " = " + vertex[i]);
            }*/
            
            return true;
        }
    }
}