﻿using System;
using System.Collections.Generic;
using System.IO;
using GraphBuilder;
using Newtonsoft.Json;

namespace Lab
{
    class Program
    {
        static void Main(string[] args)
        {
            FileStream fileStream = File.OpenRead("c.json");
            StreamReader streamReader = new StreamReader(fileStream);
            string str = streamReader.ReadToEnd();
            streamReader.Close();
            fileStream.Close();
            
            // Read G
            Graph g = JsonConvert.DeserializeObject<Graph>(str);

            int t = 1;
            Int32.TryParse(Console.ReadLine(), out t);
            if (t < 1) t = 1;

            for (int i = 0; i < t; i++)
            {
                int[][] answ1;
                int[][] f;
                int[][] r;
                int[] path;
                Alisa.PrepareAnswer1(g, out answ1, out path, out r, out f);
                bool bob1 = Bob.CheckAnswer1(path, answ1, r, f);

                Graph h;
                int[] replc;
                Alisa.PrepareAnswer2(g, out h, out replc);
                bool bob2 = Bob.CheckAnswer2(g, h, replc);
                
                Console.WriteLine("\nBob answer["+(i+1)+"]: " + (bob1&&bob2));
                if(i>0) continue;
                
                WriteAnswer1(answ1, path);
                Console.WriteLine("Bob answer 1: "+bob1);
                
                WriteAnswer2(replc);
                Console.WriteLine("Bob answer 2: "+bob2);
            }

            static void WriteAnswer2(int[] replc)
            {
                string a1 = "\nAlise send Answer 2\nReplaces: ";
                for (int j = 0; j < 5 && j < replc.Length; j++)
                {
                    a1 += j+"="+replc[j] + " ";
                }
                a1 += " ... ";
                
                for (int j = replc.Length-5; j > 5 && j < replc.Length; j++)
                {
                    a1 += j+"="+replc[j] + " ";
                }
                a1 += "\n";
                Console.WriteLine(a1);
            }
            
            static void WriteAnswer1(int[][] answ, int[] path)
            {
                string a1 = "\nAlise send Answer 1\nPath: ";
                for (int j = 0; j < 5 && j < path.Length; j++)
                {
                    a1 += path[j] + " ";
                }
                a1 += " ... ";
                
                for (int j = path.Length-5; j > 5 && j < path.Length; j++)
                {
                    a1 += path[j] + " ";
                }

                a1 += "\nMatrix (rnd[i,j] || h[i,j]):\n";
            
                for (int j = 0; j < 5 && j < answ.Length; j++)
                {
                    a1 += answ[0][j] + " ";
                }
                a1 += " ... ";
                
                for (int j = path.Length-5; j > 5 && j < answ.Length; j++)
                {
                    a1 += answ[0][j] + " ";
                }
                a1 += "\n...\n";
                for (int j = 0; j < 5 && j < answ.Length; j++)
                {
                    a1 += answ[^1][j] + " ";
                }
                a1 += " ... ";
                
                for (int j = path.Length-5; j > 5 && j < answ.Length; j++)
                {
                    a1 += answ[^1][j] + " ";
                }

                a1 += "\n";
            
                Console.WriteLine(a1);
            }
        }
    }
}