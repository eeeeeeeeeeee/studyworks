﻿using System;
using System.Collections.Generic;
using GraphBuilder;
using Microsoft.VisualBasic.CompilerServices;

namespace Lab
{
    public static class Alisa
    {
        public static void MakeH(Graph g, out Graph h, out int[] replaces)
        {
            int n = g.Edges[^1].V0 + 1;
            
            List<int> vertex = new List<int>();
            for (int i = 0; i < n; i++)
            {
                vertex.Add(i);
            }
            
            h = new Graph();
            
            Random r = new Random();
            for (int i = 0; i < n; i++)
            {
                int t = vertex[i];
                int j = r.Next(n);
                vertex[i] = vertex[j];
                vertex[j] = t;
            }

            List<Graph.Edge> hEdges = new List<Graph.Edge>();
            foreach (Graph.Edge e in g.Edges)
            {
                hEdges.Add(new Graph.Edge(vertex[e.V0],vertex[e.V1]));
            }
            
            h.Edges = hEdges.ToArray();

            int[] hCycle = new int[n+1];
            for (int i = 0; i < n + 1; i++)
            {
                hCycle[i] = vertex[g.Cycle[i]];
            }

            h.Cycle = hCycle;
            replaces = vertex.ToArray();
        }

        public static void MakeF(Graph g, int n, out int[][] h, out int[][] r)
        {
            h = new int[n][];
            r = new int[n][];
            for (int i = 0; i < n; i++)
            {
                h[i] = new int[n];
                r[i] = new int[n];
            }

            foreach (Graph.Edge e in g.Edges)
            {
                h[e.V0][e.V1] = 1;
            }
            
            Random rnd = new Random();
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    r[i][j] = rnd.Next(10);
                    h[i][j] = (r[i][j] << 1) + h[i][j];
                }
            }
        }

        public static void PrepareAnswer1(Graph g, out int[][] res, out int[] cycle, out int[][] r, out int[][] f)
        {
            int n = g.Edges[^1].V0 + 1;
            
            // Make H
            Graph h;
            int[] replc;
            MakeH(g, out h, out replc);
            
            // Make F 
            MakeF(h, n, out f, out r);
            
            // Copy F
            res = new int[n][];
            for (int i = 0; i < n; i++)
            {
                res[i] = new int[n];
            }

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    res[i][j] = f[i][j];
                }
            }

            // Make F with decrypt path
            cycle = h.Cycle;
            for (int i = 0; i < n; i++)
            {
                res[cycle[i]][cycle[i + 1]] &= 1;
            }
        }

        public static void PrepareAnswer2(Graph g, out Graph h, out int[] replc)
        {
            int n = g.Edges[^1].V0 + 1;
            
            // Make H
            MakeH(g, out h, out replc);
        }
    }
}