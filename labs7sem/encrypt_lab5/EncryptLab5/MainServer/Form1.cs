﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Numerics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MainServer
{
    public partial class Form1 : Form
    {
        private Thread _thread;
        private List<BigInteger> _s = new List<BigInteger>();
        public Form1()
        {
            InitializeComponent();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            if(Server.server != null)
            Server.server.Stop();
        }

        private void GetKeys()
        {
            try
            {
                TcpClient tcpClient = new TcpClient();
                tcpClient.Connect("127.0.0.1", 8002);
                NetworkStream stream = tcpClient.GetStream();
                
                byte[] buf = new byte[1024];
                stream.Read(buf);
                Config.N = new BigInteger(buf, true);
                
                stream.Write(buf);
                
                buf = new byte[1024];
                stream.Read(buf);
                Config.d = new BigInteger(buf, true);
                
                stream.Close();
                tcpClient.Close();

                label4.Text = "Ready";
                Config.ready = true;
                button1.Enabled = false;

                Console.WriteLine(Config.N);
                Console.WriteLine(Config.d);
                
                _thread = new Thread(Server.Process);
                _thread.Start(this);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private string IncString(string str)
        {
            int v = 0;
            if (str != "")
            {
                v = Int32.Parse(str);
            }

            v += 1;
            return v.ToString();
        }
        
        public void AddVote(int v, BigInteger s)
        {
            if (_s.Contains(s)) return;
            _s.Add(s);
            
            if (v == 1)
            {
                textBox1.Text = IncString(textBox1.Text);
            } else if (v == 2)
            {
                textBox2.Text = IncString(textBox2.Text);
            } else if (v == 4)
            {
                textBox3.Text = IncString(textBox3.Text);
            }
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            GetKeys();
        }
    }
}