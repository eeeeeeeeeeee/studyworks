﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Numerics;
using System.Security.Cryptography;
using System.Windows.Forms;
using Encrypt;

namespace MainServer
{
    public class Server
    {
        public static TcpListener server;
        public static void Process(object form)
        {
            Form1 form1 = (Form1) form;
            
            IPAddress localAddr = IPAddress.Parse("127.0.0.1");
            int port = 8003;
            server = new TcpListener(localAddr, port);
            server.Start();

            while (true)
            {
                TcpClient client;
                try
                {
                    client = server.AcceptTcpClient();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    return;
                }
                NetworkStream stream = client.GetStream();
                
                // Получение n
                byte[] buf = new byte[2048];
                stream.Read(buf);
                BigInteger n = new BigInteger(buf, true);
                
                // Отправка ответа (костыль крч)
                stream.Write(BigInteger.One.ToByteArray());
                
                // Получение S
                buf = new byte[1024];
                stream.Read(buf);
                BigInteger s = new BigInteger(buf, true);

                // Добавление голоса с учетом корректности подписи и отсутствия ее в БД
                SHA256 sha256 = SHA256.Create();
                BigInteger shan = new BigInteger(sha256.ComputeHash(n.ToByteArray()), true);
                BigInteger sdn = MathUtils.PowMod(s, Config.d, Config.N);

                if (shan == sdn)
                {
                    form1.AddVote((int)(n&0xff), s);
                }
    
                stream.Close();
                client.Close();
            }
        }
    }
}