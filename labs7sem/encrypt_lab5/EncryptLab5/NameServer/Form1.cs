﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NameServer
{
    public partial class Form1 : Form
    {
        private Thread _namesThread, _confThread;
        private List<string> _names = new List<string>();
        public Form1()
        {
            InitializeComponent();
            _namesThread = new Thread(VoteServer.Process);
            _namesThread.Start(this);
            
            _confThread = new Thread(ConfigServer.Process);
            _confThread.Start();
        }

        public void ShowMessage(string str)
        {
            label2.Text = str;
        }

        public bool AddName(string name)
        {
            if (_names.Contains(name))
            {
                return true;
            }
            _names.Add(name);
            listBox1.Items.Add(name);
            return false;
        }
        
        protected override void OnClosing(CancelEventArgs e)
        {
            VoteServer.server.Stop();
            ConfigServer.server.Stop();
        }
    }
}