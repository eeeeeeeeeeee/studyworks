﻿using System;
using System.Net;
using System.Net.Sockets;

namespace NameServer
{
    public class ConfigServer
    {
        public static TcpListener server;
        public static void Process()
        {
            IPAddress localAddr = IPAddress.Parse("127.0.0.1");
            int port = 8002;
            server = new TcpListener(localAddr, port);
            server.Start();

            // Ожидание параметров
            while (VoteServer.n < 1 && VoteServer.d < 1);
            
            while (true)
            {
                TcpClient client;
                try
                {
                    client = server.AcceptTcpClient();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    return;
                }
                NetworkStream stream = client.GetStream();
                
                stream.Write(VoteServer.n.ToByteArray());
                
                // Получение ответа
                byte[] buf = new byte[1024];
                stream.Read(buf);
                
                stream.Write(VoteServer.d.ToByteArray());

                stream.Close();
                client.Close();
            }
        }
    }
}