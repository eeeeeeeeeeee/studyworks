﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Numerics;
using System.Text;
using System.Windows.Forms;
using Encrypt;

namespace NameServer
{
    public class VoteServer
    {
        public static BigInteger p, q, d, c, n, f;
        public static TcpListener server;
        public static void Process(object o)
        {
            Form1 form1 = (Form1) o;
            
            GenerateRSA(256);

            form1.ShowMessage("Ready");
            
            IPAddress localAddr = IPAddress.Parse("127.0.0.1");
            int port = 8001;
            server = new TcpListener(localAddr, port);
            server.Start();

            while (true)
            {
                TcpClient client;
                try
                {
                    client = server.AcceptTcpClient();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    return;
                }
                NetworkStream stream = client.GetStream();
                
                // Получение h1
                byte[] buf = new byte[1024];
                stream.Read(buf);
                BigInteger h1 = new BigInteger(buf, true);
                
                // Отправка ответа о получении
                stream.Write(buf);
                
                // Получение имени
                buf = new byte[1024];
                stream.Read(buf);

                if (form1.AddName(Encoding.UTF8.GetString(buf)))
                {
                    // Отправка отрицательного ответа
                    stream.Write(BigInteger.One.ToByteArray());
                    stream.Close();
                    client.Close();
                    continue;
                }
                
                // Отправка s1
                BigInteger s1 = MathUtils.PowMod(h1, c, n);
                stream.Write(s1.ToByteArray());
                
                stream.Close();
                client.Close();
            }
        }

        public static void GenerateRSA(int of2)
        {
            p = Randomizer.GenerateP(Randomizer.RandomBigInteger(of2));
            q = Randomizer.GenerateP(Randomizer.RandomBigInteger(of2));
            n = p * q;
            
            f = (p - 1) * (q - 1);
            d = Randomizer.RandomBigInteger(of2)%f;
            while (Utility.Euclid(d,f)[0]!=1)
            {
                d += 1;
            }

            c = MathUtils.Inverse(d, f);
        }
    }
}