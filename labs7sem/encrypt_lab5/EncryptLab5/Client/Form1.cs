﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Numerics;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Encrypt;

namespace Client
{
    public partial class Form1 : Form
    {
        private BigInteger N, d;
        public Form1()
        {
            InitializeComponent();
        }

        public void GetKeys()
        {
            try
            {
                TcpClient tcpClient = new TcpClient();
                tcpClient.Connect("127.0.0.1", 8002);
                NetworkStream stream = tcpClient.GetStream();
                
                byte[] buf = new byte[1024];
                stream.Read(buf);
                N = new BigInteger(buf, true);
                
                stream.Write(buf);
                
                buf = new byte[1024];
                stream.Read(buf);
                d = new BigInteger(buf, true);
                
                stream.Close();
                tcpClient.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Trim().Length < 1)
            {
                MessageBox.Show("Введите нормальное имя", "Ошибка");
                return;
            }

            BigInteger rnd = Randomizer.RandomBigInteger(250) + ((BigInteger) 1) << 256;
            BigInteger v = 1;
            if (radioButton2.Checked) v = 2;
            if (radioButton3.Checked) v = 4;

            BigInteger n = rnd;
            int i = 0;

            while (v >> i > 0)
            {
                n <<= 1;
                i++;
            }

            n += v;

            BigInteger s;
            
            // Получение открытых ключей
            GetKeys();
            
            try
            {
                // Обращение к серверу имен
                TcpClient tcpClient = new TcpClient();
                tcpClient.Connect("127.0.0.1", 8001);
                NetworkStream stream = tcpClient.GetStream();

                // Вычисление h1
                BigInteger r = Randomizer.Coprime(250, N);
                SHA256 sha256 = SHA256.Create();
                BigInteger h = new BigInteger(sha256.ComputeHash(n.ToByteArray()), true);
                BigInteger h1 = h % N * MathUtils.PowMod(r, d, N) % N;
                
                // Отправка h1
                stream.Write(h1.ToByteArray());
                
                // Ожидание отчета о получении
                byte[] buf = new byte[1024];
                stream.Read(buf);
                
                // Отправка имени
                stream.Write(Encoding.UTF8.GetBytes(textBox1.Text));
                
                // Ожидание S1
                buf = new byte[1024];
                stream.Read(buf);
                BigInteger s1 = new BigInteger(buf, true);
                
                if (s1 == BigInteger.One)
                {
                    MessageBox.Show("Вы уже голосовали!", "Ошибка");
                    stream.Close();
                    tcpClient.Close();
                    return;
                }

                stream.Close();
                tcpClient.Close();

                BigInteger r1 = MathUtils.Inverse(r, N);
                s = s1 % N * (r1 % N) % N;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка доступа к серверу имен");
                return;
            }

            // Отправка голоса
            try
            {
                TcpClient tcpClient = new TcpClient();
                tcpClient.Connect("127.0.0.1", 8003);
                NetworkStream stream = tcpClient.GetStream();

                Console.WriteLine(s);
                
                stream.Write(n.ToByteArray());
                byte[] buf = new byte[1024];
                stream.Read(buf);
                stream.Write(s.ToByteArray());
                
                stream.Close();
                tcpClient.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка при отправке голоса");
                return;
            }
            
        }
        
    }
}