using NUnit.Framework;
using WinFormsApp1;

namespace TestProject1
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            AbonentList abonentList = new AbonentList();
            abonentList.Add("Vasya", "0");
            Assert.AreEqual("Vasya", abonentList.GetNames()[0]);
            Assert.AreEqual("0", abonentList.GetByName("Vasya")[0]);
            abonentList.Add("Vova", "2");
            abonentList.Add("Vasya", "3");
            Assert.AreEqual(2, abonentList.GetByName("Vasya").Count);
            Assert.AreEqual(1, abonentList.GetByName("Vova").Count);
            abonentList.Clear();
            Assert.AreEqual(0, abonentList.GetNames().Count);
        }
    }
}