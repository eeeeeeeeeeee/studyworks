﻿using System;
using System.Collections.Generic;

namespace WinFormsApp1
{
    public class AbonentList
    {
        SortedDictionary<string, List<string>> _dic = new SortedDictionary<string,List<string>>();

        public void Add(string name, string phone)
        {
            if(_dic.ContainsKey(name)) {
                _dic[name].Add(phone);
            }
            else
            {
                _dic.Add(name, new List<string>());
                _dic[name].Add(phone);
            }
        }

        public List<string> GetByName(string name)
        {
            if (_dic.ContainsKey(name)) return _dic[name];
            return null;
        }

        public void Remove(string name, string phone)
        {
            if (_dic.ContainsKey(name))
            {
                if (_dic[name].Contains(phone))
                    _dic[name].Remove(phone);
                if (_dic[name].Count < 1)
                {
                    _dic.Remove(name);
                }
            }
        }

        public void Remove(string name)
        {
            if (_dic.ContainsKey(name))
            {
                _dic.Remove(name);
            }
        }

        public void ChangeName(string a, string b)
        {
            if (!_dic.ContainsKey(a)) return;
            if (_dic.ContainsKey(b))
            {
                _dic[b].AddRange(_dic[a]);
                _dic.Remove(a);
            }
            else
            {
                _dic.Add(b, _dic[a]);
                _dic.Remove(a);
            }
        }
        
        public void ChangePhone(string name, string a, string b)
        {
            if (!_dic.ContainsKey(name)) return;
            if (_dic[name].Contains(a))
            {
                _dic[name][_dic[name].IndexOf(a)] = b;
            }
        }

        public List<string> GetNames()
        {
            List<string> names = new List<string>();
            foreach (var k in _dic.Keys)
            {
                names.Add(k);
            }

            return names;
        }

        public void Clear()
        {
            _dic.Clear();
        }
    }
}