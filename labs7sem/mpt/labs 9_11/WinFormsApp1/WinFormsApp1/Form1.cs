﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsApp1
{
    public partial class Form1 : Form
    {
        private AbonentList _abonentList = new AbonentList();

        private void UpdateLists()
        {
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            foreach (var n in _abonentList.GetNames())
            {
                listBox1.Items.Add(n);
            }
        }
        
        private void UpdateLists(string name)
        {
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            foreach (var n in _abonentList.GetNames())
            {
                if (n == name)
                {
                    listBox1.Items.Add(n);
                    break;
                }
            }
        }

        private void UpdateListByName(string name)
        {
            listBox2.Items.Clear();
            List<string> p = _abonentList.GetByName(name);
            if(p == null) return;
            foreach (var n in p)
            {
                listBox2.Items.Add(n);
            }
        }
        
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _abonentList.Clear();
            UpdateLists();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex < 0) return;
            if (listBox2.SelectedIndex < 0)
            {
                _abonentList.Remove(listBox1.Items[listBox1.SelectedIndex].ToString());
                UpdateLists();
                return;
            }
            _abonentList.Remove(listBox1.Items[listBox1.SelectedIndex].ToString(),
                listBox2.Items[listBox2.SelectedIndex].ToString());
            UpdateListByName(listBox1.Items[listBox1.SelectedIndex].ToString());
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Trim().Length < 1 || textBox2.Text.Trim().Length < 1)
            {
                MessageBox.Show("Одно из полей не заполнено");
                return;
            }
            _abonentList.Add(textBox1.Text.Trim(), textBox2.Text.Trim());
            UpdateLists();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex < 0) return;
            UpdateListByName(listBox1.Items[listBox1.SelectedIndex].ToString());
            textBox1.Text = listBox1.Items[listBox1.SelectedIndex].ToString();
            textBox2.Text = "";
        }

        private void label2_Click(object sender, EventArgs e)
        {
            
        }

        private void button7_Click(object sender, EventArgs e)
        {
            UpdateLists(textBox3.Text.Trim());
        }

        private void button5_Click(object sender, EventArgs e)
        {
            UpdateLists();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex < 0) return;
            if (listBox2.SelectedIndex < 0)
            {
                _abonentList.ChangeName(listBox1.Items[listBox1.SelectedIndex].ToString(), textBox1.Text.Trim());
                UpdateLists();
                return;
            }
            _abonentList.ChangePhone(listBox1.Items[listBox1.SelectedIndex].ToString(),
                listBox2.Items[listBox2.SelectedIndex].ToString(), textBox2.Text.Trim());
            UpdateListByName(listBox1.Items[listBox1.SelectedIndex].ToString());
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox2.SelectedIndex < 0) return;
            textBox2.Text = listBox2.Items[listBox2.SelectedIndex].ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FileStream fileStream = File.OpenWrite("data.txt");
            StreamWriter streamWriter = new StreamWriter(fileStream);
            foreach (var n in _abonentList.GetNames())
            {
                streamWriter.WriteLine("["+n+"]");
                foreach (var p in _abonentList.GetByName(n))
                {
                    streamWriter.WriteLine(p);
                }
                streamWriter.WriteLine();
            }
            streamWriter.Close();
            fileStream.Close();

            MessageBox.Show("Сохранено");
        }
    }
}