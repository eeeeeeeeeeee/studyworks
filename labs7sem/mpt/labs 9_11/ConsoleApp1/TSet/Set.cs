﻿using System;
using System.Collections.Generic;

namespace TSet
{
    public class Set<T>
    {
        private HashSet<T> _s;

        public int Count => _s.Count;
        
        public T this[int i] {
            get
            {
                int j = 0;
                foreach (T e in _s)
                {
                    if (i == j) return e;
                    j++;
                }

                throw new Exception();
            }
        }
        

        public Set()
        {
            _s = new HashSet<T>();
        }

        public void Add(T e)
        {
            _s.Add(e);
        }

        public void Remove(T e)
        {
            _s.Remove(e);
        }

        public void Clear()
        {
            _s.Clear();
        }

        public bool IsEmpty()
        {
            return (_s.Count < 1);
        }

        public bool Contains(T e)
        {
            return _s.Contains(e);
        }

        public static Set<T> operator |(Set<T> s, Set<T> s1)
        {
            Set<T> s2 = new Set<T>();
            foreach (T o in s._s)
            {
                s2.Add(o);
            }
            foreach (T o in s1._s)
            {
                s2.Add(o);
            }

            return s2;
        }
        
        public static Set<T> operator -(Set<T> s, Set<T> s1)
        {
            Set<T> s2 = new Set<T>();
            foreach (T o in s._s)
            {
                s2.Add(o);
            }
            foreach (T o in s1._s)
            {
                if(s2.Contains(o)) s2.Remove(o);
            }
            return s2;
        }
        
        public static Set<T> operator &(Set<T> s, Set<T> s1)
        {
            Set<T> s2 = new Set<T>();
            foreach (T o in s._s)
            {
                if(s1.Contains(o)) s2.Add(o);
            }
            return s2;
        }
    }
}