using ConsoleApp1;
using NUnit.Framework;

namespace TestProject1
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void MemberTest()
        {
            Member m = new Member(2, 1);
            Member m1 = new Member(2, 1);
            Assert.AreEqual(true, m == m1);
            
            Member m2 = new Member(2, 0);
            Assert.AreEqual(true , m2 == m.Diff());
            
            Assert.AreEqual("2 * x^0" , m2.ToString());
            
            Assert.AreEqual( 3*2, m.Value(3));
        }
        
        [Test]
        public void PolyTest()
        {
            Polynome p0 = new Polynome(2, 1);
            Polynome p1 = new Polynome(2, 1);
            Assert.AreEqual(true, p0 == p1);
            Polynome p2 = p0 * p1;
            Assert.AreEqual(true, p0 == p1);
            Assert.AreEqual(false, p2 == p1);
            Assert.AreEqual(1, p2.Count);
            p2 += p1;
            Assert.AreEqual(2, p2.GetDegree());
            Assert.AreEqual(2, p2.Count);
            Assert.AreEqual(2, p2.GetFactor(1));
            Assert.AreEqual(4, p2.GetFactor(2));
            Polynome p3 = p2 * p2;
            Assert.AreEqual(3, p3.Count);
            Assert.AreEqual(16, p3.GetFactor(4));
            p3.Clear();
            Assert.AreEqual(0, p3.Count);
            Assert.AreEqual(20, p2.Value(2));
        }
    }
}