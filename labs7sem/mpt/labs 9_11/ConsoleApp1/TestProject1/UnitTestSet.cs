﻿using System.Runtime.Intrinsics.X86;
using ConsoleApp1;
using NUnit.Framework;
using TSet;

namespace TestProject1
{
    public class TestsSet
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void IntTest()
        {
            Set<int> s = new Set<int>();
            Assert.AreEqual(true, s.IsEmpty());
            
            s.Add(322);
            Assert.AreEqual(false, s.IsEmpty());
            
            s.Add(322);
            Assert.AreEqual(1, s.Count);
            
            s.Add(228);
            Assert.AreEqual(2, s.Count);
            
            s.Add(1337);
            
            Set<int> s1 = new Set<int>();
            s1.Add(228);
            s1.Add(229);
            s1.Add(230);
            
            Assert.AreEqual(3, s1.Count);
            Assert.AreEqual(322, s[0]);

            Set<int> s2 = s | s1;
            Assert.AreEqual(5, s2.Count);
            
            s2 = s & s1;
            Assert.AreEqual(1, s2.Count);
            
            s2 = s - s1;
            Assert.AreEqual(2, s2.Count);
        }
    }
}