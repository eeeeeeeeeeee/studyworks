﻿using System;
using System.Text;

namespace ConsoleApp1
{
    public class Member
    {
        private int _factor, _deg;

        public int Factor
        {
            get { return _factor; }
            set { _factor = value; }
        }
        
        public int Degree
        {
            get { return _deg; }
            set { _deg = value; }
        }

        public Member(int factor, int deg)
        {
            _factor = factor;
            _deg = deg;
        }

        public static bool operator ==(Member a, Member b)
        {
            return a._factor == b._factor && a._deg == b._deg;
        }
        
        public static bool operator <(Member a, Member b)
        {
            return a._deg < b._deg;
        }

        public static bool operator >(Member a, Member b)
        {
            return a._deg > b._deg;
        }

        public static bool operator !=(Member a, Member b)
        {
            return !(a == b);
        }

        public Member Diff()
        {
            return new Member(_deg*_factor, _deg-1);
        }

        public double Value(int x)
        {
            return Math.Pow(x, _deg) * _factor;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(_factor);
            sb.Append(" * x^");
            sb.Append(_deg);
            return sb.ToString();
        }
    }
}