﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp1
{
    public class Polynome
    {
        private List<Member> _members;

        public Polynome(int a, int c)
        {
            _members = new List<Member>();
            _members.Add(new Member(a, c));
        }

        public int GetDegree()
        {
            int max = 0;
            foreach (var m in _members)
            {
                if (m.Factor != 0 && m.Degree > max) max = m.Degree;
            }

            return max;
        }

        public int GetFactor(int n)
        {
            foreach (var m in _members)
            {
                if (m.Degree == n)
                {
                    return m.Factor;
                }
            }

            return 0;
        }

        public double Value(int x)
        {
            double value = 0;
            foreach (var m in _members)
            {
                value += m.Value(x);
            }

            return value;
        }

        public void Diff()
        {
            List<Member> nList = new List<Member>();
            foreach (var m in _members)
            {
                nList.Add(m.Diff());
            }

            _members = nList;
        }
        
        public void Clear()
        {
            _members.Clear();
        }

        public Member this[int i] => _members[i];

        public int Count => _members.Count;
        
        public static Polynome operator+(Polynome a, Polynome b)
        {
            Polynome nPolynome = new Polynome(0,0);
            nPolynome.Clear();
            foreach (var m in a._members)
            {
                nPolynome._members.Add(new Member(m.Factor+b.GetFactor(m.Degree), m.Degree));
            }

            foreach (var m in b._members)
            {
                if (a.GetFactor(m.Degree) == 0)
                {
                    nPolynome._members.Add(new Member(m.Factor, m.Degree));
                }
            }

            return nPolynome;
        }
        
        public static Polynome operator-(Polynome a, Polynome b)
        {
            Polynome nPolynome = new Polynome(0,0);
            nPolynome.Clear();
            foreach (var m in a._members)
            {
                nPolynome._members.Add(new Member(m.Factor-b.GetFactor(m.Degree), m.Degree));
            }

            foreach (var m in b._members)
            {
                if (a.GetFactor(m.Degree) == 0)
                {
                    nPolynome._members.Add(new Member(-m.Factor, m.Degree));
                }
            }

            return nPolynome;
        }
        
        public static Polynome operator-(Polynome a)
        {
            Polynome nPolynome = new Polynome(0,0);
            nPolynome.Clear();
            foreach (var m in a._members)
            {
                nPolynome._members.Add(new Member(-m.Factor, m.Degree));
            }

            return nPolynome;
        }
        
        public static Polynome operator*(Polynome a, Polynome b)
        {
            Polynome nPolynome = new Polynome(0,0);
            nPolynome.Clear();

            foreach (var _a in a._members)
            {
                foreach (var _b in b._members)
                {
                    int deg = _a.Degree + _b.Degree;
                    int c = _a.Factor * _b.Factor;
                    if(nPolynome.GetFactor(deg) == 0) nPolynome._members.Add(new Member(c, deg));
                    else
                    {
                        foreach (var m in nPolynome._members)
                        {
                            if (m.Degree == deg)
                            {
                                m.Factor += c;
                                break;
                            }
                        }
                    }
                }
            }
            
            return nPolynome;
        }

        public static bool operator ==(Polynome a, Polynome b)
        {
            foreach (var m in a._members)
            {
                if (m.Factor != b.GetFactor(m.Degree)) return false;
            }
            
            foreach (var m in b._members)
            {
                if (m.Factor != a.GetFactor(m.Degree)) return false;
            }
            
            return true;
        }

        public static bool operator !=(Polynome a, Polynome b)
        {
            return !(a == b);
        }
    }
}