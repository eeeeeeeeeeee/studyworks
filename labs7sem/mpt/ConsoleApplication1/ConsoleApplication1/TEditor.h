#pragma once
#pragma once
#include <iostream>
#include <sstream>
#include <regex>
#include "TPNumber.h"

class TEditor
{
protected:
	float GetFloat(const char* v) {
		std::cmatch res;
		std::regex rg("^(-{0,1}[0-9]{1,8}[.]{0,1}[0-9]{0,8})$");

		if (!std::regex_match(v, rg)) return 0;

		std::regex_search(v, res, rg);
		float result = std::atof(res.str(1).c_str());
		return result;
	}

	int GetInt(const char* v) {
		std::cmatch res;
		std::regex rg("^(-{0,1}[0-9]{1,8}[.]{0,1}[0-9]{0,8})$");

		if (!std::regex_match(v, rg)) return 0;

		std::regex_search(v, res, rg);
		int result = std::atoi(res.str(1).c_str());
		return result;
	}
public:
	virtual bool IsZero() = 0;

	virtual string AddSign(const char* s) = 0;

	virtual string AddNumber(int n) = 0;

	virtual string AddZero() = 0;

	virtual string RemoveLast() = 0;

	virtual string Clear() = 0;
};

