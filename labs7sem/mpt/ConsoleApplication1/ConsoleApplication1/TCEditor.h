#pragma once
#include <iostream>
#include <sstream>
#include <regex>
#include "TEditor.h"

enum class TMode {Re, Im};

class TCEditor : public TEditor
{
private:
	string a;
	string b;
	string c;
	TMode mode;

public:
	string getprop() {
		std::stringstream s;
		s << a << " " << c << " i * " << b;
		return s.str();
	}
	void setprop(string val) {
		std::cmatch res;
		std::regex rg("^(-{0,1}[0-9]+[.]{0,1}[0-9]*)[ ]{0,1}([+]|[-])[ ]{0,1}i[ ]{0,1}[*][ ]{0,1}([0-9]+[.]{0,1}[0-9]*)$");

		if (!std::regex_match(val.c_str(), rg)) return;

		std::regex_search(val.c_str(), res, rg);
		a = res.str(1);
		b = res.str(3);
		c = res.str(2);
	}

	string geta() {
		return a;
	}
	void seta(string val) {
		std::cmatch res;
		std::regex rg("^(-{0,1}[0-9]+[.]{0,1}[0-9]*)$");

		if (!std::regex_match(val.c_str(), rg)) return;

		a = val;
	}
	string getb() {
		return b;
	}
	void setb(string val) {
		std::cmatch res;
		std::regex rg("^(-{0,1})([0-9]+[.]{0,1}[0-9]*)$");

		if (!std::regex_match(val.c_str(), rg)) return;

		std::regex_search(val.c_str(), res, rg);
		c = res.str(1);
		if (res.str(1).length() < 1) c = "+";
		b = res.str(2);
	}
	__declspec(property(get = getprop, put = setprop)) string Str;
	__declspec(property(get = geta, put = seta)) string A;
	__declspec(property(get = getb, put = setb)) string B;

	TCEditor() {
		Str = "0 + i * 0";
		mode = TMode::Re;
	}

	TCEditor(float _a, float _b) {
		mode = TMode::Re;
		std::stringstream s;
		string sign = "+";
		if (_b < 0) sign = "-";
		s << _a << " " << sign << " i * " << fabs(_b);
		Str = s.str();
	}

	bool IsZero() {
		return a == "0" && b == "0";
	}

	string AddSign(const char* s) {
		if (std::strlen(s) < 1) return Str;
		if (s == "+" && a[0] == '-') a = a.substr(1);
		if (s == "-" && a[0] != '-') a = "-" + a;
		if (s == "+i" && c[0] == '-') c = "+";
		if (s == "-i" && c[0] != '-') c = "-";
		return Str;
	}

	void Switch(TMode m) {
		mode = m;
	}

	string Add(int n) {
		switch (mode)
		{
			case TMode::Re: {
				AddNumber(n);
				break;
			}
			case TMode::Im: {
				AddNumberI(n);
				break;
			}
		}
		return Str;
	}

	string AddNumber(int n) {
		std::stringstream s;
		if (a != "0") s << A;
		s << n;
		A = s.str();
		return Str;
	}

	string AddNumberI(int n) {
		std::stringstream s;
		if (B != "0") s << B;
		s << n;
		B = s.str();
		return Str;
	}

	string AddZero() {
		switch (mode)
		{
			case TMode::Re: {
				AddNumber(0);
				break;
			}
			case TMode::Im: {
				AddNumberI(0);
				break;
			}
		}
		return Str;
	}

	string RemoveLast() {
		switch (mode)
		{
			case TMode::Re: {
				RemoveLastR();
				break;
			}
			case TMode::Im: {
				RemoveLastI();
				break;
			}
		}
		return Str;
	}

	string AddDelim() {

		switch (mode)
		{
			case TMode::Re: {
				if (a.find('.') == string::npos) {
					a += ".";
				}
				break;
			}
			case TMode::Im: {
				if (b.find('.') == string::npos) {
					b += ".";
				}
				break;
			}
		}

		return Str;
	}

	string RemoveLastR() {
		if (A.length() <= 1) {
			A = "0";
			return Str;
		}
		A = A.substr(0, a.length() - 1);
		if (A[A.length() - 1] == '.') RemoveLast();
		return Str;
	}

	string RemoveLastI() {
		if (B.length() <= 1) {
			B = "0";
			return Str;
		}
		B = B.substr(0, B.length() - 1);
		if (B[B.length() - 1] == '.') RemoveLastI();
		return Str;
	}

	string Clear() {
		A = "0";
		B = "0";
		return Str;
	}
};

