#pragma once
enum class TOperation { None, Add, Sub, Mul, Dvd };
enum class TFunc { Sqr, Rev };

template <class T>
class TProc
{
public:
	TOperation getprop() {
		return op;
	}
	void setprop(TOperation param) {
		op = param;
	}

	T GetLeft() {
		T o;
		o = *Lop_Res;
		return o;
	}

	T GetRight() {
		T o;
		o = *Rop_Res;
		return o;
	}

	void SetLeft(T o) {
		*Lop_Res = o;
	}

	void SetRight(T o) {
		*Rop_Res = o;
	}

	__declspec(property(get = getprop, put = setprop)) TOperation Operation;
	__declspec(property(get = GetLeft, put = SetLeft)) T LOperand;
	__declspec(property(get = GetRight, put = SetRight)) T ROperand;

	
	TProc() {
		op = TOperation::None;
		Lop_Res = new T();
		Rop_Res = new T();
	}

	void Reset() {
		op = TOperation::None;
		delete Lop_Res;
		delete Rop_Res;
		Lop_Res = new T();
		Rop_Res = new T();
	}

	void ResetOp() {
		op = TOperation::None;
	}

	void DoIt() {
		switch(op) {
			case TOperation::Add: {
				*Lop_Res += *Rop_Res;
				break;
			}
			case TOperation::Sub: {
				*Lop_Res -= *Rop_Res;
				break;
			}
			case TOperation::Mul: {
				*Lop_Res *= *Rop_Res;
				break;
			}
			case TOperation::Dvd: {
				*Lop_Res /= *Rop_Res;
				break;
			}
		}
		op = TOperation::None;
	}

	void DoFunc(TFunc f) {
		switch (f) {
			case TFunc::Sqr: {
				*Rop_Res *= *Rop_Res;
				break;
			}
			case TFunc::Rev: {
				T o;
				*Rop_Res = o - *Rop_Res;
				break;
			}
		}
	}

private:
	T *Lop_Res, *Rop_Res;
	TOperation op;
};

