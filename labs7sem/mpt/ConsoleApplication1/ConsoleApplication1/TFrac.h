#pragma once
#include <iostream>
#include <sstream>
#include <regex>

using std::string;

class TFrac
{
private:
	int a, b;

	void CutSelf() {
		bool negative = false;
		if (a < 0 && b > 0) negative = true;
		if (a >= 0 && b < 0) negative = true;

		a = abs(a);
		b = abs(b);

		if (b == 0) {
			b = 1;
			throw AnError("Divide by ZERO error");
		}

		if (a % b == 0) {
			a /= b;
			b /= b;
		}

		if (a > 0 && b % a == 0) {
			b /= a;
			a /= a;
		}

		if (negative) a = -a;
	}
public:

	class AnError {
	private:
		string m;
	public:
		AnError(string _m) {
			m = _m;
		}

		string GetMessage() {
			return m;
		}
	};

	TFrac() : TFrac(0, 1) {

	}

	TFrac(int _a, int _b) {
		a = _a;
		b = _b;
		CutSelf();
	}

	TFrac(const char* v) {
		std::cmatch res;
		std::regex rg("^(-{0,1}[0-9]{0,5})/(-{0,1}[0-9]{0,5})$");

		if (!std::regex_match(v, rg)) throw AnError("Can't create a fraction");

		std::regex_search(v, res, rg);
		a = std::atoi(res.str(1).c_str());
		b = std::atoi(res.str(2).c_str());
		CutSelf();
	}

	const TFrac& operator=(TFrac i) {
		a = i.a;
		b = i.b;
		return *this;
	}

	const TFrac& operator+=(const TFrac& i) {
		a = (a*i.b + i.a*b);
		b = b*i.b;
		CutSelf();
		return *this;
	}

	const TFrac& operator-=(const TFrac& i) {
		a = (a * i.b - i.a * b);
		b = b * i.b;
		CutSelf();
		return *this;
	}

	const TFrac& operator/=(const TFrac& i) {
		a = a * i.b;
		b = b * i.a;
		CutSelf();
		return *this;
	}

	const TFrac& operator*=(const TFrac& i) {
		a = a * i.a;
		b = b * i.b;
		CutSelf();
		return *this;
	}

	const TFrac operator+(const TFrac& i) {
		return (TFrac(a, b) += i);
	}

	const TFrac& operator-(const TFrac& i) {
		return (TFrac(a, b) -= i);
	}

	const TFrac& operator-() {
		return TFrac(-a, b);
	}

	bool Equal(const TFrac i) {
		if (i.a != 0 && a / i.a == b / i.b && a / i.a != 0) return true;
		if (a != 0 && i.a / a == i.b / b && i.a / a != 0) return true;
		if (i.a == 0 && a == 0) return true;
		return false;
	}

	friend const bool operator==(TFrac i, TFrac i1) {
		return (i.Equal(i1));
	}

	friend const bool operator!=(TFrac i, TFrac i1) {
		return (!i.Equal(i1));
	}


	bool Greater(const TFrac& i) {
		if ((double)a / b < (double)i.a / i.b) return true;
		return false;
	}

	bool Less(const TFrac& i) {
		if ((double)a / b > (double)i.a / i.b) return true;
		return false;
	}

	friend const bool operator<(TFrac i, TFrac i1) {
		return (i.Greater(i1));
	}

	friend const bool operator>(TFrac i, TFrac i1) {
		return (i.Less(i1));
	}

	const TFrac& operator*(const TFrac& i) {
		return (TFrac(a, b) *= i);
	}

	const TFrac& operator/(const TFrac& i) {
		return (TFrac(a, b) /= i);
	}

	const TFrac& Copy() {
		return TFrac(a, b);
	}

	static const TFrac& Sqr(const TFrac& i) {
		return TFrac(i.a,i.b)*i;
	}

	static const TFrac& Reverse(const TFrac& i) {
		return TFrac(1, 1) / i;
	}

	string ToString() {
		std::stringstream s;
		s << a << "/" << b;
		return s.str();
	}

	void Set(int a, int b) {
		this->a = a;
		this->b = b;
	}

	int GetUp() {
		return a;
	}

	string GetUpString() {
		std::stringstream s;
		s << a;
		return s.str();
	}

	string GetDownString() {
		std::stringstream s;
		s << b;
		return s.str();
	}

	int GetDown() {
		return b;
	}

};

