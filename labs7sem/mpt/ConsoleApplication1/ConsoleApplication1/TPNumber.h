#pragma once
#include <iostream>
#include <sstream>
#include <regex>

using std::string;

class TPNumber
{
private:
	double a;
	int b;
	int c;

	float GetFloat(const char* v) {
		std::cmatch res;
		std::regex rg("^(-{0,1}[0-9]{1,8}[.]{0,1}[0-9]{0,8})$");

		if (!std::regex_match(v, rg)) throw AnError("Can't create a number");

		std::regex_search(v, res, rg);
		float result = std::atof(res.str(1).c_str());
		return result;
	}

	int GetFPart(float n) {
		float cr = n - (int)n;
		for (int i = 0; i < c; i++) {
			cr *= 10;
		}
		return (int)cr;
	}

	char IntToChar(int ch) {
		int nCh = ch;
		if (nCh >= 10 && nCh <= 16) {
			nCh = 'a' + (nCh % 10);
		}
		else {
			nCh = '0' + nCh;
		}
		return (char)nCh;
	}

	int CharToInt(char ch) {
		int v = 0;
		if (ch >= '0' && ch <= '9') {
			v = ch - '0';
			return v;
		}
		if (ch >= 'a' && ch <= 'f') {
			v = 10 + ch - 'a';
			return v;
		}
		throw AnError("Uncorrect");

		return v;
	}

public:

	class AnError {
	private:
		string m;
	public:
		AnError(string _m) {
			m = _m;
		}

		string GetMessage() {
			return m;
		}
	};

	TPNumber() : TPNumber(0, 2, 4) {

	}

	TPNumber(float _a, int _b, int _c) {
		if (_b > 16 || _b < 2) throw AnError("Incorrect B. 2 >= B >= 16");
		if (_c < 0) throw AnError("Incorrect C. C >= 0");

		a = _a;
		b = _b;
		c = _c;
	}

	TPNumber(const char* v) {
		Set(v);
	}

	const TPNumber& operator=(TPNumber i) {
		a = i.a;
		b = i.b;
		c = i.c;
		return *this;
	}

	const TPNumber& operator+=(const TPNumber& i) {
		if (b != i.b || c != i.c) throw AnError("Incorrect operation+");
		a += i.a;
		b = i.b;
		c = i.c;
		return *this;
	}

	const TPNumber& operator-=(const TPNumber& i) {
		if (b != i.b || c != i.c) throw AnError("Incorrect operation-");
		a -= i.a;
		b = i.b;
		c = i.c;
		return *this;
	}

	const TPNumber& operator*=(const TPNumber& i) {
		if (b != i.b || c != i.c) throw AnError("Incorrect operation*");
		a *= i.a;
		b = i.b;
		c = i.c;
		return *this;
	}

	const TPNumber& operator/=(const TPNumber& i) {
		if (b != i.b || c != i.c) throw AnError("Incorrect operation*");
		a /= i.a;
		b = i.b;
		c = i.c;
		return *this;
	}

	const TPNumber operator+(const TPNumber& i) {
		return (TPNumber(a, b, c) += i);
	}

	const TPNumber& operator-(const TPNumber& i) {
		return (TPNumber(a, b, c) -= i);
	}

	const TPNumber operator*(const TPNumber& i) {
		return (TPNumber(a, b, c) *= i);
	}

	const TPNumber& operator/(const TPNumber& i) {
		return (TPNumber(a, b, c) /= i);
	}

	const TPNumber& operator-() {
		return TPNumber(-a, b, c);
	}

	const TPNumber& Copy() {
		return TPNumber(a, b, c);
	}

	const int Abs() {
		return sqrt(a * a + b * b);
	}

	static const TPNumber& Sqr(const TPNumber& i) {
		return TPNumber(i.a, i.b, i.c) * i;
	}

	static const TPNumber& Reverse(const TPNumber& i) {
		if(i.a == 0) throw AnError("Incorrect reverse");
		return TPNumber(1 / i.a, i.b, i.c);
	}

	void Set(int _a, int _b, int _c) {
		a = _a;
		b = _b;
		c = _c;
	}

	void Set(const char* v) {
		std::cmatch res;
		std::regex rg("^(-{0,1}[a-f0-9]+[.]{0,1}[a-f0-9]*),[ ]*([0-9]+),[ ]*([0-9]+)$");

		if (!std::regex_match(v, rg)) throw AnError("Can't create a number");

		std::regex_search(v, res, rg);
		float _b = std::atoi(res.str(2).c_str());
		float _c = std::atoi(res.str(3).c_str());
		SetModule(_b);
		SetAcc(_c);
		SetPN(res.str(1).c_str());
	}

	float GetPN() {
		return a;
	}

	string GetPNString() {
		std::stringstream s;

		string v = "";
		int val = a;
		while (val > 0) {
			
			v = IntToChar(val%b) + v;
			val /= b;
		}
		s << v;

		float val2 = a - (int)a;
		if(val2 == 0) return s.str();

		v = "";
		for (int i = 0; i < c; i++) {
			val2 *= b;
			v = v + IntToChar((int)val2 % b);
			val2 -= (int)val2 % b;
		}
		s << "." << v;
		return s.str();
	}

	int GetModule() {
		return b;
	}

	string GetModuleString() {
		std::stringstream s;
		s << b;
		return s.str();
	}

	int GetAcc() {
		return c;
	}

	string GetAccString() {
		std::stringstream s;
		s << c;
		return s.str();
	}

	void SetPN(float p) {
		a = p;
	}

	void SetModule(int p) {
		if (p > 16 || p < 2) throw AnError("Incorrect B. 2 >= B >= 16");
		b = p;
	}

	void SetAcc(int p) {
		if (p < 0) throw AnError("Incorrect C. C >= 0");
		c = p;
	}

	void SetPN(const char* p) {
		int l = 0;
		float r = 0;
		int i = 0;

		for (; i < std::strlen(p); i++) {
			if (p[i] == '.') break;

			int val = CharToInt(p[i]);
			if (val >= b) throw AnError("Uncorrect P");

			l *= b;
			l += val;
		}

		for (int j = i+c; j > i; j--) {
			if (j > std::strlen(p) - 1) {
				continue;
			}
			int val = CharToInt(p[j]);
			if (val >= b) throw AnError("Uncorrect P");

			r += val;
			r /= (double)b;
		}

		a = l + r;
	}

	void SetModule(const char* p) {
		SetModule(GetFloat(p));
	}

	void SetAcc(const char* p) {
		SetAcc(GetFloat(p));
	}

};

