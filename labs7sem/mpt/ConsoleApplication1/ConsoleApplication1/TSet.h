#pragma once
#include <set>
template <class T>
class TSet
{
private:
	std::set<T> s;
public:
	TSet() {

	}

	void Clear() {
		s.clear();
	}

	void Add(T e) {
		s.insert(e);
	}

	void Remove(T e) {
		s.erase(e);
	}

	bool IsEmpty() {
		return s.size() < 1;
	}

	bool Containts(T e) {
		return s.find(e) != s.end();
	}

	void Add(TSet<T> s1) {
		s.insert(s1);
	}

	int Size() {
		return s.size();
	}

	TSet<T> Con(TSet<T> s1) {
		TSet<T> s2;

		std::set<T>::iterator it;
		for (it = s.begin(); it != s.end(); ++it)
		{
			s2.Add(*it);
		}

		std::set<T>::iterator jt;
		for (jt = s1.s.begin(); jt != s1.s.end(); ++jt)
		{
			if (!s2.Containts(*jt)) s2.Add(*jt);
		}

		return s2;
	}

	TSet<T> Dis(TSet<T> s1) {
		TSet<T> s2;

		std::set<T>::iterator it;
		for (it = s.begin(); it != s.end(); ++it)
		{
			s2.Add(*it);
		}

		std::set<T>::iterator jt;
		for (jt = s1.s.begin(); jt != s1.s.end(); ++jt)
		{
			if (s1.Containts(*jt)) s2.Remove(*jt);
		}

		return s2;
	}

	TSet<T> Mult(TSet<T> s1) {
		TSet<T> s2;

		std::set<T>::iterator it;
		for (it = s.begin(); it != s.end(); ++it)
		{
			if(s1.Containts(*it)) s2.Add(*it);
		}

		return s2;
	}

	T operator[](int n) {
		int i = 0;
		std::set<T>::iterator it;
		for (it = s.begin(); it != s.end(); ++it)
		{
			if (i == n) return *it;
			i += 1;
		}
		return NULL;
	}
};

