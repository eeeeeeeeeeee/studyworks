#pragma once
#include <iostream>
#include <sstream>
#include <regex>

#define PI 3.141592

using std::string;

class TComplex
{
private:
	float a, b;

public:

	class AnError {
	private:
		string m;
	public:
		AnError(string _m) {
			m = _m;
		}

		string GetMessage() {
			return m;
		}
	};

	TComplex() : TComplex(0, 0) {

	}

	TComplex(int _a, int _b) {
		a = _a;
		b = _b;
	}

	TComplex(const char* v) {
		Set(v);
	}

	void Set(const char* v) {
		std::cmatch res;
		std::regex rg("^(-{0,1}[0-9]+[.]{0,1}[0-9]*)[ ]{0,1}([+]|[-])[ ]{0,1}i[ ]{0,1}[*][ ]{0,1}(-{0,1}[0-9]+[.]{0,1}[0-9]*)$");

		if (!std::regex_match(v, rg)) throw AnError("Can't create a number");

		std::regex_search(v, res, rg);
		a = std::atof(res.str(1).c_str());
		b = std::atof(res.str(3).c_str());
		if (res.str(2).c_str()[0] == '-') {
			b *= -1;
		}
	}

	const TComplex& operator=(TComplex i) {
		a = i.a;
		b = i.b;
		return *this;
	}

	const TComplex& operator+=(const TComplex& i) {
		a += i.a;
		b += i.b;
		return *this;
	}

	const TComplex& operator-=(const TComplex& i) {
		a -= i.a;
		b -= i.b;
		return *this;
	}

	const TComplex& operator*=(const TComplex& i) {
		a = a * i.a - b * i.b;
		b = a * i.b + i.a * b;
		return *this;
	}

	const TComplex operator+(const TComplex& i) {
		return (TComplex(a, b) += i);
	}

	const TComplex& operator-(const TComplex& i) {
		return (TComplex(a, b) -= i);
	}

	const TComplex& operator-() {
		return TComplex(-a, -b);
	}

	const TComplex& operator/=(const TComplex& i) {
		a = (a * i.a + b * i.b)/(i.a*i.a + i.b*i.b);
		b = (i.a*b - a*i.b)/(i.a*i.a + i.b*i.b);
		return *this;
	}

	const TComplex& operator/(const TComplex& i) {
		return (TComplex(a, b) /= i);
	}

	const bool operator==(const TComplex& i) {
		if (fabs(i.a - a) < 0.001 && fabs(i.b - b) < 0.001) return true;
		return false;
	}

	const bool operator!=(const TComplex& i) {
		return(!(*this == i));
	}

	const TComplex& operator*(const TComplex& i) {
		return (TComplex(a, b) *= i);
	}

	const TComplex& Copy() {
		return TComplex(a, b);
	}

	const int Abs() {
		return sqrt(a * a + b * b);
	}

	static const TComplex& Sqr(const TComplex& i) {
		return TComplex(i.a, i.b) * i;
	}

	static const TComplex& Reverse(const TComplex& i) {
		return TComplex(i.a/(i.a*i.a+i.b*i.b), -i.b/(i.a*i.a+i.b*i.b));
	}

	string ToString() {
		std::stringstream s;
		s << a;
		if (b >= 0) s << "+i*"; else s << "-i*";
		s << fabs(b);
		return s.str();
	}

	void Set(int a, int b) {
		this->a = a;
		this->b = b;
	}

	int GetRe() {
		return a;
	}

	float ToRad() {
		if (a == 0 && b < 0) return -PI / 2;
		if (a == 0 && b > 0) return PI / 2;
		if (a == 0 && b == 0) return 0;
		if (a > 0) return atan2(b, a);
		else
			return atan2(b, a) + PI;
	}

	const TComplex& Sqrt(int i, int n) {
		if (i >= n) {
			throw AnError("Incorrect K");
		}

		float fi = ToRad();
		float r = Abs();
		r = pow(r, 1. / n);
		return TComplex(r * cos((fi+2 * i * PI)/n), r * sin((fi + 2 * i * PI) / n));
	}
	

	const TComplex& Pow(int n) {
		float r = Abs();
		for (int i = 0; i < n; i++) {
			r *= r;
		}
		float fi = ToRad();
		return TComplex(r*cos(n*fi), r*sin(n*fi));
	}

	float ToGrad() {
		return (ToRad() * 180) / PI;
	}

	string GetReString() {
		std::stringstream s;
		s << a;
		return s.str();
	}

	string GetImString() {
		std::stringstream s;
		s << b;
		return s.str();
	}

	int GetIm() {
		return b;
	}

};

