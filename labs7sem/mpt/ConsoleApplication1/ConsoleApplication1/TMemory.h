#pragma once
template<class T>
class TMemory
{
private:
	T fnumber;
	bool fstate;
public:
	bool getprop() {
		return fstate;
	}
	void setprop(bool param) {
		fstate = param;
	}
	__declspec(property(get = getprop, put = setprop)) bool FState;

	TMemory() {
		fstate = false;
	}

	void Put(T o) {
		fnumber = o;
		fstate = true;
	}

	T Get() {
		T o = fnumber;
		fstate = true;
		return o;
	}

	void Add(T o) {
		fstate = true;
		fnumber += o;
	}

	void Clear() {
		fstate = false;
		T def;
		fnumber = def;
	}

	const char* ReadMState() {
		if (fstate) return "_On"; else return "_Off";
	}

	T* GetT() {
		return &fnumber;
	}
};

