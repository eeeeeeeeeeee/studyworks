#pragma once
#include <iostream>
#include <sstream>
#include <regex>
#include "TEditor.h"

enum class TFMode { Up, Down };
class TFEditor : public TEditor
{
private:
	string str;
	TFMode mode = TFMode::Up;

public:
	string getprop() {
		return str;
	}
	void setprop(string val) {
		std::cmatch res;
		std::regex rg("^(-{0,1}[0-9]{0,5})/(-{0,1}[0-9]{0,5})$");

		if (std::regex_match(val.c_str(), rg)) str = val;
	}

	string getup() {
		std::cmatch res;
		std::regex rg("^(-{0,1}[0-9]{0,5})/(-{0,1}[0-9]{0,5})$");

		std::regex_search(str.c_str(), res, rg);
		return res.str(1);
	}
	void setup(string val) {
		std::stringstream s;
		s << val << "/" << getdown();

		Str = s.str();
	}
	string getdown() {
		std::cmatch res;
		std::regex rg("^(-{0,1}[0-9]{0,5})/(-{0,1}[0-9]{0,5})$");

		std::regex_search(str.c_str(), res, rg);
		return res.str(2);
	}
	void setdown(string val) {
		std::stringstream s;
		s << getup() << "/" << val;

		Str = s.str();
	}
	__declspec(property(get = getprop, put = setprop)) string Str;
	__declspec(property(get = getup, put = setup)) string Up;
	__declspec(property(get = getdown, put = setdown)) string Down;

	TFEditor() {
		Str = "0/1";
	}

	TFEditor(int _a, int _b) {
		std::stringstream s;
		s << _a << "/" << _b;
		Str = s.str();
	}

	bool IsZero() {
		return Up == "0";
	}

	string AddSign(const char* s) {
		if (std::strlen(s) < 1) return str;
		if (s == "+" && Up[0] == '-') Up = Up.substr(1);
		if (s == "-" && Up[0] != '-') Up = "-" + Up;
		return Str;
	}

	void Switch(TFMode m) {
		mode = m;
	}

	string AddNumber(int n) {
		if (mode == TFMode::Up) {
			std::stringstream s;
			if (Up != "0") s << Up;
			s << n;
			Up = s.str();
		}
		else {
			std::stringstream s;
			if (Down != "0") s << Down;
			s << n;
			Down = s.str();
		}
		return Str;
	}

	string AddZero() {
		return AddNumber(0);
	}

	string RemoveLast() {
		if (mode == TFMode::Up) {
			if (Up.length() <= 1) {
				Up = "0";
				return Str;
			}
			Up = Up.substr(0, Up.length() - 1);
		}
		else {
			if (Down.length() <= 1) {
				return Str;
			}
			Down = Down.substr(0, Down.length() - 1);
		}
		return Str;
	}

	string Clear() {
		Up = "0";
		return Str;
	}
};

