#pragma once
#include <iostream>
#include <sstream>
#include <regex>
#include "TPNumber.h"
#include "TEditor.h"

class TPEditor : public TEditor
{
private:
	string str;
	string a, b, c;

	char IntToChar(int ch) {
		int nCh = ch;

		if (nCh >= std::atoi(B.c_str())) throw AnError("U");

		if (nCh >= 10 && nCh <= 16) {
			nCh = 'a' + (nCh % 10);
		}
		else {
			nCh = '0' + nCh;
		}
		return (char)nCh;
	}

public:
	class AnError {
	private:
		string m;
	public:
		AnError(string _m) {
			m = _m;
		}

		string GetMessage() {
			return m;
		}
	};

	string getprop() {
		std::stringstream s;
		s << A << ", " << B << ", " << C;
		return s.str();
	}
	void setprop(string val) {
		std::cmatch res;
		std::regex rg("^(-{0,1}[0-9]+[.]{0,1}[0-9]*),[ ]*([0-9]+),[ ]*([0-9]+)$");

		if (!std::regex_match(val.c_str(), rg)) return;

		std::regex_search(val.c_str(), res, rg);
		B = res.str(2);
		C = res.str(3);
		A = res.str(1);
	}

	string geta() {
		return a;
	}
	void seta(string p) {
		std::stringstream s;
		s << (std::atoi(B.c_str()) - 1);

		std::cmatch res;
		std::regex rg("^(-{0,1})([0-"+s.str()+"a-f]+[.]{0,1}[0-" + s.str() + "a-f]{0,"+C+"})$");
		if (!std::regex_match(p.c_str(), rg)) return;

		a = p;
	}
	string getb() {
		return b;
	}
	void setb(string val) {
		std::cmatch res;
		std::regex rg("^[0-9]+$");

		if (!std::regex_match(val.c_str(), rg)) return;

		if(std::atoi(val.c_str()) < 2 || std::atoi(val.c_str()) > 16) return;

		b = val;
	}
	string getc() {
		return c;
	}
	void setc(string val) {
		std::cmatch res;
		std::regex rg("^[0-9]+$");

		if (!std::regex_match(val.c_str(), rg)) return;
		c = val;
	}
	__declspec(property(get = getprop, put = setprop)) string Str;
	__declspec(property(get = geta, put = seta)) string A;
	__declspec(property(get = getb, put = setb)) string B;
	__declspec(property(get = getc, put = setc)) string C;

	TPEditor() {
		Str = "0, 2, 1";
	}

	TPEditor(float _a, int _b, int _c) {
		std::stringstream s;
		s << _a << ", " << _b << ", " << _c;
		Str = s.str();
	}

	bool IsZero() {
		return a == "0";
	}

	string AddSign(const char* s) {
		if (std::strlen(s) < 1) return Str;
		if (s == "+" && a[0] == '-') a = a.substr(1);
		if (s == "-" && a[0] != '-') a = "-" + a;
		return Str;
	}

	string AddNumber(int n) {
		std::stringstream s;
		if (a != "0") s << A;
		s << IntToChar(n);
		A = s.str();
		return Str;
	}

	string AddDelim() {
		if (a.find('.') == string::npos) {
			a += ".";
		}
		return Str;
	}

	string AddZero() {
		return AddNumber(0);
	}

	string RemoveLast() {
		if (A.length() <= 1) {
			A = "0";
			return Str;
		}
		A = A.substr(0, a.length() - 1);
		if (A[A.length() - 1] == '.') RemoveLast();
		return Str;
	}

	string Clear() {
		A = "0";
		return Str;
	}
};

