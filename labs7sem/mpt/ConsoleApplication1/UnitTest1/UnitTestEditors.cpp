#include "pch.h"
#include "CppUnitTest.h"
#include "../ConsoleApplication1/TPEditor.h"
#include "../ConsoleApplication1/TCEditor.h"
#include "../ConsoleApplication1/TFEditor.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Editors
{
	TEST_CLASS(All)
	{
	public:

		TEST_METHOD(PNumber1)
		{
			TPEditor e(0, 10, 4);
			e.AddNumber(5);
			Assert::AreEqual(string("5, 10, 4"), e.Str);
			e.AddNumber(2);
			e.AddNumber(5);
			Assert::AreEqual(string("525, 10, 4"), e.Str);
			e.RemoveLast();
			Assert::AreEqual(string("52, 10, 4"), e.Str);
			e.RemoveLast();
			e.RemoveLast();
			Assert::AreEqual(string("0, 10, 4"), e.Str);
			e.AddNumber(1);
			e.AddZero();
			e.AddSign("-");
			Assert::AreEqual(string("-10, 10, 4"), e.Str);
			e.Clear();
			Assert::AreEqual(string("0, 10, 4"), e.Str);
			e.Str = "32,4,4";
			Assert::AreEqual(string("32, 4, 4"), e.Str);
			e.Str = "32.6, 7, 4";
			Assert::AreEqual(string("32.6, 7, 4"), e.Str);
			e.RemoveLast();
			Assert::AreEqual(string("32, 7, 4"), e.Str);
		}

		TEST_METHOD(PNumber2)
		{
			TPEditor e(0, 16, 4);
			e.AddNumber(5);
			e.AddDelim();
			e.AddNumber(5);
			e.AddNumber(15);
			Assert::AreEqual(string("5.5f, 16, 4"), e.Str);
			for (int i = 0; i < 20; i++) {
				e.AddNumber(15);
			}
			Assert::AreEqual(string("5.5fff, 16, 4"), e.Str);
			e.RemoveLast();
			Assert::AreEqual(string("5.5ff, 16, 4"), e.Str);
			e.RemoveLast();
			e.RemoveLast();
			e.RemoveLast();

			Assert::AreEqual(string("5, 16, 4"), e.Str);

			e.RemoveLast();
			e.AddDelim();
			e.AddZero();
			e.AddZero();
			Assert::AreEqual(string("0.00, 16, 4"), e.Str);
		}

		TEST_METHOD(PNumber3)
		{
			auto func = [] {TPEditor a(0, 10, 0); a.AddNumber(10); };
			Assert::ExpectException<TPEditor::AnError>(func);
		}

		TEST_METHOD(PNumber4)
		{
			auto func = [] {TPEditor a(0, 10, 0); a.AddDelim(); a.AddNumber(10); };
			Assert::ExpectException<TPEditor::AnError>(func);
		}

		TEST_METHOD(Complex1)
		{
			TCEditor e(0,5);
			e.AddNumber(5);
			Assert::AreEqual(string("5 + i * 5"), e.Str);
			e.AddNumber(25);
			Assert::AreEqual(string("525 + i * 5"), e.Str);
			e.RemoveLast();
			e.AddSign("-i");
			Assert::AreEqual(string("52 - i * 5"), e.Str);
			e.RemoveLast();
			e.RemoveLast();
			e.AddSign("+i");
			e.RemoveLastI();
			Assert::AreEqual(string("0 + i * 0"), e.Str);
			e.AddNumber(1);
			e.AddZero();
			e.AddSign("-");
			Assert::AreEqual(string("-10 + i * 0"), e.Str);
			e.Clear();
			Assert::AreEqual(string("0 + i * 0"), e.Str);
			e.Str = "32 + i *4";
			Assert::AreEqual(string("32 + i * 4"), e.Str);
			e.Str = "32.6+ i* 4";
			Assert::AreEqual(string("32.6 + i * 4"), e.Str);
		}

		TEST_METHOD(Complex2)
		{
			TCEditor e;
			e.Add(5);
			e.Switch(TMode::Im);
			e.Add(3);
			Assert::AreEqual(string("5 + i * 3"), e.Str);
			e.AddDelim();
			Assert::AreEqual(string("5 + i * 3."), e.Str);
			e.AddDelim();
			Assert::AreEqual(string("5 + i * 3."), e.Str);
			e.RemoveLast();
			Assert::AreEqual(string("5 + i * 3"), e.Str);
			e.Switch(TMode::Re);
			e.AddDelim();
			Assert::AreEqual(string("5. + i * 3"), e.Str);
			e.Add(6);
			Assert::AreEqual(string("5.6 + i * 3"), e.Str);
		}

		TEST_METHOD(Fractions1)
		{
			TFEditor e(1, 1);
			e.AddNumber(5);
			Assert::AreEqual(string("15/1"), e.Str);
			e.AddNumber(25);
			Assert::AreEqual(string("1525/1"), e.Str);
			e.RemoveLast();
			Assert::AreEqual(string("152/1"), e.Str);
			e.RemoveLast();
			e.RemoveLast();
			e.RemoveLast();
			Assert::AreEqual(string("0/1"), e.Str);
			e.AddNumber(1);
			e.AddZero();
			e.AddSign("-");
			Assert::AreEqual(string("-10/1"), e.Str);
			e.Clear();
			Assert::AreEqual(string("0/1"), e.Str);
			e.Str = "32/5";
			Assert::AreEqual(string("32/5"), e.Str);
		}

		TEST_METHOD(Fractions2)
		{
			TFEditor e(11, 1);
			e.Switch(TFMode::Down);
			e.AddNumber(5);
			Assert::AreEqual("11/15", e.Str.c_str());
			e.RemoveLast();
			e.RemoveLast();
			Assert::AreEqual("11/1", e.Str.c_str());
		}

	};
}
