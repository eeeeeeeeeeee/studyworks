#include "pch.h"
#include "CppUnitTest.h"
#include "../ConsoleApplication1/TFrac.h"
#include "../ConsoleApplication1/TSet.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Set
{
	TEST_CLASS(All)
	{
	public:

		TEST_METHOD(Floats1)
		{
			TSet<float> s;
			Assert::AreEqual(true, s.IsEmpty());
			s.Add(32.5);
			Assert::AreEqual(false, s.IsEmpty());
			s.Remove(32.5);
			Assert::AreEqual(true, s.IsEmpty());
			Assert::AreEqual(false, s.Containts(32.5));
			s.Add(11.5);
			Assert::AreEqual(true, s.Containts(11.5));
			Assert::AreEqual(1, s.Size());
			s.Add(22.5);
			Assert::AreEqual(2, s.Size());
			s.Add(22.5);
			Assert::AreEqual(2, s.Size());
		}

		TEST_METHOD(Floats2)
		{
			TSet<float> s;
			s.Add(11.5);
			s.Add(22.5);
			s.Add(32);
			TSet<float> s1;
			s1.Add(11);
			s1.Add(22);
			s1.Add(32);
			TSet<float> s2 = s1.Con(s);
			Assert::AreEqual(5, s2.Size());
			Assert::AreEqual(true, fabs(11 - s2[0]) < 0.01);
		}

		TEST_METHOD(Fracs1)
		{
			TSet<TFrac> s;
			Assert::AreEqual(true, s.IsEmpty());
			s.Add(TFrac(32,2));
			Assert::AreEqual(false, s.IsEmpty());
			s.Remove(TFrac(32, 2));
			Assert::AreEqual(true, s.IsEmpty());
			Assert::AreEqual(false, s.Containts(TFrac(33, 2)));
			s.Add(TFrac(23, 2));
			Assert::AreEqual(true, s.Containts(TFrac(23, 2)));
			Assert::AreEqual(1, s.Size());
			s.Add(TFrac(36, 2));
			Assert::AreEqual(2, s.Size());
			s.Add(TFrac(36, 2));
			Assert::AreEqual(2, s.Size());
			Assert::AreEqual(TFrac(23, 2).ToString(), s[0].ToString());
		}

		TEST_METHOD(Fracs2)
		{
			TSet<TFrac> s;
			s.Add(TFrac(23,2));
			s.Add(TFrac(24,2));
			s.Add(TFrac(25,2));
			TSet<TFrac> s1;
			s1.Add(TFrac(23,2));
			s1.Add(TFrac(23,3));
			s1.Add(TFrac(23,4));
			TSet<TFrac> s2 = s1.Con(s);
			Assert::AreEqual(5, s2.Size());
			Assert::AreEqual(TFrac(23,4).ToString(),s2[0].ToString());
		}

		TEST_METHOD(Fracs3)
		{
			TSet<TFrac> s;
			s.Add(TFrac(23, 2));
			s.Add(TFrac(24, 2));
			s.Add(TFrac(25, 2));
			TSet<TFrac> s1;
			s1.Add(TFrac(23, 2));
			s1.Add(TFrac(23, 3));
			s1.Add(TFrac(23, 4));
			TSet<TFrac> s2 = s1.Dis(s);
			Assert::AreEqual(2, s2.Size());
			Assert::AreEqual(TFrac(23, 4).ToString(), s2[0].ToString());
		}

		TEST_METHOD(Fracs4)
		{
			TSet<TFrac> s;
			s.Add(TFrac(23, 2));
			s.Add(TFrac(24, 2));
			s.Add(TFrac(25, 2));
			TSet<TFrac> s1;
			s1.Add(TFrac(23, 2));
			s1.Add(TFrac(23, 3));
			s1.Add(TFrac(23, 4));
			TSet<TFrac> s2 = s1.Mult(s);
			Assert::AreEqual(1, s2.Size());
			Assert::AreEqual(TFrac(23, 2).ToString(), s2[0].ToString());
		}

	};
}
