#include "pch.h"
#include "CppUnitTest.h"
#include "../ConsoleApplication1/TPNumber.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace PNumber
{
	TEST_CLASS(PNumber1)
	{
	public:

		TEST_METHOD(Constructor1)
		{
			TPNumber a(32, 2, 2);
			Assert::AreEqual((int)a.GetPN(), 32);
			Assert::AreEqual(a.GetModule(), 2);
			Assert::AreEqual(a.GetModule(), 2);
			Assert::AreEqual(a.GetPNString(), string("100000"));
			Assert::AreEqual(a.GetModuleString(), string("2"));
			Assert::AreEqual(a.GetModuleString(), string("2"));
			a += TPNumber("100000.1, 2, 2");
			Assert::AreEqual(645, (int)(a.GetPN()*10));
		}

		TEST_METHOD(Constructor2)
		{
			TPNumber a("1000, 2, 2");
			Assert::AreEqual(8, (int)a.GetPN());
			
			TPNumber b("1001.1, 2, 4");
			Assert::AreEqual(95, (int)(b.GetPN()*10));

			b.SetPN("100.1111");
			Assert::AreEqual(4937, (int)(b.GetPN() * 1000));

			b.SetPN("100.1111111111");
			Assert::AreEqual(string("100.1111"), b.GetPNString());
			Assert::AreEqual(4937, (int)(b.GetPN() * 1000));
		}

		TEST_METHOD(Constructor3)
		{
			auto func = [] {TPNumber f(32, 0, 0); };
			Assert::ExpectException<TPNumber::AnError>(func);

			auto func1 = [] {TPNumber f(32, 2, -1); };
			Assert::ExpectException<TPNumber::AnError>(func1);

			auto func2 = [] {TPNumber a("f, 14, 2"); };
			Assert::ExpectException<TPNumber::AnError>(func2);
		}

		TEST_METHOD(Constructor4)
		{
			TPNumber a("1.fa, 16, 2");
			Assert::AreEqual("1.fa", a.GetPNString().c_str());

			TPNumber b("23.faad, 16, 5");
			Assert::AreEqual(5, b.GetAcc());
			Assert::AreEqual("23.faad0", b.GetPNString().c_str());

			TPNumber c("23, 16, 5");
			Assert::AreEqual("23", c.GetPNString().c_str());
		}

		TEST_METHOD(Letters1)
		{
			TPNumber a("a, 16, 2");
			Assert::AreEqual(10, (int)a.GetPN());
			a += TPNumber(32, 16, 2);
			Assert::AreEqual("2a", a.GetPNString().c_str());
		}

		TEST_METHOD(Letters2)
		{
			TPNumber a("a.a, 16, 1");
			Assert::AreEqual(10, (int)a.GetPN());
			a += TPNumber(32, 16, 1);
			Assert::AreEqual("2a.a", a.GetPNString().c_str());
			a += TPNumber("0.1, 16, 1");
			Assert::AreEqual("2a.b", a.GetPNString().c_str());
			a -= TPNumber("0.1, 16, 1");
			Assert::AreEqual("2a.a", a.GetPNString().c_str());
			a -= TPNumber("0.a, 16, 1");
			Assert::AreEqual("2a", a.GetPNString().c_str());
		}

		TEST_METHOD(Operations1)
		{
			auto func = [] {TPNumber a(32, 2, 3); TPNumber b(32, 4, 3); a + b; };
			Assert::ExpectException<TPNumber::AnError>(func);

			auto func1 = [] {TPNumber a(32, 2, 3); TPNumber b(32, 4, 3); a / b; };
			Assert::ExpectException<TPNumber::AnError>(func1);
		}

		TEST_METHOD(Operations2)
		{
			TPNumber a(32, 5, 3);
			TPNumber b(11, 5, 3);
			TPNumber c = a * b;

			Assert::AreEqual(abs((32 * 11) - c.GetPN()) < 0.01, true);

			c = a + b;
			Assert::AreEqual(abs((32 + 11) - c.GetPN()) < 0.01, true);
		}

		TEST_METHOD(Operations3)
		{
			TPNumber a(1, 3, 6);
			a.SetPN(8);
			a.SetModule(2);
			a.SetAcc(3);
			Assert::AreEqual(string("1000"), a.GetPNString());
			Assert::AreEqual(string("2"), a.GetModuleString());
			Assert::AreEqual(string("3"), a.GetAccString());
			a.SetModule("3");
			a.SetAcc("6");
			a.SetPN("11");
			Assert::AreEqual(4, (int)a.GetPN());
			Assert::AreEqual(string("3"), a.GetModuleString());
			Assert::AreEqual(string("6"), a.GetAccString());
		}
	};
}
