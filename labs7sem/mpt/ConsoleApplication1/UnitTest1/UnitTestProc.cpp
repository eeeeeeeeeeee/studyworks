#include "pch.h"
#include "CppUnitTest.h"
#include "../ConsoleApplication1/TProc.h"
#include "../ConsoleApplication1/TFrac.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Proc
{
	TEST_CLASS(All)
	{
	public:

		TEST_METHOD(Frac)
		{
			TProc<TFrac> p;
			TFrac def;
			Assert::AreEqual(def.GetDown(), p.LOperand.GetDown());

			p.LOperand.Set(32, 1);
			Assert::AreEqual(def.GetUp(), p.LOperand.GetUp());

			TFrac def1(4, 1);
			p.LOperand = def1;
			Assert::AreEqual(false, p.LOperand == def);

			p.ROperand = p.LOperand;
			p.DoFunc(TFunc::Sqr);
			Assert::AreEqual(16, p.ROperand.GetUp());

			Assert::AreEqual(4, p.LOperand.GetUp());

			p.Operation = TOperation::Add;
			Assert::AreEqual(true, TOperation::Add == p.Operation);
			p.DoIt();
			Assert::AreEqual(20, p.LOperand.GetUp());
			Assert::AreEqual(true, p.Operation == TOperation::None);

			p.Reset();
			Assert::AreEqual(def.GetUp(), p.LOperand.GetUp());
			Assert::AreEqual(def.GetUp(), p.ROperand.GetUp());

		}

	};
}
