#include "pch.h"
#include "CppUnitTest.h"
#include "../ConsoleApplication1/TFrac.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Fractions
{
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(Constructor1)
		{
			auto func = [] {TFrac f(1, 0); };
			Assert::ExpectException<TFrac::AnError>(func);
		}

		TEST_METHOD(Constructor2)
		{
			TFrac f("2/1");
			Assert::AreEqual(1, f.GetDown());
			Assert::AreEqual(2, f.GetUp());
		}

		TEST_METHOD(Constructor3)
		{
			auto func = [] {TFrac f("10"); };
			Assert::ExpectException<TFrac::AnError>(func);
		}

		TEST_METHOD(Copy1)
		{
			TFrac f("2/3");
			TFrac f2(1,1);
			f2 = f;
			Assert::AreEqual(f.GetDown(), f2.GetDown());
			Assert::AreEqual(f.GetUp(), f2.GetUp());
		}

		TEST_METHOD(Copy2)
		{
			TFrac f("2/3");
			TFrac f2 = f.Copy();
			Assert::AreEqual(f.GetDown(), f2.GetDown());
			Assert::AreEqual(f.GetUp(), f2.GetUp());
		}

		TEST_METHOD(Operations1)
		{
			TFrac f1("1/2");
			TFrac f2(-3, 4);
			f1 += f2;
			Assert::AreEqual(4, f1.GetDown());
			Assert::AreEqual(-1, f1.GetUp());

			f1.Set(1, 2);
			f2.Set(1, 2);
			TFrac f3 = f1 - f2;

			Assert::AreEqual(1, f3.GetDown());
			Assert::AreEqual(0, f3.GetUp());

			// Check after
			Assert::AreEqual(2, f2.GetDown());
			Assert::AreEqual(1, f2.GetUp());

			Assert::AreEqual(2, f1.GetDown());
			Assert::AreEqual(1, f1.GetUp());

			f3 = f1 * f2;
			Assert::AreEqual(1, f3.GetUp());
			Assert::AreEqual(4, f3.GetDown());

			// Check after
			Assert::AreEqual(2, f2.GetDown());
			Assert::AreEqual(1, f2.GetUp());

			Assert::AreEqual(2, f1.GetDown());
			Assert::AreEqual(1, f1.GetUp());

			f3 = f1 / f2;
			Assert::AreEqual(1, f3.GetUp());
			Assert::AreEqual(1, f3.GetDown());
		}

		TEST_METHOD(Operations2)
		{
			TFrac f(4, 1);
			TFrac f1 = TFrac::Sqr(f);

			Assert::AreEqual(4, f.GetUp());
			Assert::AreEqual(1, f.GetDown());

			Assert::AreEqual(16, f1.GetUp());
			Assert::AreEqual(1, f1.GetDown());

			f1 = TFrac::Reverse(f);
			Assert::AreEqual(1, f1.GetUp());
			Assert::AreEqual(4, f1.GetDown());
		}

		TEST_METHOD(Operations3)
		{
			TFrac f(4, 1);
			TFrac f1(8, 2);

			Assert::AreEqual(f1 == f, true);

			f1 += f;

			Assert::AreEqual(f1 == f, false);

			Assert::AreEqual(f1 > f, true);
			Assert::AreEqual(f1 < f, false);
		}

		TEST_METHOD(Operations4)
		{
			TFrac f(4, 1);

			Assert::AreEqual(f.ToString(), string("4/1"));
			Assert::AreEqual(f.GetUpString(), string("4"));
			Assert::AreEqual(f.GetDownString(), string("1"));
		}
	};
}
