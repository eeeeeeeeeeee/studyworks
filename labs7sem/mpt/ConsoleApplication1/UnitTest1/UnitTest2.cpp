#include "pch.h"
#include "CppUnitTest.h"
#include "../ConsoleApplication1/TComplex.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ComplexNumber
{
	TEST_CLASS(ComplexNumber1)
	{
	public:

		TEST_METHOD(Constructor1)
		{
			TComplex c(32, 42);
			TComplex c1("31 + i * 51");

			Assert::AreEqual(32, c.GetRe());
			Assert::AreEqual(42, c.GetIm());

			Assert::AreEqual(31, c1.GetRe());
			Assert::AreEqual(51, c1.GetIm());
		}

		TEST_METHOD(Operations1)
		{
			TComplex c(32, 42);
			TComplex c1("31+i*51");

			TComplex r = c + c1;
			Assert::AreEqual(63, r.GetRe());
			Assert::AreEqual(93, r.GetIm());

			r = c - c1;
			Assert::AreEqual(1, r.GetRe());
			Assert::AreEqual(42-51, r.GetIm());
		}

		TEST_METHOD(Operations2)
		{
			TComplex c(3, 5);
			TComplex c1("1 +i*2");

			Assert::AreEqual(c == c1, false);
			Assert::AreEqual(c != c1, true);

			c = TComplex(1, 2);
			Assert::AreEqual(c == c1, true);
		}

		TEST_METHOD(Operations3)
		{

			TComplex c(3, 5);
			Assert::AreEqual(c.ToString(), string("3+i*5"));
			Assert::AreEqual(c.GetImString(), string("5"));
			Assert::AreEqual(c.GetReString(), string("3"));
		}

		TEST_METHOD(Operations4)
		{

			TComplex c(1, 1);
			Assert::AreEqual(abs(c.ToRad() - 0.79) < 0.01, true);
		}

	};
}
