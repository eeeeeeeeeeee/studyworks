#include "pch.h"
#include "CppUnitTest.h"
#include "../ConsoleApplication1/TComplex.h"
#include "../ConsoleApplication1/TPNumber.h"
#include "../ConsoleApplication1/TFrac.h"
#include "../ConsoleApplication1/TMemory.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Memory
{
	TEST_CLASS(All)
	{
	public:

		TEST_METHOD(Frac)
		{
			TMemory<TFrac> mem;
			TFrac def;
			Assert::AreEqual(def.GetUp(), mem.Get().GetUp());
			Assert::AreEqual(def.GetDown(), mem.Get().GetDown());

			mem.Put(def);
			def.Set(2, 1);
			Assert::AreEqual(false, mem.Get() == def);

			TFrac* f = mem.GetT();
			f->Set(3, 2);
			Assert::AreEqual(true, mem.Get() == *f);

			Assert::AreEqual(true, mem.FState);
			mem.Clear();
			Assert::AreEqual(false, mem.FState);

			f = new TFrac(32, 1);
			Assert::AreEqual(false, mem.Get() == *f);
		}

		TEST_METHOD(Complex)
		{
			TMemory<TComplex> mem;
			TComplex def;
			Assert::AreEqual(def.GetRe(), mem.Get().GetRe());
			Assert::AreEqual(def.GetIm(), mem.Get().GetIm());

			mem.Put(def);
			def.Set(2, 1);
			Assert::AreEqual(false, mem.Get() == def);

			TComplex* f = mem.GetT();
			f->Set(3, 2);
			Assert::AreEqual(true, mem.Get() == *f);

			Assert::AreEqual(true, mem.FState);
			mem.Clear();
			Assert::AreEqual(false, mem.FState);

			f = new TComplex(32, 1);
			Assert::AreEqual(false, mem.Get() == *f);
		}

		TEST_METHOD(PNumber)
		{
			TMemory<TPNumber> mem;
			TPNumber def;
			Assert::AreEqual(def.GetPN(), mem.Get().GetPN());
			Assert::AreEqual(def.GetModule(), mem.Get().GetModule());

			mem.Put(def);
			def.Set(2, 2, 2);
			Assert::AreEqual(false, mem.Get().GetPN() == def.GetPN());

			TPNumber* f = mem.GetT();
			f->Set(3, 2, 2);
			Assert::AreEqual(true, mem.Get().GetPN() == f->GetPN());

			Assert::AreEqual(true, mem.FState);
			mem.Clear();
			Assert::AreEqual(false, mem.FState);

			f = new TPNumber(32, 2, 2);
			Assert::AreEqual(false, mem.Get().GetPN() == f->GetPN());
		}

	};
}
