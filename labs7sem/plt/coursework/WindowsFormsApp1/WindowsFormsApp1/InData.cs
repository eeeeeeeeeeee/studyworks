﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class InData
    {
        [JsonProperty("Abc")]
        public string[] Abc { get; set; }

        [JsonProperty("Begin")]
        public string Begin { get; set; }

        [JsonProperty("End")]
        public string End { get; set; }

        [JsonProperty("Symb")]
        public string Symb { get; set; }

        [JsonProperty("Repeat")]
        public int Repeat { get; set; }

        [JsonProperty("TOut")]
        public int TOut { get; set; }
    }
}
