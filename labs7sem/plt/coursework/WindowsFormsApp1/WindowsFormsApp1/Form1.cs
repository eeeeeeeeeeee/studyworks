﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Gr;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        private Grammar _grammar = null;
        private LinesEmpl _empl = null;
        public Form1()
        {
            InitializeComponent();
        }

        private void authorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Вариант 10. Написать программу, которая" +
                " по предложенному описанию языка построит регулярную" +
                " грамматику (ЛЛ или ПЛ –по заказу пользователя)," +
                " задающую этот язык, и позволит сгенерировать с её" +
                " помощью все цепочки языка в заданном диапазоне длин.\n\n" +
                "Алфавит, заданные начальная и конечная подцепочкии кратность" +
                " вхождения некоторого символаалфавита во все цепочки языка", "Мальцев Богдан. ИП-615");
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 form = new Form2();
            form.Show();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(openFileDialog1.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            string file = openFileDialog1.FileName;
            string content;
            FileStream fileStream = File.OpenRead(file);
            StreamReader streamReader = new StreamReader(fileStream);
            content = streamReader.ReadToEnd();
            streamReader.Close();
            fileStream.Close();

            InData conf = JsonConvert.DeserializeObject<InData>(content);
            textBox4.Text = conf.Symb;
            textBox5.Text = conf.Repeat.ToString();
            textBox2.Text = conf.Begin;
            textBox3.Text = conf.End;

            if (conf.TOut > 0)
            {
                radioButton1.Checked = false;
                radioButton2.Checked = true;
            }
            else
            {
                radioButton1.Checked = true;
                radioButton2.Checked = false;
            }

            StringBuilder abcBuilder = new StringBuilder();
            foreach(string element in conf.Abc)
            {
                abcBuilder.Append(element);
                abcBuilder.Append(" ");
            }
            textBox1.Text = abcBuilder.ToString().Trim();
        }

        private InData GenerateObject()
        {
            InData conf = new InData();
            conf.Symb = textBox4.Text;
            conf.Repeat = Int32.Parse(textBox5.Text);
            conf.Begin = textBox2.Text;
            conf.End = textBox3.Text;

            if (radioButton1.Checked)
            {
                conf.TOut = 0;
            }
            else
            {
                conf.TOut = 1;
            }

            conf.Abc = textBox1.Text.Split(' ');
            if (conf.Abc.Contains(""))
            {
                throw new Exception("Алфавит содержит недопустимые символы. Убедитесь, что не указаны " +
                                    "лишние пробелы.");
            }

            return conf;
        }
        private void Save()
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            string file = saveFileDialog1.FileName;

            string data = null;
            try
            {
                data = JsonConvert.SerializeObject(GenerateObject());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!");
                return;
            }

            FileStream fileStream = File.OpenWrite(file);
            StreamWriter streamWriter = new StreamWriter(fileStream);
            streamWriter.Write(data);
            streamWriter.Close();
            fileStream.Close();
        }
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            InData conf = null;
            try
            {
                conf = GenerateObject();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!");
                return;
            }

            foreach(string element in conf.Abc)
            {
                if(element.Length>1)
                {
                    MessageBox.Show("Нельзя указывать подцепочки языка в алфавите.", "Ошибка!");
                    return;
                }
            }

            foreach (char element in conf.Begin)
            {
                if (!conf.Abc.Contains(element.ToString()))
                {
                    MessageBox.Show("Начальная подцепочка содежит символ '"+ element + "', не указанный в алфавите.", "Ошибка!");
                    return;
                }
            }

            foreach (char element in conf.End)
            {
                if (!conf.Abc.Contains(element.ToString()))
                {
                    MessageBox.Show("Конечная подцепочка содежит символ '" + element + "', не указанный в алфавите.", "Ошибка!");
                    return;
                }
            }

            if(!conf.Abc.Contains(conf.Symb))
            {
                MessageBox.Show("Кратный символ '" + conf.Symb + "', не указан в алфавите.", "Ошибка!");
                return;
            }

            if(conf.Repeat<1)
            {
                MessageBox.Show("Вхождение символа '" + conf.Symb + "' в цепочку не может быть кратно "+conf.Repeat, "Ошибка!");
                return;
            }

            int dir = 0;
            if (radioButton2.Checked) dir = 1;
            Grammar grammar = new Grammar(dir);
            grammar.Generate(conf.Begin, conf.End, conf.Abc, conf.Symb, conf.Repeat);
            richTextBox1.Text = grammar.ToString();

            _grammar = grammar;
            button3.Enabled = true;
        }

        private void writeGramToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(richTextBox1.Text.Length < 1)
            {
                MessageBox.Show("Грамматика не была сгенерирована", "Ошибка!");
                return;
            }
            if (saveFileDialog2.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            string file = saveFileDialog2.FileName;

            FileStream fileStream = File.OpenWrite(file);
            StreamWriter streamWriter = new StreamWriter(fileStream);
            streamWriter.Write(richTextBox1.Text);
            streamWriter.Close();
            fileStream.Close();
        }

        private void writeAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(richTextBox1.Text.Length < 1)
            {
                MessageBox.Show("Грамматика не была сгенерирована", "Ошибка!");
                return;
            }
            if (saveFileDialog2.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            string file = saveFileDialog2.FileName;

            FileStream fileStream = File.OpenWrite(file);
            StreamWriter streamWriter = new StreamWriter(fileStream);
            streamWriter.Write(richTextBox1.Text);
            streamWriter.WriteLine("\nLINES: ");
            foreach (var item in listBox1.Items)
            {
                streamWriter.WriteLine(item);
            }
            streamWriter.Close();
            fileStream.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // Генерация
            button4.Enabled = true;

            int max = 0, min = 0;
            try
            {
                max = Int32.Parse(textBox7.Text);
                min = Int32.Parse(textBox6.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!");
                return;
            }

            if (min > max)
            {
                MessageBox.Show("Минимальное значение не может быть выше максимального.", "Ошибка!");
                return;
            }
            
            listBox1.Items.Clear();
            
            _empl = new LinesEmpl(_grammar);
            try
            {
                _empl.Generate(max);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                MessageBox.Show("Превышена допустимая глубина рекурсии. Необходимо " +
                                "уменьшить максимальную границу генерируемых цепочек." +
                                "\n\nГенерация посредством цикла будет доступна в DLC", "Ошибка!");
                return;
            }

            foreach (var line in _empl.Lines)
            {
                if(line.Finished&&line.Content.Length>=min) listBox1.Items.Add(line.Content);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            // Подробный вывод
            if (_empl == null)
            {
                MessageBox.Show("Цепочки не сгенерированы.", "Ошибка!");
                return;
            }
            
            Form3 form3 = new Form3(_empl.Lines);
            form3.Show();
        }
    }
}
