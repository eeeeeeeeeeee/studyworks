﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using WindowsFormsApp1.Gr;

namespace WindowsFormsApp1
{
    public partial class Form3 : Form
    {
        private Dictionary<int, List<Line>> _list;
        
        public Form3(List<Line> lines)
        {
            _list = new Dictionary<int, List<Line>>();
            InitializeComponent();
            
            foreach (var line in lines)
            {
                if (!_list.ContainsKey(line.Stage))
                {
                    _list.Add(line.Stage, new List<Line>());
                    _list[line.Stage].Add(line);
                    comboBox1.Items.Add(line.Stage);
                }
                else
                {
                    _list[line.Stage].Add(line);
                }
            }
        }

        private void comboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            if (!_list.ContainsKey(comboBox1.SelectedIndex))
            {
                return;
            }
            
            foreach (var line in _list[comboBox1.SelectedIndex])
            {
                listBox1.Items.Add(line.Content);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(comboBox1.SelectedIndex>0) comboBox1.SelectedIndex -= 1;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(comboBox1.SelectedIndex<comboBox1.Items.Count-1) comboBox1.SelectedIndex += 1;
        }
    }
}