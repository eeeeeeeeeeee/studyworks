﻿using System;

namespace WindowsFormsApp1.Gr
{
    public class Line
    {
        private String _content;
        private int _stage;
        private bool _finished;

        public int Stage
        {
            get { return _stage; }
        }

        public string Content
        {
            get { return _content; }
        }

        public bool Finished
        {
            get { return _finished; }
        }

        public void Finish()
        {
            _finished = true;
        }

        public Line(int st, string content)
        {
            _stage = st;
            _content = content;
            _finished = false;
        }
        
        public Line(int st)
        {
            _stage = st;
            _content = "";
            _finished = false;
        }

        private void Inc()
        {
            _stage += 1;
        }

        public static Line operator +(String a, Line b)
        {
            Line line = new Line(b._stage+1, b._content);
            line._content = a + b._content;
            return line;
        }
        
        public static Line operator +(Line b, String a)
        {
            Line line = new Line(b._stage+1, b._content);
            line._content = b._content + a;
            return line;
        }
    }
}