﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.Gr
{
    public class Grammar
    {
        private List<Rule> _rules;
        private int _dir = 0;
        private string[] _abc;

        internal List<Rule> Rules
        {
            get { return _rules; }
        }

        public int Dir
        {
            get { return _dir; }
        }

        public Grammar()
        {
            Init(0);
        }
        public Grammar(int d)
        {
            Init(d);
        }

        private void Init(int d)
        {
            _rules = new List<Rule>();
            _dir = d;
        }

        private String Reverse(String text)
        {
            char[] array = text.ToCharArray();
            Array.Reverse(array);
            return new String(array);
        }

        public void Generate(string b, string e, string[] abc, string c, int r)
        {
            _abc = abc;
            if(_dir > 0)
            {
                string temp = b;
                b = e;
                e = temp;
            }

            int repeats = 0;
            foreach(char symb in b)
            {
                if (symb == c[0]) repeats++;
            }
            foreach (char symb in e)
            {
                if (symb == c[0]) repeats++;
            }
            repeats %= r;

            int charPos = -1;
            for(int i=0;i<abc.Length;i++)
            {
                if(abc[i] == c)
                {
                    charPos = i;
                    break;
                }
            }

            int n = 0;
            if (b.Length > 0)
            {
                Rule rule = new Rule(n++);
                rule.Add(new SubRule(b, repeats+n));
                _rules.Add(rule);
            }
            else
            {
                Rule rule = new Rule(n++);
                for (int j = 0; j < abc.Length; j++)
                {
                    if (j == charPos)
                    {
                        rule.Add(new SubRule(abc[j], (repeats + 1) % r + n));
                    } else
                    {
                        rule.Add(new SubRule(abc[j], repeats + n));
                    }
                }

                if (repeats == 0)
                {
                    rule.Add(new SubRule(e,-1));
                }

                _rules.Add(rule);
            }

            for (int i = 0; i < r; i++)
            {
                Rule rule = new Rule(n + i);
                for (int j = 0; j < abc.Length; j++)
                {
                    if (j == charPos)
                    {
                        rule.Add(new SubRule(abc[j], (i + 1) % r + n));
                    } else
                    {
                        rule.Add(new SubRule(abc[j], i + n));
                    }
                }

                if (i == 0)
                {
                    rule.Add(new SubRule(e,-1));
                }

                _rules.Add(rule);
            }

            
        }

        public static string TermToString(int term)
        {
            if (term < 0) return "";
            if (term == 0) return "S";
            return ((char)('A' + term - 1)).ToString();
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("G = ( {");
            for(int i=0;i<_abc.Length;i++)
            {
                stringBuilder.Append(_abc[i]);
                if(i>_abc.Length-2)
                {
                    stringBuilder.Append("}, ");
                } else
                {
                    stringBuilder.Append(", ");
                }
            }

            stringBuilder.Append("{");
            for (int i = 0; i < _rules.Count; i++)
            {
                stringBuilder.Append(TermToString(_rules[i].GetTerm()));
                if (i > _rules.Count - 2)
                {
                    stringBuilder.Append("}, ");
                }
                else
                {
                    stringBuilder.Append(", ");
                }
            }

            stringBuilder.Append("S} )\n");

            foreach (Rule element in _rules)
            {
                if(element.GetTerm() == 0)
                {
                    stringBuilder.Append("S -> ");
                } else
                {
                    stringBuilder.Append(TermToString(element.GetTerm()));
                    stringBuilder.Append(" -> ");
                }
                for(int i=0;i<element.Count;i++)
                {
                    SubRule sub = element[i];
                    String content = sub.GetContent();
                    if (content.Length < 1) content = "/e";

                    if (_dir > 0)
                    {
                        stringBuilder.Append(TermToString(sub.GetTerm()));
                        stringBuilder.Append(content);
                        if (i < element.Count - 1) stringBuilder.Append(" | ");
                    } else {
                        stringBuilder.Append(content);
                        stringBuilder.Append(TermToString(sub.GetTerm()));
                        if (i < element.Count - 1) stringBuilder.Append(" | ");
                    }
                }
                stringBuilder.Append("\n");
            }

            return stringBuilder.ToString();
        }
    }
}
