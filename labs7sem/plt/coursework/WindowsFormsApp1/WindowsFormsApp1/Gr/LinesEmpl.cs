﻿using System.Collections.Generic;

namespace WindowsFormsApp1.Gr
{
    public class LinesEmpl
    {
        private Grammar _grammar;
        private List<Line> _lines;

        public List<Line> Lines
        {
            get { return _lines; }
        }
        
        public LinesEmpl(Grammar grammar)
        {
            _grammar = grammar;
        }

        public void Generate(int max)
        {
            _lines = new List<Line>();
            Line l = new Line(0);
            Build(l, 0, max);
        }

        private void Build(Line line, int r, int max)
        {
            _lines.Add(line);
            if (line.Content.Length > max) return;
            if(r==-1)
            {
                line.Finish();
                return;
            }
            if (line.Content.Length == max) return;

            Rule rule = null;
            foreach (var rl in _grammar.Rules)
            {
                if (rl.GetTerm() == r)
                {
                    rule = rl;
                    break;
                }
            }

            if (rule == null) return;
            
            foreach (var sb in rule)
            {
                Line l = null;
                if (_grammar.Dir > 0)
                {
                    l = sb.GetContent() + line;
                }
                else
                {
                    l = line + sb.GetContent();
                }
                Build(l, sb.GetTerm(), max);
            }
        }
    }
}