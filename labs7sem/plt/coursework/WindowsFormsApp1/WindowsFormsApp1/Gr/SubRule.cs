﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.Gr
{
    internal class SubRule
    {
        private string _content;
        private int _term;

        public SubRule(string c, int t)
        {
            _content = c;
            _term = t;
        }

        public string GetContent()
        {
            return _content;
        }

        public int GetTerm()
        {
            return _term;
        }
    }
}
