﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.Gr
{
    class Rule : List<SubRule>
    {
        private readonly int _term;

        public int GetTerm()
        {
            return _term;
        }
        public Rule(int t)
        {
            _term = t;
        }
    }
}
