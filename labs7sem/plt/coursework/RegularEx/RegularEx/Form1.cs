﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using RegularEx.Lang;

namespace RegularEx
{
    public partial class Form1 : Form
    {
        private LanguageEmpl _languageEmpl;
        public Form1()
        {
            InitializeComponent();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(openFileDialog1.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            
            string file = openFileDialog1.FileName;
            string content;
            FileStream fileStream = File.OpenRead(file);
            StreamReader streamReader = new StreamReader(fileStream);
            content = streamReader.ReadToEnd();
            streamReader.Close();
            fileStream.Close();
            
            Language conf = JsonConvert.DeserializeObject<Language>(content);
            textBox2.Text = conf.Symb.ToString();
            textBox3.Text = conf.Repeat.ToString();
            textBox4.Text = conf.Begin;
            textBox5.Text = conf.End;
            richTextBox1.Text = conf.Expression;
            
            StringBuilder abcBuilder = new StringBuilder();
            foreach(char element in conf.Abc)
            {
                abcBuilder.Append(element);
            }
            textBox1.Text = abcBuilder.ToString().Trim();
        }
        
        private Language GenerateObject()
        {
            Language conf = new Language();
            conf.Symb = textBox2.Text[0];
            conf.Repeat = Int32.Parse(textBox3.Text);
            conf.Begin = textBox4.Text;
            conf.End = textBox5.Text;
            conf.Expression = richTextBox1.Text.Trim();

            conf.Abc = textBox1.Text.Trim().ToCharArray();

            return conf;
        }
        
        private void Save()
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            string file = saveFileDialog1.FileName;

            string data = null;
            try
            {
                data = JsonConvert.SerializeObject(GenerateObject());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!");
                return;
            }

            FileStream fileStream = File.OpenWrite(file);
            StreamWriter streamWriter = new StreamWriter(fileStream);
            streamWriter.Write(data);
            streamWriter.Close();
            fileStream.Close();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Вы уверены, что хотите очистить все поля формы?", "Новый...",
                    MessageBoxButtons.YesNo) == DialogResult.No)
            {
                return;
            }
            
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "0";
            textBox7.Text = "10";
            richTextBox1.Text = "";
            listBox1.Items.Clear();
            
            writeToolStripMenuItem.Enabled = false;
            button2.Enabled = false;
            textBox6.Enabled = false;
            textBox7.Enabled = false;

            _languageEmpl = null;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void writeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog2.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            string file = saveFileDialog2.FileName;

            FileStream fileStream = File.OpenWrite(file);
            StreamWriter streamWriter = new StreamWriter(fileStream);
            streamWriter.Write(richTextBox1.Text);
            streamWriter.Write("\n\n");
            foreach (var item in listBox1.Items)
            {
                streamWriter.WriteLine(item);
            }
            streamWriter.Close();
            fileStream.Close();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Написать программу, которая по предложенному описанию языка " +
                            "построит регулярное выражение, задающее этот язык, и сгенерирует " +
                            "с его помощью все цепочки языка в заданном диапазоне длин. " +
                            "Предусмотреть также возможность генерации цепочек по введённому " +
                            "пользователем РВ (в рамках варианта).\n\nАлфавит, заданные начальная и конечная " +
                            "подцепочкии кратность вхождения некоторого символа алфавита во все цепочки языка",
                "Вариант 10. Мальцев Богдан. ИП-615");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Language conf = null;
            try
            {
                conf = GenerateObject();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!");
                return;
            }
            
            foreach (char element in conf.Begin)
            {
                if (!conf.Abc.Contains(element))
                {
                    MessageBox.Show("Начальная подцепочка содежит символ '"+ element + "', не указанный в алфавите.", "Ошибка!");
                    return;
                }
            }

            foreach (char element in conf.End)
            {
                if (!conf.Abc.Contains(element))
                {
                    MessageBox.Show("Конечная подцепочка содежит символ '" + element + "', не указанный в алфавите.", "Ошибка!");
                    return;
                }
            }

            if(!conf.Abc.Contains(conf.Symb))
            {
                MessageBox.Show("Кратный символ '" + conf.Symb + "', не указан в алфавите.", "Ошибка!");
                return;
            }

            if(conf.Repeat<1)
            {
                MessageBox.Show("Вхождение символа '" + conf.Symb + "' в цепочку не может быть кратно "+conf.Repeat, "Ошибка!");
                return;
            }
            
            writeToolStripMenuItem.Enabled = true;
            button2.Enabled = true;
            textBox6.Enabled = true;
            textBox7.Enabled = true;
            
            _languageEmpl = LanguageEmpl.Generate(conf);
            richTextBox1.Text = _languageEmpl.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label9.Text = "Генерация...";
            Update();
            
            int min, max;
            try
            {
                min = Int32.Parse(textBox6.Text);
                max = Int32.Parse(textBox7.Text);

                if (max < min)
                {
                    MessageBox.Show("Минимальное значение не может быть больше максимального", "Ошибка!");
                    return;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Ошибка!");
                return;
            }
            
            listBox1.Items.Clear();
            SortedSet<string> set;
            try
            {
                set = new SortedSet<string>(_languageEmpl.GenerateStrings(max));
            }
            catch(Exception exception)
            {
                label9.Text = "Неудачно";
                MessageBox.Show(exception.Message, "Процесс прерван");
                Console.WriteLine(exception);
                return;
            }

            foreach (var line in set)
            {
                if(line.Length>=min && line.Length<=max)
                listBox1.Items.Add(line);
            }

            label9.Text = "Сгенерировано " + listBox1.Items.Count +" \\ " + set.Count + " цепочек";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                _languageEmpl = LanguageEmpl.Generate(richTextBox1.Text);
                Console.WriteLine(_languageEmpl);
            }
            catch (Exception exception)
            {
                MessageBox.Show("Не удалось сгенерировать регулярное выражение. Возможно, была допущена ошибка" +
                                " при вводе.", "Ошибка!");
                Console.WriteLine(exception);
                return;
            }
            
            writeToolStripMenuItem.Enabled = true;
            button2.Enabled = true;
            textBox6.Enabled = true;
            textBox7.Enabled = true;
        }
    }
}