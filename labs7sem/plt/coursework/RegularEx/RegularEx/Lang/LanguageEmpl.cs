﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace RegularEx.Lang
{
    public partial class LanguageEmpl
    {
        public const int SIMPLE = 0;
        public const int CON = 1;
        public const int UNION = 2;
        public const int ARRAY = 3;

        public const int CriticalSize = 30000;
        
        private List<LanguageEmpl> _langs = new List<LanguageEmpl>();
        private char _content;
        private int _type;
        private bool _full;

        private int _genRep = 0;

        public LanguageEmpl()
        {
            _type = CON;
        }

        public LanguageEmpl(int type)
        {
            _type = type;
        }
        
        public LanguageEmpl(int type, bool f)
        {
            _type = type;
            _full = f;
        }

        private void Add(LanguageEmpl l)
        {
            _langs.Add(l);
        }
        private void Add(char c)
        {
            _langs.Add(new LanguageEmpl(c));
        }
        
        public LanguageEmpl(char c)
        {
            _type = SIMPLE;
            _content = c;
        }
        
        public override string ToString()
        {
            if (_type == SIMPLE) return _content.ToString();

            string str = "";
            
            switch (_type)
            {
                case CON:
                {
                    str += "(";
                    foreach (var l in _langs)
                    {
                        str += l.ToString();
                    }
                    str += ")";

                    break;
                }

                case UNION:
                {
                    str += "(";
                    foreach (var l in _langs)
                    {
                        str += l+"|";
                    }
                    str = str.Remove(str.Length - 1);
                    str += ")";
                    break;
                }
                
                case ARRAY:
                {
                    if (_full)
                    {
                        str += _langs[0] + "*";
                    }
                    else
                    {
                        str += _langs[0] + "+";
                    }
                    break;
                }
            }

            return str;
        }
    }
}