﻿using System;
using System.Collections.Generic;

namespace RegularEx.Lang
{
    public partial class LanguageEmpl
    {
        public HashSet<string> GenerateStrings(int size)
        {
            HashSet<string> output = new HashSet<string>();

            if (_type == SIMPLE)
            {
                output.Add(_content.ToString());
                return output;
            }
            
            if (_type == CON)
            {
                output.Add("");
                int maxsize = size + 1;
                
                for (int i = 0; i < _langs.Count; i++)
                {
                    HashSet<string> l0 = _langs[i].GenerateStrings(maxsize-i);
                    HashSet<string> nlist = new HashSet<string>();
                    foreach (var line in output)
                    {
                        foreach (var s in l0)
                        {
                            if (nlist.Count > 0 && line.Length+s.Length > maxsize) continue; 
                            nlist.Add(line + s);
                        }
                    }

                    //if(nlist.Count > 0) size -= nlist.Min().Length;
                    //Console.WriteLine(nlist.Min().Length);

                    if(nlist.Count>0)
                    output = nlist;
                    
                    if (output.Count > CriticalSize)
                    {
                        throw new Exception("Сгенерировано более "+(output.Count)+" цепочек на одном ходу. " +
                                            "Дальнейшая генерация может занять слишком много памяти.");
                    }
                }

                return output;
            }
            
            if (_type == UNION)
            {
                foreach (var l in _langs)
                {
                    HashSet<string> lines = l.GenerateStrings(size);
                    
                    foreach (var line in lines)
                    {
                        if(output.Count > 0 && line.Length>size) continue;
                        output.Add(line);
                    }
                    if (output.Count > CriticalSize)
                    {
                        throw new Exception("Сгенерировано более "+output.Count+" цепочек на одном ходу. " +
                                            "Дальнейшая генерация может занять слишком много памяти.");
                    }
                }
                return output;
            }
            
            if (_type == ARRAY)
            {
                output = _langs[0].GenerateStrings(size);
                HashSet<string> buf = new HashSet<string>();
                foreach (var s in output) buf.Add(s);

                for (int i = 0; i < Math.Log2(size); i++)
                {
                    HashSet<string> nout = new HashSet<string>();

                    foreach (var o in buf)
                    {
                        if (buf.Count > 0 && o.Length > size) continue;
                        foreach (var s in output)
                        {
                            if (buf.Count > 0 && o.Length+s.Length > size) continue;
                            nout.Add(s + o);
                        }
                    }

                    if (output.Count + nout.Count > CriticalSize)
                    {
                        throw new Exception("Сгенерировано более "+(output.Count + nout.Count)+" цепочек на одном ходу. " +
                                            "Дальнейшая генерация может занять слишком много памяти.");
                    }
                    
                    foreach (var s in nout) output.Add(s);
                    buf = nout;
                }
                
                if(_full) output.Add("");
                
                return output;
                
            }

            return output;
        }
    }
}