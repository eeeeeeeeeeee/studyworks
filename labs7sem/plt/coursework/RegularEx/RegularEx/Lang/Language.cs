﻿using Newtonsoft.Json;

namespace RegularEx.Lang
{
    public class Language
    {
        [JsonProperty("Abc")]
        public char[] Abc { get; set; }

        [JsonProperty("Begin")]
        public string Begin { get; set; }

        [JsonProperty("End")]
        public string End { get; set; }

        [JsonProperty("Symb")]
        public char Symb { get; set; }

        [JsonProperty("Repeat")]
        public int Repeat { get; set; }
        
        [JsonProperty("Expression")]
        public string Expression { get; set; }
    }
}