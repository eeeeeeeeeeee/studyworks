﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace RegularEx.Lang
{
    public partial class LanguageEmpl
    {
        private static LanguageEmpl GenerateUnion(Language conf)
        {
            LanguageEmpl ar = new LanguageEmpl(ARRAY, true);
            LanguageEmpl un = new LanguageEmpl(UNION);

            foreach (char s in conf.Abc)
            {
                if(s == conf.Symb) continue;
                LanguageEmpl l = new LanguageEmpl(s);
                un.Add(l);
            }
            
            ar.Add(un);
            return ar;
        }

        // Генерация на основе конфигурации
        public static LanguageEmpl Generate(Language conf)
        {
            int incl = 0;
            for (int i = 0; i < conf.Begin.Length && i < conf.End.Length; i++)
            {
                if (conf.Begin[conf.Begin.Length - 1 - i] == conf.End[0]) incl += 1;
                else break;
            }
            
            int repeat = conf.Repeat;
            int r = 0;
            foreach (var s in conf.Begin)
            {
                if (s == conf.Symb) r++;
            }
            foreach (var s in conf.End)
            {
                if (s == conf.Symb) r++;
            }

            if (r > 0 && r < repeat) r = repeat - r;
            else r %= repeat;
            
            LanguageEmpl lang = new LanguageEmpl(CON);
            foreach (var s in conf.Begin)
            {
                lang.Add(s);
            }
            lang.Add(GenerateUnion(conf));

            if (r > 0)
            {
                LanguageEmpl con = new LanguageEmpl(CON);
                con.Add(GenerateUnion(conf));
                for (int i = 0; i < r; i++)
                {
                    con.Add(conf.Symb);
                    con.Add(GenerateUnion(conf));
                }
                lang.Add(con);
            }
            
            LanguageEmpl midArray = new LanguageEmpl(ARRAY, true);
            LanguageEmpl mid = new LanguageEmpl(CON);
            for (int i = 0; i < repeat; i++)
            {
                mid.Add(conf.Symb);
                mid.Add(GenerateUnion(conf));
            }
            midArray.Add(mid);
            
            LanguageEmpl end = new LanguageEmpl(CON);
            foreach (var s in conf.End)
            {
                end.Add(s);
            }

            lang.Add(midArray);
            
            if(conf.End.Length>0)
            lang.Add(end);

            if (incl > 0 && r == 0)
            {
                LanguageEmpl l = new LanguageEmpl(CON);
                for (int i = 0; i < conf.Begin.Length-incl; i++)
                {
                    l.Add(conf.Begin[i]);
                }
                foreach (var s in conf.End)
                {
                    l.Add(s);
                }
                LanguageEmpl un = new LanguageEmpl(UNION);
                un.Add(l);
                un.Add(lang);
                lang = un;
            }
            
            return lang;
        }

        // Генерация на основе строки
        public static LanguageEmpl Generate(string str)
        {
            LanguageEmpl lang = new LanguageEmpl(CON);
            lang._genRep = 0;
            
            Console.WriteLine("STR: "+str);
            for(int i=0;i<str.Length;i++)
            {
                lang._genRep++;
                    
                char c = str[i];
                if (c == '(')
                {
                    LanguageEmpl buf  = Generate(str.Substring(i + 1));
                    i+=buf._genRep;
                    lang._genRep += buf._genRep;
                    lang.Add(buf);
                    continue;
                }

                if (c == '|')
                {
                    lang._type = UNION;
                    continue;
                }

                if (c == '*' || c == '+')
                {
                    LanguageEmpl buf = lang._langs[^1];
                    lang._langs.Remove(buf);
                    
                    LanguageEmpl l = new LanguageEmpl();
                    l._type = ARRAY;

                    if (c == '*') l._full = true;
                    else l._full = false;
                    
                    l.Add(buf);
                    lang.Add(l);
                    continue;
                }

                if (c == ')')
                {
                    if (str.Length > i + 1)
                    {
                        if (str[i + 1] == '*')
                        {
                            LanguageEmpl l = new LanguageEmpl();
                            l._type = ARRAY;
                            l._full = true;
                            l.Add(lang);
                            l._genRep += lang._genRep+1;
                            lang = l;
                        } else if (str[i + 1] == '+')
                        {
                            LanguageEmpl l = new LanguageEmpl();
                            l._type = ARRAY;
                            l._full = false;
                            l.Add(lang);
                            l._genRep += lang._genRep+1;
                            lang = l;
                        }
                    }
                    
                    break;
                }
                
                lang.Add(c);
            }

            Console.WriteLine("OUT: "+lang);
            return lang;
        }

    }
}