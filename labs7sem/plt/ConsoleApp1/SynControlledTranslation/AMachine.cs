﻿using System;
using System.Collections.Generic;
using System.Text;
using Rule = SynControlledTranslation.AConfig.Rule;

namespace SynControlledTranslation
{
    public class AMachine
    {
        private AConfig _config;
        private String _state;
        private String _stack;
        private String _trans;
        private String _in;
        private String _path ;

        public AMachine(AConfig config)
        {
            _config = config;
            _state = config.StartState;
            _path = "";
            _trans = "";
        }

        private String ReportState(int rule)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("(");
            stringBuilder.Append(_state);
            stringBuilder.Append(", ");
            String sIn = _in;
            if (_in == "") sIn = "~";
            stringBuilder.Append(sIn);
            stringBuilder.Append(", ");
            String sOut = _stack;
            if (_stack == "") sOut = "~";
            stringBuilder.Append(sOut);
            stringBuilder.Append(",");
            sOut = _trans;
            if (_trans == "") sOut = "~";
            stringBuilder.Append(sOut);
            stringBuilder.Append(")");

            if (rule > -1)
            {
                stringBuilder.Append(" ├ [");
                stringBuilder.Append(rule);
                stringBuilder.Append("] ");
            }

            return stringBuilder.ToString();
        }

        private bool ContainsInFinal(String state)
        {
            foreach (var st in _config.FinalStates)
            {
                if (st == state)
                {
                    return true;
                }
            }

            return false;
        }
        
        public void Run(String str)
        {
            String stack = "" + _config.InitStackSymbol;
            String start = _config.StartState;
            String trans = "";

            try
            {
                String outstr = Run(str, stack, start, trans)[0];
                if (outstr != null) Form1.WriteLine(outstr);
                else Form1.WriteLine("failed");
            }
            catch(Exception e)
            {
                Form1.WriteLine(e.Message);
            }
        }
        public String[] Run(String str, String stack, String start, String trans)
        {
            _stack = stack;
            _state = start;
            _in = str;
            _path = "";
            _trans = trans;

            //Console.Write("Start automate for ");
            //ReportState(-1);
            
            while (_stack.Length > 0)
            {
                Rule rule = null;
                int rulesCount = 0;
                int ruleIndex = 0;

                for(int i=0;i<_config.Rules.Length;i++)
                {
                    var r = _config.Rules[i];
                    
                    //if(r.InSymbol.Length<1 && _in.Length>0) continue;

                    if (_in.Length > 0 && r.InSymbol.Length > 0 && r.InState == _state && r.InSymbol[0] == _in[0] && r.InSymbolStack[0] == _stack[0])
                    {
                        rulesCount += 1;
                        rule = r;
                        ruleIndex = i+1;
                        break;
                    }
                    
                    if (r.InState == _state && r.InSymbol.Length < 1 && r.InSymbolStack.Length > 0 && r.InSymbolStack[0] == _stack[0])
                    {
                        rulesCount += 1;
                        rule = r;
                        ruleIndex = i+1;
                        break;
                    }
                }
                
                // Update path
                _path += ReportState(ruleIndex);
                
                //if(rulesCount>1) {throw new Exception(_path+" {failed. Dual rules exception}");}
                if(rule == null) {throw new Exception(_path+" {failed. No rules exception}");}

                if (rule.OutState.Length > 1)
                {
                    String s = null;
                    String tr = null;
                    int validIndex = -1;
                    for (int i = 0; i < rule.OutState.Length; i++)
                    {
                        String stat = rule.OutState[i];
                        String input = new String(_in);
                        if(rule.InSymbol.Length > 0) input = input.Substring(1);
                        String stck = new String(_stack);
                        stck = stck.Substring(1);
                        stck = rule.OutSymbols[i] + stck;
                        String trns = _trans + rule.OutTrans[i];
                        
                        AMachine aMachine = new AMachine(_config);
                        try
                        {
                            string[] ss = aMachine.Run(input, stck, stat, trns);
                            s = ss[0];
                            tr = ss[1];
                            validIndex = i;
                        }
                        catch (Exception)
                        {
                            //Form1.WriteLine(e.Message);
                        }
                    }

                    if (validIndex < 0)
                    {
                        string[] ret = new string[2];
                        ret[0] = _path + " {failed. No sub rules exception}";
                        ret[1] = _trans;
                        return ret;
                    }
                    string[] rt = new string[2];
                    rt[0] = _path + "["+(validIndex+1)+"] " + s;
                    rt[1] = tr+_trans;
                    return rt;
                }
                
                _state = rule.OutState[0];
                if(rule.InSymbol.Length > 0 && _in.Length>0) _in = _in.Substring(1);
                _stack = rule.OutSymbols[0]+_stack.Substring(1);
                _trans = rule.OutTrans[0]+_trans;

                //_path += ReportState(-1);
            }

            if (_stack.Length > 0) {
                string[] ret = new string[2];
                ret[0] = _path + ReportState(-1) + " {failed. Stack has symbols}";
                ret[1] = _trans;
                return ret;
            } 
            if (_in.Length > 0) {
                string[] ret = new string[2];
                ret[0] = _path + ReportState(-1) + " {failed. Input has symbols}";
                ret[1] = _trans;
                return ret;
            }
            string[] rt1 = new string[2];
            rt1[0] = _path + ReportState(-1) + " {successful}";
            rt1[1] = _trans;
            return rt1;
        }
    }
}