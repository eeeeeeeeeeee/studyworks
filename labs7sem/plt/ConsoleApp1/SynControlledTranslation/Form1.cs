﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace SynControlledTranslation
{
    public partial class Form1 : Form
    {
        static string DisplayMachine(AConfig conf)
        {
            string ouputstr = "";
            
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("P = ({ ");
            foreach (var s in conf.States)
            {
                stringBuilder.Append(s);
                stringBuilder.Append(" ");
            }
            stringBuilder.Append("}, { ");
            foreach (var s in conf.InAbc)
            {
                stringBuilder.Append(s);
                stringBuilder.Append(" ");
            }
            stringBuilder.Append("}, { ");
            foreach (var s in conf.StackAbc)
            {
                stringBuilder.Append(s);
                stringBuilder.Append(" ");
            }
            stringBuilder.Append("}, { ");
            foreach (var s in conf.TransAbc)
            {
                stringBuilder.Append(s);
                stringBuilder.Append(" ");
            }
            stringBuilder.Append("}, S, ");
            stringBuilder.Append(conf.StartState);
            stringBuilder.Append(", ");
            stringBuilder.Append(conf.InitStackSymbol);
            stringBuilder.Append(", { ");
            foreach (var s in conf.FinalStates)
            {
                stringBuilder.Append(s);
                stringBuilder.Append(" ");
            }
            stringBuilder.Append("})");
            ouputstr += stringBuilder + "\n";

            int i = 0;
            foreach (var r in conf.Rules)
            {
                i++;
                StringBuilder sb = new StringBuilder();
                sb.Append(i);
                sb.Append(") S( ");
                sb.Append(r.InState);
                sb.Append(", ");
                String inSymb = r.InSymbol;
                if (inSymb == "") inSymb = "~";
                sb.Append(inSymb);
                sb.Append(", ");
                sb.Append(r.InSymbolStack);
                sb.Append(" ) ├ { ");
                for (int j = 0; j < r.OutSymbols.Length; j++)
                {
                    sb.Append("( ");
                    sb.Append(r.OutState[j]);
                    sb.Append(", ");
                    String symb = r.OutSymbols[j];
                    if (symb == "") symb = "~";
                    sb.Append(symb);
                    sb.Append(", ");
                    // Out Trans
                    symb = r.OutTrans[j];
                    if (symb == "") symb = "~";
                    sb.Append(symb);
                    sb.Append(") ");
                }
                sb.Append("}");
                ouputstr += sb + "\n";
            }

            return ouputstr;
        }

        private AConfig _info;
        private static RichTextBox _textbox;
        public Form1()
        {
            InitializeComponent();
            _info = new AConfig();
            _textbox = richTextBox1;
        }

        public static void WriteLine(string str)
        {
            _textbox.Text += str+"\n";
        }

        // LOAD
        private void button3_Click(object sender, EventArgs e)
        {
            FileStream file = new FileStream("conf.json", FileMode.Open);
            StreamReader reader = new StreamReader(file);
            
            string json = reader.ReadToEnd();
            
            reader.Close();
            file.Close();
            
            _info = JsonConvert.DeserializeObject<AConfig>(json);
        }

        // RUN
        private void button2_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            AMachine machine = new AMachine(_info);
            richTextBox1.Text += DisplayMachine(_info);

            String userStr = textBox2.Text.Trim();
            if(userStr.Length < 1 || userStr[0] == '\n') return;
            try
            {
                machine.Run(userStr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                //throw;
            }
        }

        //DELETE
        private void button5_Click(object sender, EventArgs e)
        {
            throw new System.NotImplementedException();
        }
    }
}