﻿using System;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using PushdownAutomaton.res;

namespace PushdownAutomaton
{
    class Program
    {
        static void DisplayMachine(AConfig conf)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("P = ({ ");
            foreach (var s in conf.States)
            {
                stringBuilder.Append(s);
                stringBuilder.Append(" ");
            }
            stringBuilder.Append("}, { ");
            foreach (var s in conf.InAbc)
            {
                stringBuilder.Append(s);
                stringBuilder.Append(" ");
            }
            stringBuilder.Append("}, { ");
            foreach (var s in conf.StackAbc)
            {
                stringBuilder.Append(s);
                stringBuilder.Append(" ");
            }
            stringBuilder.Append("}, S, ");
            stringBuilder.Append(conf.StartState);
            stringBuilder.Append(", ");
            stringBuilder.Append(conf.InitStackSymbol);
            stringBuilder.Append(", { ");
            foreach (var s in conf.FinalStates)
            {
                stringBuilder.Append(s);
                stringBuilder.Append(" ");
            }
            stringBuilder.Append("})");
            Console.WriteLine(stringBuilder.ToString());

            int i = 0;
            foreach (var r in conf.Rules)
            {
                i++;
                StringBuilder sb = new StringBuilder();
                sb.Append(i);
                sb.Append(") S( ");
                sb.Append(r.InState);
                sb.Append(", ");
                String inSymb = r.InSymbol;
                if (inSymb == "") inSymb = "~";
                sb.Append(inSymb);
                sb.Append(", ");
                sb.Append(r.InSymbolStack);
                sb.Append(" ) ├ { ");
                for (int j = 0; j < r.OutSymbols.Length; j++)
                {
                    sb.Append("( ");
                    sb.Append(r.OutState[j]);
                    sb.Append(", ");
                    String symb = r.OutSymbols[j];
                    if (symb == "") symb = "~";
                    sb.Append(symb);
                    sb.Append(") ");
                }
                sb.Append("}");
                Console.WriteLine(sb.ToString());
            }
        }
        
        static void Main(string[] args)
        {
            FileStream file = new FileStream("conf.json", FileMode.Open);
            StreamReader reader = new StreamReader(file);
            
            string json = reader.ReadToEnd();
            
            reader.Close();
            file.Close();
            
            AConfig info = JsonConvert.DeserializeObject<AConfig>(json);
            AMachine machine = new AMachine(info);

            DisplayMachine(info);
            
            while (true)
            {
                Console.WriteLine("\nInput your string: ");
                String userStr = Console.ReadLine(); 
                if(userStr.Length < 1 || userStr[0] == '\n') break;
                try
                {
                    machine.Run(userStr);
                }
                catch (Exception e)
                {
                    //Console.WriteLine(e.Message);
                    //throw;
                }
            }

        }
    }
}