﻿using Newtonsoft.Json;

namespace PushdownAutomaton
{
    public class AConfig
    {
        public class Rule
        {
            [JsonProperty("InState")]
            public string InState { get; set; }
            
            [JsonProperty("InSymbol")]
            public string InSymbol { get; set; }
            
            [JsonProperty("InSymbolStack")]
            public string InSymbolStack { get; set; }
            
            [JsonProperty("OutState")]
            public string[] OutState { get; set; }
            
            [JsonProperty("OutSymbols")]
            public string[] OutSymbols { get; set; }
        }
        
        [JsonProperty("States")]
        public string[] States { get; set; }

        [JsonProperty("InAbc")]
        public string[] InAbc { get; set; }

        [JsonProperty("StackAbc")]
        public string[] StackAbc { get; set; }
        
        [JsonProperty("StartState")]
        public string StartState { get; set; }
        
        [JsonProperty("InitStackSymbol")]
        public string InitStackSymbol { get; set; }
        
        [JsonProperty("FinalStates")]
        public string[] FinalStates { get; set; }
        
        [JsonProperty("Rules")]
        public Rule[] Rules { get; set; }
    }
}