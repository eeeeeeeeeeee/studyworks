﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            FileStream file = new FileStream("input.json", FileMode.Open);
            StreamReader reader = new StreamReader(file);
            string json = reader.ReadToEnd();

            Console.WriteLine(json);

            MConfig info = JsonConvert.DeserializeObject<MConfig>(json);

            Console.WriteLine("Input line: ");
            String line = Console.ReadLine();
            Machine m = new Machine(info);
            while(line != "")
            {
                try
                {
                    Console.WriteLine();
                    m.Clean();
                    m.Run(line);
                    // Console.WriteLine("Succesfull!\nRoute: "+m.Path);
                } catch(Exception)
                {
                    Console.WriteLine();
                }
                line = Console.ReadLine();
            }
        }
    }
}
