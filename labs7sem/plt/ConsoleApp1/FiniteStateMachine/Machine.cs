﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp1
{
    class Machine
    {
        private int _state;
        private readonly MConfig _conf;
        private readonly Dictionary<string, int> _states = new Dictionary<string, int>();
        private readonly Dictionary<string, int> _abc = new Dictionary<string, int>();

        public String Path { get; set; }

        public Machine(MConfig conf)
        {
            Path = "";
            
            _conf = conf;
            _state = 0;

            for (int i = 0; i < conf.Nodes.Length; i++)
            {
                _states.Add(conf.Nodes[i], i);
            }
            
            for (int i = 0; i < conf.Abc.Length; i++)
            {
                _abc.Add(conf.Abc[i], i);
            }
        }

        public void Clean()
        {
            _state = 0;
            Path = "";
        }

        private bool Go(string let)
        {
            int hor = 0;
            try
            {
                hor = _abc[let];
            }
            catch
            {
                Console.WriteLine("## Invalid character");
                return true;
            }

            int vert = _state;

            if (_conf.Matrix[vert, hor] == "-")
            {
                Console.WriteLine("## Invalid route");
            }
            _state = _states[_conf.Matrix[vert, hor]];

            Path += _conf.Matrix[vert, hor] + " ";

            return false;
        }

        private void WriteState(string[] letters, int n)
        {
            Console.Write("({0}, ", _conf.Nodes[_state]);
            for (int i = n; i < letters.Length; i++)
            {
                Console.Write(letters[i]);
            }

            Console.Write(")");
        }
        
        public void Run(string line)
        {
            line += " L";
            
            string[] letters = line.Split(' ');
            int n = 0;
            WriteState(letters, 0);
            foreach (string let in letters)
            {
                Console.WriteLine(" ├ ");
                if(Go(let)) return;
                WriteState(letters, ++n);
            }
            
            Console.WriteLine();
        }
    }
}
