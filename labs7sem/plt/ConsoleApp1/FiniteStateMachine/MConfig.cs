﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class MConfig
    {
        [JsonProperty("abc")]
        public string[] Abc { get; set; }

        [JsonProperty("nodes")]
        public string[] Nodes { get; set; }

        [JsonProperty("matrix")]
        public string[,] Matrix { get; set; }
    }
}
