﻿using Newtonsoft.Json;

namespace ConsoleApp2
{
    public class LConfig
    {
        [JsonProperty("G")]
        public string[] G { get; set; }
        
        [JsonProperty("Z")]
        public string[] Z { get; set; }
        
        [JsonProperty("P")]
        public string[][] P { get; set; }
    }
}