﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            string json = "";

            try
            {
                FileStream fileStream = new FileStream("z.json", FileMode.Open);
                StreamReader reader = new StreamReader(fileStream);

                json = reader.ReadToEnd();

                reader.Close();
                fileStream.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message, e.Source);
                return;
            }

            LConfig lConfig = null;
            try
            {
                lConfig = JsonConvert.DeserializeObject<LConfig>(json);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message, e.Source);
                return;
            }

            Console.WriteLine(json);

            Console.WriteLine("Input min:");
            int min = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Input max:");
            int max = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine(max);
            
            if (min < 1) min = 1;
            if (max < min) max = min;

            Console.WriteLine("Building...");
            
            Builder builder = new Builder(lConfig,max);
            Console.WriteLine(builder.build("",0));

            int i = 0;
            List<String> let = new List<string>();
            foreach (var e in builder.letters)
            {
                if (e.Length >= min && e.Length <= max)
                {
                    let.Add(e);
                    Console.WriteLine((i++) +") " + e);
                }
            }

            while (true)
            {
                Console.WriteLine("Build tree for: ");
                String inpt = Console.ReadLine();
                if(inpt == "") break;
                int id = Convert.ToInt32(inpt);

                Console.WriteLine(let[id]);
                TreeEmpl tree = new TreeEmpl(lConfig);
                tree.Build(let[id]);
                tree.Write();
            }
            
        }
    }
}