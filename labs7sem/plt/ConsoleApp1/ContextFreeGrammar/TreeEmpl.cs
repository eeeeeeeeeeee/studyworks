﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp2
{
    public class TreeEmpl
    {
        private LConfig _conf;
        private List<TNode> _roots;

        public TreeEmpl(LConfig conf)
        {
            _conf = conf;
            _roots = new List<TNode>();
        }

        public void Build(string line)
        {
            Route(new TNode("", 0, null), line);
        }

        public void Write()
        {
            foreach (var node in _roots)
            {
                string output = "";
                TNode p = node;
                while (p != null)
                {
                    if(p.Unterm < 0) output = p.Content;
                    else output = p.Content+_conf.G[p.Unterm] + " => " + output;
                    p = p.Prev;
                }
                
                Console.WriteLine(output);
            }
        }

        private int GetUnterm(char ch)
        {
            for (int i = 0; i < _conf.G.Length; i++)
            {
                if (_conf.G[i][0] == ch) return i;
            }

            return -1;
        }

        private void Route(TNode node, string target)
        {
            //Console.WriteLine(target+" "+node.Content+" "+node.Unterm);
            if (node.Unterm < 0)
            {
                _roots.Add(node);
                return;
            }
            
            //if (node.Content.Length > target.Length) return;
            

            for (int i = 0; i < _conf.P[node.Unterm].Length; i++)
            {
                string line = "";
                bool containsUnterm = false;
                for (int j = 0; j < _conf.P[node.Unterm][i].Length; j++)
                {
                    if (_conf.G.Contains(_conf.P[node.Unterm][i][j].ToString()))
                    {
                        containsUnterm = true;

                        if (target.IndexOf(line) != 0) break;
                        
                        int unterm = GetUnterm(_conf.P[node.Unterm][i][j]);
                        Route(new TNode(node.Content+line, unterm, node), target.Substring(line.Length));
                        break;
                    }
                    line += _conf.P[node.Unterm][i][j];
                }
                
                if (!containsUnterm)
                {
                    if (target.IndexOf(line) != 0) break;
                    Route(new TNode(node.Content+line, -1, node),  target.Substring(line.Length));
                }
            }
        }
    }
}