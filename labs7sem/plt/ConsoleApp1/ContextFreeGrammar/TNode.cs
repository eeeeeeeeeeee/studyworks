﻿using System;
using System.Collections.Generic;

namespace ConsoleApp2
{
    public class TNode
    {
        private TNode _prev;
        private string _content;
        private int _unterm;

        public int Unterm => _unterm;
        public TNode Prev => _prev;
        public string Content => _content;

        public TNode(string content, int unterm, TNode prev)
        {
            _content = content;
            _unterm = unterm;
            _prev = prev;
        }
    }
}