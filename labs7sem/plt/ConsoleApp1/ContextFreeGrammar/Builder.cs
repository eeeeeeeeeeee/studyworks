﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ConsoleApp2
{
    public class Builder
    {

        private LConfig conf;
        private Dictionary<char, int> G;
        private HashSet<char> Z;
        private int length;

        public List<string> letters { get; set; }

        public Builder(LConfig conf, int length)
        {
            letters = new List<string>();
            
            G = new Dictionary<char, int>();
            Z = new HashSet<char>();
            this.conf = conf;
            this.length = length;

            for (int i=0;i<conf.G.Length;i++)
            {
                G.Add(conf.G[i][0],i);
            }

            foreach (var e in conf.Z)
            {
                Z.Add(e[0]);
            }
        }

        public string build(string str, int pos)
        {
            if (str.Length > length) return str;

            for (int i = 0; i < conf.P[pos].Length; i++)
            {
                string rule = conf.P[pos][i];
                string nStr = str;

                bool f = true;
                foreach (char ch in rule)
                {
                    /*if (ch == '&')
                    {
                        letters.Add(nStr);
                        break;
                    }*/
                
                    if (Z.Contains(ch))
                    {
                        nStr += ch;
                        continue;
                    }
                    
                    if (G.ContainsKey(ch))
                    {
                        f = false;
                        build(nStr, G[ch]);
                        break;
                    } 
                    
                    throw new Exception("Error. Character: "+ch);

                }

                if(f) letters.Add(nStr);
            }
           

            return str;
        }
    }
}