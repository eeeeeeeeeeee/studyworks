﻿using System;
using System.Numerics;
using Encrypt;
using Encrypt.sign;

namespace ConsoleApp10
{
    class Program
    {
        static void Main(string[] args)
        {
            Vertex[] keys = SignElGamal.GenerateKeys( "source.txt");
            BigInteger p = keys[0].x;
            BigInteger g = keys[0].y;
            BigInteger y = keys[0].z;
            BigInteger r = keys[1].x;
            BigInteger s = keys[1].y;
            
            SignElGamal.SignFile("source.txt","signed.txt", r, s);
            SignElGamal.CheckFile("signed.txt", y, g, p);
        }
    }
}