﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Threading.Channels;
using Encrypt;

namespace ConsoleApp12
{
    class Program
    {
        static void Main(string[] args)
        {
            BigInteger p = Randomizer.GenerateP(Randomizer.RandomBigInteger(20));
            BigInteger q = Randomizer.GenerateP(Randomizer.RandomBigInteger(20));
            BigInteger n = p*q;
            BigInteger f = (p-1)*(q-1);
            
            
            int nCards = 0, nPlayers = 0;
            Console.WriteLine("Number of cards [48]: ");
            while (nCards < 5+4 || nCards > 48)
            {
                String str = Console.ReadLine();
                if (str.Length < 1)
                {
                    nCards = 48;
                    break;
                }
                nCards = Convert.ToInt32(str);
            }

            Console.WriteLine("Number of players [4]: ");
            while (nPlayers < 2 || nPlayers > (nCards-5)/2)
            {
                String str = Console.ReadLine();
                if (str.Length < 1)
                {
                    nPlayers = 4;
                    break;
                }
                nPlayers = Convert.ToInt32(str);
            }
            
            List<BigInteger> cards = new List<BigInteger>();
            for (int i = 0; i < nCards; i++)
            {
                cards.Add(i + 2);
            }
            
            List<Player> players = new List<Player>();
            for (int i = 0; i < nPlayers; i++)
            {
                Player player = new Player(n,f);
                players.Add(player);
                player.Mix(cards);
            }

            // Game
            Console.WriteLine("\nCards on table: ");
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine(
                    Cards.GetCardName(players[0].Decrypt(cards[i], 
                    players)));
            }

            Console.WriteLine("\nCards on hand: ");
            for (int i = 5; i < 7; i++)
            {
                Console.WriteLine(
                    Cards.GetCardName(players[0].Decrypt(cards[i], 
                        players)));
            }

            Console.WriteLine("\nPress X to open");
            Console.ReadKey();
            Console.WriteLine();
            for (int i = 0; i < players.Count; i++)
            {
                Console.WriteLine("Player "+(i+1)+" cards: ");
                for (int j = 0; j < 2; j++)
                {
                    Console.Write(
                        Cards.GetCardName(players[i].Decrypt(cards[i*2+5+j], 
                        players))+"\n");
                }
                Console.WriteLine();
            }
            
        }
    }
}