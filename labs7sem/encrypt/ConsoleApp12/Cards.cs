﻿using System;
using System.Numerics;
using System.Text;

namespace ConsoleApp12
{
    public static class Cards
    {
        public static String GetCardName(BigInteger n)
        {
            int buf = (int) n;
            n -= 2;
            
            int m = 0;
            while (n - 13 > 0)
            {
                m += 1;
                n -= 13;
            }
            StringBuilder stringBuilder = new StringBuilder();
            switch (m)
            {
                case 1:
                    stringBuilder.Append("spade ");
                    break;
                case 2:
                    stringBuilder.Append("club ");
                    break;
                case 3:
                    stringBuilder.Append("diamond ");
                    break;
                default:
                    stringBuilder.Append("heart ");
                    break;
            }

            if (n < 11 && n > 0)
            {
                stringBuilder.Append(n+1);
            }
            else
            {
                switch ((int)n)
                {
                    case 0:
                        stringBuilder.Append("ace");
                        break;
                    case 11:
                        stringBuilder.Append("jack");
                        break;
                    case 12:
                        stringBuilder.Append("queen");
                        break;
                    case 13:
                        stringBuilder.Append("king");
                        break;
                }
            }

            stringBuilder.Append(" [");
            stringBuilder.Append(buf);
            stringBuilder.Append("]");
            
            return stringBuilder.ToString();
        }
    }
}