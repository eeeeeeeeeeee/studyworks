﻿using System;
using System.Collections.Generic;
using System.Numerics;
using Encrypt;

namespace ConsoleApp12
{
    public class Player
    {
        private BigInteger _n, _d, _c;

        public Player(BigInteger n, BigInteger f)
        {
            _n = n;
            _d = Randomizer.RandomBigInteger(200)%f;
            while (Utility.Euclid(_d,f)[0]!=1)
            {
                _d += 1;
            }

            _c = MathUtils.Inverse(_d, f);
        }

        public BigInteger Encrypt(BigInteger m)
        {
            return MathUtils.PowMod(m, _d, _n);
        }
        
        public BigInteger Decrypt(BigInteger m)
        {
            return MathUtils.PowMod(m, _c, _n);
        }

        public BigInteger Decrypt(BigInteger m, List<Player> players)
        {
            foreach (var pl in players)
            {
                if(pl == this) continue;
                m = pl.Decrypt(m);
            }

            return Decrypt(m);
        }

        public void Mix(List<BigInteger> cards)
        {
            if(cards.Count < 1) return;
            
            for (int i = 0; i < cards.Count; i++)
            {
                cards[i] = Encrypt(cards[i]);
            }
            
            Random random = new Random();
            for (int i = 0; i < cards.Count; i++)
            {
                int t = random.Next(cards.Count);
                BigInteger buf = cards[t];
                cards[t] = cards[i];
                cards[i] = buf;
            }
        }
    }
}