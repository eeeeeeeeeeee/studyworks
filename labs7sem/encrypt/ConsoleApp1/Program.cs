﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ConsoleApp1 {
    class Program {
        static void Main(string[] args) {
            ulong a, x, p;
            StreamReader f = new StreamReader("input_1_0.txt");
            a = Convert.ToUInt64(f.ReadLine());
            x = Convert.ToUInt64(f.ReadLine());
            p = Convert.ToUInt64(f.ReadLine());
            f.Close();
            
            a = (ulong) Utility.RandomSeed64(1000000000);
            x = (ulong) Utility.RandomSeed64(1000000000);
            p = (ulong) Utility.RandomSeed64(1000000000);
            
            Console.Out.WriteLine("Input:\na = {0}; x = {1}; p = {2};", a, x, p);
            Console.Out.WriteLine("Teoretical complexity (mult) <= {0}", 2*Math.Log(x,2));
            
            List<ulong> list = new List<ulong>(); // Список множителей (для вывода)
            int n = 0;                            // Показатель трудоемкости
            
            ulong y = 1, s = a;
            int gradle = 1;                       // Степень (только для вывода)
            do {
                if ((x & 1) > 0) {
                    y = y * s % p;
                    n++;
                    list.Add(s);    // не используется для вычислений
                } else {
                    list.Add(1); // не используется для вычислений
                }
                
                x >>= 1;

                if (!(x > 0)) break;
                s = s * s % p;
                n++;
                
                Console.Out.WriteLine("{0}^{1} mod {2} = [({0}^{3} mod {2}) * ({0}^{3} mod {2})] mod {2} = {4};",
                    a, gradle<<1, p, gradle, s);
                gradle <<= 1;
            } while (true);

            Console.Out.Write("y = ");
            for(int j=0;j<list.Count-1;j++) {
                Console.Out.Write(list[j]+" * ");
            }
            Console.Out.Write(list[list.Count-1] + " mod " + p + " = "+y+"\n");
            Console.Out.WriteLine("Complexity = {0}", n);
        }
    }
}