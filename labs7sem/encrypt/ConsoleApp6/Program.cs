﻿using System;
using System.IO;
using System.Numerics;
using Encrypt;
using Encrypt.encrypt;

namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            int of2 = 200;
            
            BigInteger p = Randomizer.GenerateP(Randomizer.RandomBigInteger(of2));
            BigInteger g = Randomizer.RandomBigInteger(of2) % p;
            g = Prime.GetPrime(g);

            BigInteger x = Randomizer.RandomBigInteger(of2) % p;
            x = Prime.GetPrime(x);
            
            BigInteger y = MathUtils.PowMod(g, x, p);
            
            BigInteger k = Randomizer.RandomBigInteger(of2) % p-1;
            k = Prime.GetPrime(k);
            
            BigInteger a = MathUtils.PowMod(g, k, p);
            
            Console.WriteLine("input:");
            String inputFile = Console.ReadLine();
            String outputFile = Console.ReadLine();
            
            FileStream wStream = new FileStream("keys.txt", FileMode.Create);
            StreamWriter writer = new StreamWriter(wStream);
            writer.WriteLine("p: "+p);
            writer.WriteLine("g: "+g);
            writer.WriteLine("x: "+x);
            writer.WriteLine("y: "+y);
            writer.WriteLine("k: "+k);
            writer.WriteLine("a: "+a);
            writer.Close();
            wStream.Close();
            
            // X1 =====================================================
            byte[] bytes = File.ReadAllBytes(inputFile);
            Elgamal encrypt = new Elgamal(p, a, y, k, x);
            encrypt.EncryptToFile("en.txt",  bytes);
            encrypt.DectryptFromFile("en.txt",outputFile);
        }
    }
}