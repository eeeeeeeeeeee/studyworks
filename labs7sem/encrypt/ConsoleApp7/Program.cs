﻿using System;
using System.IO;
using System.Numerics;
using Encrypt;
using Encrypt.encrypt;

namespace ConsoleApp7
{
    class Program
    {
        static void Main(string[] args)
        {
            int of2 = 200;
            BigInteger p = Randomizer.GenerateP(Randomizer.RandomBigInteger(of2));
            BigInteger q = Randomizer.GenerateP(Randomizer.RandomBigInteger(of2));
            BigInteger n = p * q;
            
            BigInteger f = (p - 1) * (q - 1);
            BigInteger d = Randomizer.RandomBigInteger(of2)%f;
            while (Utility.Euclid(d,f)[0]!=1)
            {
                d += 1;
            }

            BigInteger c = MathUtils.Inverse(d, f);

            Console.WriteLine("input:");
            String inputFile = Console.ReadLine();
            String outputFile = Console.ReadLine();
            
            FileStream wStream = new FileStream("keys.txt", FileMode.Create);
            StreamWriter writer = new StreamWriter(wStream);
            writer.WriteLine("p: "+p);
            writer.WriteLine("n: "+n);
            writer.WriteLine("d: "+d);
            writer.WriteLine();
            writer.WriteLine("c: "+c);
            writer.WriteLine();
            writer.WriteLine("f: "+f);
            writer.WriteLine("q: "+q);
            writer.Close();
            wStream.Close();
            
            byte[] bytes = File.ReadAllBytes(inputFile);
            Rsa encrypt = new Rsa(p, n, d, c);
            encrypt.EncryptToFile("en.txt",  bytes);
            encrypt.DectryptFromFile("en.txt",outputFile);

        }
    }
}