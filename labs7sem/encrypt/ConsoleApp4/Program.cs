﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace ConsoleApp4 {
    class Program {
        public static ulong N = 0;
        
        static void Main(string[] args) {
            //10000000000
            BigInteger a;
            BigInteger p;
            BigInteger q = Encrypt.Randomizer.RandomBigInteger(100)%((1000000000)/2);
            BigInteger y = Encrypt.Randomizer.RandomBigInteger(100)%1000000000;
            
            p = 2 * q + 1;
            while (!Encrypt.Prime.MillerRabinTest(q,11) || !Encrypt.Prime.MillerRabinTest(p,11)) {
                q++;
                p = 2 * q + 1;
            }
            a = Encrypt.Randomizer.RandomBigInteger(100)%p;

            /* 131^x mod 618678851 = 86536
             * 318276265^x mod 328447774 = 108163827
             * 459^x mod 857400563 = 106100
            */

            Console.Out.WriteLine("{0}^x mod {1} = {2}", a, p, y);
            
            if (y > p) {
                Console.Out.WriteLine("No decision. Change Y and P");
                BigInteger temp = y;
                y = p;
                p = temp;
                Console.Out.WriteLine("{0}^x mod {1} = {2}", a, p, y);
            }

            if (p == 0) {
                Console.Out.WriteLine("No decision. P == {0}", p);
            }

            if (y == p) {
                Console.Out.WriteLine("No decision; Y == P");
                return;
            }
            
            if (a < 2 && a != y) {
                Console.Out.WriteLine("No decision");
                Console.Out.WriteLine("A = {0}; Y = {1}", a, y);
                return;
            }
            
            BigInteger m = Encrypt.MathUtils.Sqrt(p) + 1;
            BigInteger k = m;

            while(m*k <= p) {
                k += 1;
                m += 1;
            }

            Console.Out.WriteLine("m = {0}; k = {1}", m, k);
            Console.Out.WriteLine("Making matrix...");
            
            List<BigInteger> aList = new List<BigInteger>();
            List<BigInteger> yaList = new List<BigInteger>();
            for (BigInteger i = 1; i < m; i++) {
                //BigInteger vl = a * Utility.PowMod(a, i, p) % p;
                //BigInteger vl = y * BigInteger.Pow(a,(int)i) % p;
                BigInteger vl = (y%p) * (Encrypt.MathUtils.PowMod(a, i, p)) % p;
                yaList.Add(vl);
            }
            for (BigInteger i = 1; i <= m; i++) {
                BigInteger vl = Encrypt.MathUtils.PowMod(a, i*m, p);
                aList.Add(vl);
            }

            /*Console.Out.Write("j: ");
            foreach (var e in aList) {
                Console.Out.Write(e+" ");
            }
            Console.Out.WriteLine("");
            Console.Out.Write("i: ");
            foreach (var e in yaList) {
                Console.Out.Write(e+" ");
            }
            Console.Out.WriteLine("");*/

            Console.Out.WriteLine("Searching...");

            BigInteger? value = null;
            foreach (BigInteger e in aList.Intersect(yaList)) {
                value = e;
                break;
            }

            if (value == null) {
                Console.Out.WriteLine("No decision");
                return;
            }
            
            Console.Out.WriteLine("Value = {0}", value);

            int ja = 1;
            int ia = 1;
            foreach (var e in aList) {
                if(e == value) break;
                ia++;
            }
            foreach (var e in yaList) {
                if(e == value) break;
                ja++;
            }

            BigInteger x = m*ia - ja;
            Console.Out.WriteLine("i = {0}; j = {1};", ia, ja);
            Console.Out.WriteLine("x = i * m - j = {0}", x);
            Console.Out.WriteLine("Check value = " + Encrypt.MathUtils.PowMod(a, x, p));
            
            Console.Out.WriteLine("\nTeoritical complexity = sqrt(p)*log(p,2) = {0}", (Encrypt.MathUtils.Sqrt(p))*(BigInteger) BigInteger.Log(p, 2));
            Console.Out.WriteLine("Complexity = " + (2*aList.Count+yaList.Count*3+(int)N));
        }
    }
}