﻿using System;
using System.IO;
using System.Numerics;
using Encrypt;
using Encrypt.sign;

namespace ConsoleApp9
{
    class Program
    {
        static void Main(string[] args)
        {
            Vertex keys = SignRSA.GenerateKeys();
            BigInteger n = keys.x;
            BigInteger c = keys.y;
            BigInteger d = keys.z;
            
            SignRSA.SignFile("source.txt","signed.txt",c,n);
            SignRSA.CheckFile("signed.txt", n, d);
        }
    }
}