﻿using System;
using System.Numerics;
using Encrypt;
using Encrypt.sign;

namespace ConsoleApp11
{
    class Program
    {
        static void Main(string[] args)
        {
            Vertex keys = SignGOST.GenerateKeys();
            BigInteger p = keys.x;
            BigInteger q = keys.y;
            BigInteger a = keys.z;

            //Console.WriteLine(q-BigInteger.Pow(2,255));
            
            BigInteger x = Randomizer.RandomBigInteger(2024) % q;
            BigInteger y = MathUtils.PowMod(a, x, p);
            
            SignGOST.SignFile("source.txt", "signed.txt", p, q, a, x);
            SignGOST.CheckFile("signed.txt", a, y, q, p);
        }
    }
}