﻿using System;
using System.Numerics;

namespace Encrypt
{
    public static class Randomizer
    {
        public static BigInteger RandomBigInteger(int v)
        {
            Random random = new Random();
            BigInteger val = 0;
            for (int i = 0; i < v; i++)
            {
                val += random.Next(0,2);
                val <<= 1;
            }

            return val;
        }

        public static BigInteger Coprime(int v, BigInteger value)
        {
            BigInteger res = RandomBigInteger(v);
            if (res < value)
            {
                res += value - res;
            }
            
            while (Utility.Euclid(res, value)[0] > 1)
            {
                res += 1;
            }

            return res;
        }
        
        public static BigInteger Coprime(BigInteger start, BigInteger value)
        {
            BigInteger res = start + 1;
            while (Utility.Euclid(res, value)[0] > 1)
            {
                res += 1;
            }

            return res;
        }

        public static BigInteger GenerateP(BigInteger seed)
        {
            return GenerateP(seed, false);
        }
        public static BigInteger GenerateP(BigInteger seed, bool strong)
        {
            BigInteger q = seed;
            BigInteger p = 2 * q + 1;

            if (strong)
            {
                while (!Prime.IsPrime(q) || !Prime.IsPrime(p))
                {
                    q++;
                    p = 2 * q + 1;
                }
            }
            else
            {
                while (!Prime.MillerRabinTest(q,11) || !Prime.MillerRabinTest(p,11))
                {
                    q++;
                    p = 2 * q + 1;
                } 
            }

            return p;
        }
    }
}