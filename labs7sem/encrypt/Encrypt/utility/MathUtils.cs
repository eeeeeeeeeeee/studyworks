﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Xml.Schema;

namespace Encrypt
{
    public static class MathUtils
    {
        public static BigInteger Sqrt(BigInteger n) 
        {
            if (n == 0) 
                return 0;
            if (n > 0) 
            {
                int bitLength = Convert.ToInt32(Math.Ceiling(BigInteger.Log(n, 2)));
                BigInteger root = BigInteger.One << (bitLength / 2);

                while (!IsSqrt(n, root)) 
                {
                    root += n / root;
                    root /= 2;
                }

                return root;
            }

            throw new ArithmeticException("NaN");
        }

        private static Boolean IsSqrt(BigInteger n, BigInteger root) 
        {
            BigInteger lowerBound = root*root;
            BigInteger upperBound = (root + 1)*(root + 1);

            return (n >= lowerBound && n < upperBound);
        }

        public static BigInteger PowMod(BigInteger a, BigInteger x, BigInteger p) 
        {
            BigInteger y = 1, s = a;
            do 
            {
                if ((x & 1) > 0) 
                {
                    y = y * s % p;
                }
                
                x >>= 1;

                if (!(x > 0)) break;
                s = s * s % p;
            } while (true);

            return y;
        }

        public static BigInteger GetGradle(BigInteger a, BigInteger p, BigInteger y)
        {
            if (y >= p) return 0;
            if (Utility.Euclid(a, p)[0] > 1) return 0;
            
            BigInteger m = Sqrt(p) + 1;
            BigInteger k = m;

            while(m*k <= p) {
                k += 1;
                m += 1;
            }
            
            List<BigInteger> aList = new List<BigInteger>();
            List<BigInteger> yaList = new List<BigInteger>();
            for (BigInteger i = 1; i < m; i++) {
                BigInteger vl = (y%p) * (PowMod(a, i, p)) % p;
                yaList.Add(vl);
            }
            for (BigInteger i = 1; i <= m; i++) {
                BigInteger vl = PowMod(a, i*m, p);
                aList.Add(vl);
            }
            
            BigInteger? value = null;
            foreach (BigInteger e in aList.Intersect(yaList)) 
            {
                value = e;
                break;
            }

            if (value == null)
            {
                return 0;
            }
            
            int ja = 1;
            int ia = 1;
            foreach (var e in aList) {
                if(e == value) break;
                ia++;
            }
            foreach (var e in yaList) {
                if(e == value) break;
                ja++;
            }

            BigInteger x = m*ia - ja;

            return x;
        }
        
        public static BigInteger Inverse(BigInteger c, BigInteger m)
        {
            BigInteger[] u = Utility.Euclid(c, m);
            
            BigInteger val;
            if (c > m) val = u[1];
            else val = u[2];
            if (val < 0) val += m;
            
            return val;
        }
    }
}