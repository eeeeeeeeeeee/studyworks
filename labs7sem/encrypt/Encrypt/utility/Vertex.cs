﻿using System.Numerics;

namespace Encrypt
{
    public struct Vertex
    {
        public BigInteger x, y, z;

        public Vertex(BigInteger x, BigInteger y) 
        {
            this.x = x;
            this.y = y;
            this.z = 0;
        }
        
        public Vertex(BigInteger x, BigInteger y, BigInteger z) 
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }
}