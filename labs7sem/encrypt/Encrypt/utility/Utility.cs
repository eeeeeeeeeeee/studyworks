﻿using System;
using System.Numerics;
using System.Security.Cryptography;

namespace Encrypt 
{
    public static class Utility 
    {
        public static BigInteger[] Euclid(BigInteger a, BigInteger b) 
        {
            if (b > a) 
            {
                BigInteger temp = a;
                a = b;
                b = temp;
            }
            
            BigInteger[][] matrix = new BigInteger[3][];
            matrix[0] = new[] {a,1,0};
            matrix[1] = new[] {b,0,1};
            matrix[2] = new[] {(BigInteger)0,0,0};
            
            do 
            {
                BigInteger q = matrix[0][0] / matrix[1][0];
                
                for (int i = 0; i < 3; i++) {
                    matrix[2][i] = matrix[0][i] - q * matrix[1][i];
                }

                for (int i = 0; i < 3; i++) {
                    matrix[0][i] = matrix[1][i];
                    matrix[1][i] = matrix[2][i];
                } 
                
            } while (matrix[1][0] != 0);

            return matrix[0];
        }
        public static Vertex GenerateKey(BigInteger seed) 
        {
            return GenerateKey(seed, false);
        }
        
        public static Vertex GenerateKey(BigInteger seed, bool strong) 
        {
            BigInteger q = seed;
            BigInteger p = 2 * q + 1;

            if (strong) 
            {
                while (!Prime.IsPrime(q) || !Prime.IsPrime(p)) 
                {
                    q++;
                    p = 2 * q + 1;
                }
            }
            else 
            {
                while (!Prime.MillerRabinTest(q, 10) || !Prime.MillerRabinTest(p, 10)) 
                {
                    q++;
                    p = 2 * q + 1;
                }
            }

            BigInteger f = p - 1;
            BigInteger g = Randomizer.RandomBigInteger(100)%p;
            if (MathUtils.PowMod(g, f, p) != 1) 
            {
                Console.Out.WriteLine("Failed");
                return new Vertex();
            }
            
            return new Vertex(p, g);
        }
        
    }
}