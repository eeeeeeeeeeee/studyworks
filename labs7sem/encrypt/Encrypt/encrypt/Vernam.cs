﻿using System.Numerics;

namespace Encrypt.encrypt
{
    public class Vernam : Encrypter
    {
        private BigInteger k;
        
        public Vernam(BigInteger k) : base(k)
        {
            this.k = k;
        }

        protected override BigInteger Encrypt(BigInteger m)
        {
            return m ^ k;
        }

        protected override BigInteger Decrypt(BigInteger m)
        {
            return m ^ k;
        }
    }
}