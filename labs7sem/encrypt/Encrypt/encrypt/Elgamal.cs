﻿using System.Numerics;


namespace Encrypt.encrypt
{
    public class Elgamal : Encrypter
    {
        private BigInteger a, // open
            y,
            k,
            p,
            x;

        public Elgamal(BigInteger p, BigInteger a, BigInteger y, BigInteger k, BigInteger x) : base(p)
        {
            this.a = a;
            this.y = y;
            this.k = k;
            this.p = p;
            this.x = x;
        }

        protected override BigInteger Decrypt(BigInteger m)
        {
            return m * MathUtils.PowMod(a, p - 1 - x, p) % p;
        }

        protected override BigInteger Encrypt(BigInteger m)
        {
            return MathUtils.PowMod(y, k, p) * m % p;
        }
    }
}