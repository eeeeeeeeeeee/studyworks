﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;

namespace Encrypt.encrypt
{
    public abstract class Encrypter
    {
        private BigInteger _maxMessageLen = 0;

        protected Encrypter(BigInteger p)
        {
            _maxMessageLen = 0;
            BigInteger buf = p - 1;

            while (buf>0)
            {
                buf >>= 1;
                _maxMessageLen += 1;
            }

            _maxMessageLen = (_maxMessageLen / 8) * 8;
        }

        protected abstract BigInteger Encrypt(BigInteger m);
        
        protected abstract BigInteger Decrypt(BigInteger m);

        public void EncryptToFile(String file, byte[] bytes)
        {
            FileStream wStream = new FileStream("en.txt", FileMode.Create);
            StreamWriter writer = new StreamWriter(wStream);

            BigInteger m = 0;
            BigInteger mLen = 0;
            foreach (var v in bytes)
            {
                if (mLen + 8 < _maxMessageLen)
                {
                    m <<= 8;
                    m += v;
                    mLen += 8;
                }
                else
                {
                    writer.WriteLine(Encrypt(m));
                    m = v;
                    mLen = 8;
                }

            }

            if (mLen > 0)
            {
                writer.WriteLine(Encrypt(m));
            }
            
            writer.Close();
            wStream.Close();
        }

        public void DectryptFromFile(String source, String dest)
        {
            FileStream rStream = new FileStream(source, FileMode.Open);
            StreamReader reader = new StreamReader(rStream);
            
            FileStream wStream = new FileStream(dest, FileMode.Create);
            StreamWriter writer = new StreamWriter(wStream);

            String str;
            while ((str = reader.ReadLine()) != null)
            {
                BigInteger digit = BigInteger.Parse(str);
                digit = Decrypt(digit);
                List<byte> res = new List<byte>();
                for (int i=0;i<_maxMessageLen-8;i+=8)
                {
                    res.Insert(0,(byte)(digit&0xff));
                    digit >>= 8;
                }

                wStream.Write(res.ToArray());
            }
            
            writer.Close();
            wStream.Close();
            reader.Close();
            rStream.Close();
        }
    }
}