﻿using System.Numerics;

namespace Encrypt.encrypt
{
    public class Rsa : Encrypter
    {
        private BigInteger n, d, // open
            c;                   // close
        
        public Rsa(BigInteger p, BigInteger n, BigInteger d, BigInteger c) : base(p)
        {
            this.n = n;
            this.d = d;
            this.c = c;
        }

        protected override BigInteger Encrypt(BigInteger m)
        {
            return MathUtils.PowMod(m, d, n);
        }

        protected override BigInteger Decrypt(BigInteger m)
        {
            return MathUtils.PowMod(m, c, n);
        }
    }
}