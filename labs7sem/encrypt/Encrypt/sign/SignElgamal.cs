﻿using System;
using System.Collections;
using System.IO;
using System.Numerics;
using System.Security.Cryptography;
using System.Threading.Channels;

namespace Encrypt.sign
{
    public static class SignElGamal
    {
        const int bits = 256 + 32;
        private const int bytes = (256 + 32) / 8 + 1;
        public static Vertex[] GenerateKeys(String file)
        {
            FileStream fileStream = new FileStream(file, FileMode.Open);
            byte[] message = new byte[fileStream.Length];
            fileStream.Read(message);
            fileStream.Close();
            
            // P-size control
            // 1 < Hash(message) < P
            // Hash size = 256
            
            BigInteger p = Randomizer.GenerateP(Randomizer.RandomBigInteger(bits));
            BigInteger g = Randomizer.RandomBigInteger(bits) % p;
            g = Prime.GetPrime(g);
            
            BigInteger x = Randomizer.RandomBigInteger(bits) % p;
            x = Prime.GetPrime(x);
            
            BigInteger y = MathUtils.PowMod(g, x, p);
            
            BigInteger k = Randomizer.RandomBigInteger(bits) % (p-1);
            while (Utility.Euclid(p-1,k)[0]>1 || !Prime.MillerRabinTest(k, 22))
            {
                k += 1;
            }

            BigInteger r = MathUtils.PowMod(g, k, p);
            
            SHA256 sha256 = SHA256.Create();
            BigInteger h = new BigInteger(sha256.ComputeHash(message), true);
            
            BigInteger u = (h-x*r) % (p-1);
            if (u < 0) u += p - 1;
            BigInteger invK = MathUtils.Inverse(k, p - 1);
            
            BigInteger s = invK*u % (p-1);
            if (s < 0) s += p - 1;

            Console.WriteLine("p-h: {0}", p - h);
            
            return new[] {new Vertex(p, g, y), new Vertex(r, s)};
        }

        public static void SignFile(String source, String dest, BigInteger r, BigInteger s)
        {
            // Get message
            FileStream fileStream = new FileStream(source, FileMode.Open);
            byte[] message = new byte[fileStream.Length];
            fileStream.Read(message);
            fileStream.Close();
            
            // Create signed message
            fileStream = new FileStream(dest, FileMode.Create);
            
            fileStream.Write(message);
            
            byte[] buf = new byte[bytes];
            byte[] sBuf = r.ToByteArray();
            for (int i = 0; i < sBuf.Length; i++)
            {
                buf[buf.Length-1-i] = sBuf[sBuf.Length-1-i];
            }
            fileStream.Write(buf);
            
            buf = new byte[bytes];
            sBuf = s.ToByteArray();
            for (int i = 0; i < sBuf.Length; i++)
            {
                buf[buf.Length-1-i] = sBuf[sBuf.Length-1-i];
            }
            fileStream.Write(buf);
            
            fileStream.Close();
            
            Console.WriteLine("r:"+r);
            Console.WriteLine("s:"+s);
        }

        public static bool CheckFile(String file, BigInteger y, BigInteger g, BigInteger p)
        {
            FileStream fileStream = new FileStream(file, FileMode.Open);
            byte[] bufS = new byte[bytes];
            byte[] message = new byte[fileStream.Length - bytes*2];
            
            fileStream.Read(message);
            
            fileStream.Read(bufS, 0, bytes);
            
            // Read R
            int i = 0;
            while (bufS[i] == 0)
            {
                i++;
            }

            byte[] buf = new byte[bytes-i];
            for (int j = i; j < bytes; j++)
            {
                buf[j - i] = bufS[j];
            }
            BigInteger r = new BigInteger(buf);
            
            // Read S
            fileStream.Read(bufS, 0, bytes);
            i = 0;
            while (bufS[i] == 0)
            {
                i++;
            }

            buf = new byte[bytes-i];
            for (int j = i; j < bytes; j++)
            {
                buf[j - i] = bufS[j];
            }
            BigInteger s = new BigInteger(buf);
            fileStream.Close();
            
            SHA256 sha256 = SHA256.Create();
            BigInteger h = new BigInteger(sha256.ComputeHash(message), true);
            if (s < 0) s += p;
            
            BigInteger a0 = MathUtils.PowMod(g, h, p);
            BigInteger a1 = (MathUtils.PowMod(y, r, p) * MathUtils.PowMod(r, s, p)) % p;

            Console.WriteLine("r:"+r);
            Console.WriteLine("s:"+s);
            
            Console.WriteLine(a0);
            Console.WriteLine(a1);

            if (a0 == a1) return true;
            
            return false;
        }
    }
}