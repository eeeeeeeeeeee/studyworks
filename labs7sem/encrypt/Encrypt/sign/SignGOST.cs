﻿using System;
using System.IO;
using System.Numerics;
using System.Security.Cryptography;

namespace Encrypt.sign
{
    public static class SignGOST
    {
        public static Vertex GenerateKeys()
        {
            BigInteger p = 1;
            for (int i = 0; i < 1023; i++)
            {
                p <<= 1;
            }
            p += Randomizer.RandomBigInteger(1020);
            
            BigInteger q = 1;
            for (int i = 0; i < 255; i++)
            {
                q <<= 1;
            }
            q += Randomizer.RandomBigInteger(250);
            
            while (!Prime.MillerRabinTest(q, 22))
            {
                q += 1;
            }
            
            while (!Prime.MillerRabinTest(p+1, 22))
            {
                p += 1;
            }

            BigInteger delta = p % q;
            p -= delta;

            while (!Prime.MillerRabinTest(p+1, 22))
            {
                p += q;
            }

            p += 1;
            BigInteger b = p / q;

            BigInteger a;
            
            BigInteger g = Randomizer.RandomBigInteger(2048) % (p - 1);
            while (true)
            {
                a = MathUtils.PowMod(g, b, p);
                if (a > 1) break;
                g = (g + 1) % (p - 1);
            }

            Console.WriteLine("a: "+ MathUtils.PowMod(a,q,p));
            
            return new Vertex(p, q, a);
        }

        public static void SignFile(String source, String dest, BigInteger p, BigInteger q, BigInteger a, BigInteger x)
        {
            FileStream fileStream = new FileStream(source, FileMode.Open);

            byte[] message = new byte[fileStream.Length];
            fileStream.Read(message);
            
            SHA256 sha256 = SHA256.Create();
            BigInteger h = new BigInteger(sha256.ComputeHash(message),true);

            fileStream.Close();
            
            BigInteger r, s;
            BigInteger k = Randomizer.RandomBigInteger(2024) % q;
            while (true)
            {
                while (true)
                {
                    r = MathUtils.PowMod(a, k, p) % q;
                    if (r != 0) break;
                
                    k += 1;
                    if (k > q-1) k = 0;
                }

                s = (k * h + x * r) % q;
                if (s != 0) break;
                
                k += 1;
                if (k > q-1) k = 0;
            }

            fileStream = new FileStream(dest, FileMode.Create);
            
            // write message
            fileStream.Write(message);
            
            // write r
            byte[] buf = new byte[32];
            byte[] rb = r.ToByteArray();
            for (int i = 0; i < 32; i++)
            {
                buf[i] = rb[i];
            }
            fileStream.Write(buf);
            
            // write s
            buf = new byte[32];
            rb = s.ToByteArray();
            for (int i = 0; i < 32; i++)
            {
                buf[i] = rb[i];
            }
            fileStream.Write(buf);

            fileStream.Close();
        }

        public static bool CheckFile(String file, BigInteger a, BigInteger y, BigInteger q, BigInteger p)
        {
            FileStream fileStream = new FileStream(file, FileMode.Open);
            byte[] bufR = new byte[32];
            byte[] bufS = new byte[32];
            byte[] message = new byte[fileStream.Length - 64];
            fileStream.Read(message);
            fileStream.Read(bufR, 0, 32);
            fileStream.Read(bufS, 0, 32);
            fileStream.Close();
            
            SHA256 sha256 = SHA256.Create();
            BigInteger r = new BigInteger(bufR, true);
            BigInteger s = new BigInteger(bufS, true);
            BigInteger h = new BigInteger(sha256.ComputeHash(message),true);
            
            if (r >= q || s >= q) return false;
            BigInteger invH = MathUtils.Inverse(h, q);
            BigInteger u1 = s * invH % q;
            BigInteger u2 = (q-r) * invH % q;
            if (u2 < 0) u2 += q;
            BigInteger v = (MathUtils.PowMod(a, u1, p) * MathUtils.PowMod(y, u2, p)) % p % q;
            
            Console.WriteLine(v);
            Console.WriteLine(r);
            
            if (v == r) return true;

            return false;
        }
    }
}