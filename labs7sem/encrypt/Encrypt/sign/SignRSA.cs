﻿using System;
using System.IO;
using System.Numerics;
using System.Security.Cryptography;

namespace Encrypt.sign
{
    public static class SignRSA
    {
        const int bits = 256 + 32;
        private const int bytes = (256 + 32)*2/8 + 1;
        public static Vertex GenerateKeys()
        {
            BigInteger p = Randomizer.GenerateP(Randomizer.RandomBigInteger(bits));
            BigInteger q = Randomizer.GenerateP(Randomizer.RandomBigInteger(bits));
            BigInteger n = p * q;
            
            BigInteger f = (p - 1) * (q - 1);
            BigInteger d = Randomizer.RandomBigInteger(bits)%f;
            while (Utility.Euclid(d,f)[0]!=1)
            {
                d += 1;
            }

            BigInteger c = MathUtils.Inverse(d, f);
            
            return new Vertex(n, c, d);
        }

        public static void SignFile(String source, String dest, BigInteger c, BigInteger n)
        {
            FileStream fileStream = new FileStream(source, FileMode.Open);

            byte[] message = new byte[fileStream.Length];
            fileStream.Read(message);
            
            SHA256 sha256 = SHA256.Create();
            byte[] h = sha256.ComputeHash(message);

            fileStream.Close();
            
            BigInteger s = MathUtils.PowMod(new BigInteger(h,true),c,n);
            fileStream = new FileStream(dest, FileMode.Create);
            
            fileStream.Write(message);
            
            byte[] buf = new byte[bytes];
            byte[] sBuf = s.ToByteArray();
            for (int i = 0; i < sBuf.Length; i++)
            {
                buf[buf.Length-1-i] = sBuf[sBuf.Length-1-i];
            }
            fileStream.Write(buf);
            
            fileStream.Close();
        }

        public static bool CheckFile(String file, BigInteger n, BigInteger d)
        {
            FileStream fileStream = new FileStream(file, FileMode.Open);
            byte[] bufS = new byte[bytes];
            byte[] message = new byte[fileStream.Length - bytes];
            fileStream.Read(message);
            fileStream.Read(bufS, 0, bytes);
            fileStream.Close();
            
            int i = 0;
            while (bufS[i] == 0)
            {
                i++;
            }

            byte[] buf = new byte[bytes-i];
            for (int j = i; j < bytes; j++)
            {
                buf[j - i] = bufS[j];
            }
            BigInteger s = new BigInteger(buf);
            
            SHA256 sha256 = SHA256.Create();
            BigInteger h = new BigInteger(sha256.ComputeHash(message),true);
                
            BigInteger e = MathUtils.PowMod(s, d, n);
            
            Console.WriteLine(h);
            Console.WriteLine(e);

            if (h == e) return true;
            
            return false;
        }
    }
}