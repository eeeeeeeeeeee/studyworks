﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using Encrypt;

namespace ConsoleApp5
{
    class Program
    {
        private const int A = 0, B = 1;
        
        static void Main(string[] args)
        {
            BigInteger p;
            BigInteger[] c = new BigInteger[2], d = new BigInteger[2];
            
            p = Randomizer.GenerateP(Randomizer.RandomBigInteger(300));

            c[A] = Randomizer.Coprime(400, p - 1);
            d[A] = MathUtils.Inverse(c[A], p - 1);

            c[B] = Randomizer.Coprime(400, p - 1);
            d[B] = MathUtils.Inverse(c[B], p - 1);

            Console.WriteLine(c[A] * d[A] % (p - 1));
            Console.WriteLine(c[B] * d[B] % (p - 1));

            Console.WriteLine("input:");
            String inputFile = Console.ReadLine();
            String outputFile = Console.ReadLine();
            
            FileStream wStream = new FileStream("keys.txt", FileMode.Create);
            StreamWriter writer = new StreamWriter(wStream);
            writer.WriteLine(p);
            writer.WriteLine(c[A]);
            writer.WriteLine(d[A]);
            writer.WriteLine(c[B]);
            writer.WriteLine(d[B]);
            writer.Close();
            wStream.Close();
            
            // X1 =====================================================
            byte[] bytes = File.ReadAllBytes(inputFile);
            
            wStream = new FileStream("x1.txt", FileMode.Create);
            writer = new StreamWriter(wStream);
            
            BigInteger m = 0;
            BigInteger maxMessageLen = 0;
            BigInteger buf = p - 1;

            while (buf>0)
            {
                buf >>= 1;
                maxMessageLen += 1;
            }

            maxMessageLen = (maxMessageLen / 8) * 8;

            Console.WriteLine(maxMessageLen);
            
            BigInteger mLen = 0;
            foreach (var v in bytes)
            {
                if (mLen + 8 < maxMessageLen)
                {
                    m <<= 8;
                    m += v;
                    mLen += 8;
                }
                else
                {
                    writer.WriteLine(MathUtils.PowMod(m, c[A], p));
                    m = v;
                    mLen = 8;
                }

            }

            if (mLen > 0)
            {
                writer.WriteLine(MathUtils.PowMod(m, c[A], p));
            }
            
            writer.Close();
            wStream.Close();
            
            // X2 =======================================================
            FileStream rStream = new FileStream("x1.txt", FileMode.Open);
            StreamReader reader = new StreamReader(rStream);
            
            wStream = new FileStream("x2.txt", FileMode.Create);
            writer = new StreamWriter(wStream);

            String str;
            while ((str = reader.ReadLine()) != null)
            {
                BigInteger digit = BigInteger.Parse(str);
                writer.WriteLine( MathUtils.PowMod(digit, c[B], p) );
            }
            
            writer.Close();
            wStream.Close();
            reader.Close();
            rStream.Close();
            
            // X3 =======================================================
            rStream = new FileStream("x2.txt", FileMode.Open);
            reader = new StreamReader(rStream);
            
            wStream = new FileStream("x3.txt", FileMode.Create);
            writer = new StreamWriter(wStream);

            while ((str = reader.ReadLine()) != null)
            {
                BigInteger digit = BigInteger.Parse(str);
                writer.WriteLine( MathUtils.PowMod(digit, d[A], p) );
            }
            
            writer.Close();
            wStream.Close();
            reader.Close();
            rStream.Close();
            
            // X4 =======================================================
            rStream = new FileStream("x3.txt", FileMode.Open);
            reader = new StreamReader(rStream);
            
            wStream = new FileStream(outputFile, FileMode.Create);
            writer = new StreamWriter(wStream);

            while ((str = reader.ReadLine()) != null)
            {
                BigInteger digit = BigInteger.Parse(str);
                digit = MathUtils.PowMod(digit, d[B], p);
                List<byte> res = new List<byte>();
                for (int i=0;i<maxMessageLen-8;i+=8)
                {
                    res.Insert(0,(byte)(digit&0xff));
                    digit >>= 8;
                }

                wStream.Write(res.ToArray());
            }
            
            writer.Close();
            wStream.Close();
            reader.Close();
            rStream.Close();
        }
    }
}