﻿using System;
using System.IO;

namespace ConsoleApp2 {
    class Program {
        private const int U = 0;
        private const int V = 1;
        private const int T = 2; 
        
        private static void OutMatrix(long[][] matrix) {
            for (int i = 0; i < matrix.Length-1; i++) {
                for (int j = 0; j < matrix[i].Length;j++) {
                    Console.Out.Write("{0, -5}",matrix[i][j]);
                }
                Console.Out.Write("\n");
            }
        }
        
        static void Main(string[] args) {
            int n = 0;
            
            long a, b;

            a = (long) Utility.RandomSeed64(1000000000);
            b = (long) Utility.RandomSeed64(1000000000);

            if (b > a) {
                long temp = a;
                a = b;
                b = temp;
            }

            Console.Out.WriteLine("a = {0}; b = {1};", a, b);
            long[][] matrix = new long[3][];
            matrix[U] = new long[]{a, 1, 0};
            matrix[V] = new long[]{b, 0, 1};
            matrix[T] = new long[3];

            Console.Out.WriteLine("Begin matrix: ");
            OutMatrix(matrix);
            
            do {
                n++;
                
                long q = matrix[U][0] / matrix[V][0];
                for (int i = 0; i < 3; i++) {
                    matrix[T][i] = matrix[U][i] - q * matrix[V][i];
                    Console.Out.WriteLine("T[{0}] = {1} - {3} * {2}", i, matrix[U][i], matrix[V][i], q);
                }

                for (int i = 0; i < 3; i++) {
                    matrix[U][i] = matrix[V][i];
                    matrix[V][i] = matrix[T][i];
                } 

                Console.Out.WriteLine("\nSTEP {0} MATRIX: ", n);
                OutMatrix(matrix);
            } while (matrix[V][0] != 0);

            Console.Out.WriteLine("\nU = {0}", matrix[U][0]);
            Console.Out.Write("\nControl check:\na*x + b*y = U = ");
            Console.Out.WriteLine("{0}*{2} + {1}*{3} = {4}", a, b, matrix[0][1], matrix[0][2], 
                (a*matrix[0][1] + b*matrix[0][2]));
            Console.Out.WriteLine("Exptected: {0}", matrix[0][0]);
            Console.Out.WriteLine("Complexity (steps): {0}", n);
        }
    }
}