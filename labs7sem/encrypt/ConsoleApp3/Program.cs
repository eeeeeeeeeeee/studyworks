﻿using System;
using System.Numerics;

namespace ConsoleApp3 {
    class Program {
        private const int A = 0, B = 1;
        static void Main(string[] args) {
            Console.WriteLine("Generating...");



            ulong seed = (ulong)Utility.GetPrime(Utility.RandomSeed64(1000000000), 11);
            Console.Out.WriteLine("Seed: {0}", seed);

            ulong q = seed;
            ulong p = 2 * q + 1;
            while (!Utility.IsPrime(q) || !Utility.IsPrime(p)) {
                q++;
                p = 2 * q + 1;
            }

            Console.Out.WriteLine("q = {0}; p = {1};", q, p);
            
            ulong f = p - 1;
            ulong g = (ulong)Utility.RandomSeed64(p);
            Console.WriteLine("g seed = {0}\nChecking...",g);
            
            while (Utility.Euler(p,g)>1) {
                g += 1;
            }
            
            if (Utility.PowMod(g, f, p) != 1) {
                Console.Out.WriteLine("Failed");
                return;
            }

            Console.Out.WriteLine("q = {0}; p = {1}; g = {2};\n", q, p, g);

            ulong[] x = {(ulong)Utility.RandomSeed64(p), (ulong)Utility.RandomSeed64(p)};
            ulong[] y = {(ulong)Utility.PowMod(g, x[A], p), (ulong)Utility.PowMod(g, x[B], p)};
            ulong[] z = {(ulong)Utility.PowMod(y[B], x[A], p), (ulong)Utility.PowMod(y[A], x[B], p)};

            Console.Out.WriteLine("Xa = {0}; Xb = {1};", x[A], x[B]);
            Console.Out.WriteLine("Ya = {0}; Yb = {1};", y[A], y[B]);
            Console.Out.WriteLine("Zab = {0}; Zba = {1};", z[A], z[B]);

            //
            // Дополнительный вывод
            //
            
            Console.Out.WriteLine("\n\nNot strong check (CAN BE FAILED):");
            Utility.Vertex vertex = Utility.GenerateKey(9999999535345334999, false);
            Console.Out.WriteLine("p = {0}, g = {1}", vertex.x, vertex.y);
            
            BigInteger[] xx = {Utility.RandomSeed64(p), Utility.RandomSeed64(p)};
            BigInteger[] yy = {Utility.PowMod(vertex.y, xx[A], vertex.x), Utility.PowMod(vertex.y, xx[B], vertex.x)};
            BigInteger[] zz = {Utility.PowMod(yy[B], xx[A], vertex.x), Utility.PowMod(yy[A], xx[B], vertex.x)};

            Console.Out.WriteLine("Xa = {0}; Xb = {1};", xx[A], xx[B]);
            Console.Out.WriteLine("Ya = {0}; Yb = {1};", yy[A], yy[B]);
            Console.Out.WriteLine("Zab = {0}; Zba = {1};", zz[A], zz[B]);
            /*if(Console.ReadLine() != "g") return;

            for (int i = 0; i < 100000; i++) {
                Utility.Vertex vertex = Utility.GenerateKey(1999999999);
                Console.Out.WriteLine("p = {0}, g = {1}", vertex.x, vertex.y);
            }*/
        }
    }
}