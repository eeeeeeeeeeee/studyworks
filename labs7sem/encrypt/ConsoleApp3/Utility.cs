﻿using System;
using System.Numerics;
using System.Security.Cryptography;

namespace ConsoleApp3 {
    public static class Utility {
        public struct Vertex {
            public BigInteger x, y;

            public Vertex(BigInteger x, BigInteger y) {
                this.x = x;
                this.y = y;
            }
        }

        public static ulong RandomSeed32() {
            Random random = new Random();
            ulong seed32 = 0;
            ulong value = 1;
            for (int i = 0; i < 9; i++) {
                seed32 +=  ((ulong)random.Next(1, 9) * value);
                value *= 10;
            }
            return seed32;
        }

        public static BigInteger RandomSeed64(BigInteger p) {
            return ((RandomSeed32() << 32) + RandomSeed32())%p;
        }

        public static bool IsPrime(BigInteger digit) {
            if (digit <= 1) return false;
            BigInteger b = Sqrt(digit);
            for (ulong i = 2; i <= b; i++) {
                if ((digit % i) == 0) return false;
            }
            return true;
        }

        public static BigInteger GetPrime(BigInteger seed, int k) {
            while(!MillerRabinTest(seed, k)) {
                seed += 1;
            }
            return seed;
        }
        public static bool MillerRabinTest(BigInteger n, int k) {
            if (n == 2 || n == 3)
                return true;
            if (n < 2 || n % 2 == 0)
                return false;
            // представим n-1 в виде (2^s)·t, где t нечётно, это можно сделать последовательным делением n - 1 на 2
            BigInteger t = n - 1;
            int s = 0;
            while (t % 2 == 0) {
                t /= 2;
                s += 1;
            }
 
            for (int i = 0; i < k; i++) {
                // выберем случайное целое число a в отрезке [2, n-3]
                RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
                byte[] _a = new byte[n.ToByteArray().LongLength];
                BigInteger a;
                do {
                    rng.GetBytes(_a);
                    a = new BigInteger(_a);
                }
                while (a < 2 || a >= n - 2);
 
                // x ← a^t mod n, вычислим с помощью возведения в степень по модулю
                BigInteger x = BigInteger.ModPow(a, t, n);
 
                // если x == 1 или x == n-1, то перейти на следующую итерацию цикла
                if (x == 1 || x == n - 1)
                    continue;
 
                // повторить s - 1 раз
                for (int r = 1; r < s; r++) {
                    x = BigInteger.ModPow(x, 2, n);
                    // если x == 1, то вернуть "составное"
                    if (x == 1)
                        return false;
                    // если x == n -1, то перейти на следующую итерацию внешнего цикла
                    if (x == n - 1)
                        break;
                }
 
                if (x != n - 1)
                    return false;
            }
 
            // вернуть "вероятно простое"
            return true;
        }

        private static BigInteger Sqrt(BigInteger n) {
            if (n == 0) return 0;
            if (n > 0) {
                int bitLength = Convert.ToInt32(Math.Ceiling(BigInteger.Log(n, 2)));
                BigInteger root = BigInteger.One << (bitLength / 2);

                while (!IsSqrt(n, root)) {
                    root += n / root;
                    root /= 2;
                }

                return root;
            }

            throw new ArithmeticException("NaN");
        }

        private static Boolean IsSqrt(BigInteger n, BigInteger root) {
            BigInteger lowerBound = root*root;
            BigInteger upperBound = (root + 1)*(root + 1);

            return (n >= lowerBound && n < upperBound);
        }

        public static BigInteger PowMod(BigInteger a, BigInteger x, BigInteger p) {
            BigInteger y = 1, s = a;
            do {
                if ((x & 1) > 0) {
                    y = y * s % p;
                } 
                
                x >>= 1;

                if (!(x > 0)) break;
                s = s * s % p;
            } while (true);

            return y;
        }
        
        public static ulong Euler(ulong a, ulong b) {
            if (b > a) {
                ulong temp = a;
                a = b;
                b = temp;
            }
            
            ulong[] matrix = new ulong[3];
            matrix[0] = a;
            matrix[1] = b;
            matrix[2] = 0;
            
            do {
                ulong q = matrix[0] / matrix[1];
                matrix[2] = matrix[0] - q * matrix[1];

                for (int i = 0; i < 3; i++) {
                    matrix[0] = matrix[1];
                    matrix[1] = matrix[2];
                } 
                
            } while (matrix[1] != 0);

            return matrix[0];
        }

        public static Vertex GenerateKey() {
            BigInteger seed = Utility.GetPrime(Utility.RandomSeed64(1999999999), 11);
            return GenerateKey(seed, false);
        }
        
        public static Vertex GenerateKey(BigInteger seed) {
            return GenerateKey(seed, false);
        }
        
        public static Vertex GenerateKey(BigInteger seed, bool strong) {
            BigInteger q = seed;
            BigInteger p = 2 * q + 1;

            if (strong) {
                while (!Utility.IsPrime(q) || !Utility.IsPrime(p)) {
                    q++;
                    p = 2 * q + 1;
                }
            }
            else {
                while (!Utility.MillerRabinTest(q, 10) || !Utility.MillerRabinTest(p, 10)) {
                    q++;
                    p = 2 * q + 1;
                }
            }

            BigInteger f = p - 1;
            BigInteger g = Utility.RandomSeed64(p);
            if (Utility.PowMod(g, f, p) != 1) {
                Console.Out.WriteLine("Failed");
                return new Vertex();
            }
            
            return new Vertex(p, g);
        }
        
    }
}