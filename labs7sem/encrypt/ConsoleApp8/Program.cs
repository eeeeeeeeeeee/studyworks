﻿using System;
using System.IO;
using System.Numerics;
using Encrypt;
using Encrypt.encrypt;

namespace ConsoleApp8
{
    class Program
    {
        static void Main(string[] args)
        {
            int of2 = 200;

            BigInteger k = Randomizer.RandomBigInteger(of2);
            BigInteger buf = k;
            int i = 0;
            while (buf>0)
            {
                buf >>= 1;
                i += 1;
            }

            if (i > of2) k >>= 1;
            
            for (; i < of2; i++)
            {
                k <<= 1;
                k += Randomizer.RandomBigInteger(1);
            }

            Console.WriteLine("input:");
            String inputFile = Console.ReadLine();
            String outputFile = Console.ReadLine();
            
            FileStream wStream = new FileStream("keys.txt", FileMode.Create);
            StreamWriter writer = new StreamWriter(wStream);
            writer.WriteLine("k: "+k);
            writer.Close();
            wStream.Close();
            
            byte[] bytes = File.ReadAllBytes(inputFile);
            Vernam encrypt = new Vernam(k);
            encrypt.EncryptToFile("en.txt",  bytes);
            encrypt.DectryptFromFile("en.txt",outputFile);
        }
    }
}