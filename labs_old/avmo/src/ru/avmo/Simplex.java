package ru.avmo;

import ru.avmo.fractions.Fraction;

import javax.sound.midi.Soundbank;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Simplex {
    // Путь до файла вводных данных
    private static final String file = "siplex.txt";

    private static void print(Fraction[][] matrix) {
        for(int i=0;i<matrix.length;i++) {
            for(int j=0;j<matrix[i].length;j++) {
                System.out.printf("%4s ",matrix[i][j].toString());
            }
            System.out.println();
        }
    }

    public static Solution simplex(Fraction[][] matrix, int n, int m, int[] xTable) {
        while(true) {

            print(matrix);

            boolean itsOkey = true;
            for (int i = 0; i < m; i++) {
                if (matrix[n][i].compareTo(Fraction.ZERO) < 0) {
                    itsOkey = false;
                    break;
                }
            }

            if (itsOkey) break;

            int col = 0;
            for (int i = 1; i < m-1; i++) {
                if(matrix[n][i].compareTo(Fraction.ZERO) >= 0) continue;
                if (matrix[n][i].abs().compareTo(matrix[n][col].abs()) > 0) {
                    col = i;
                }
            }

            Fraction min = null;
            int row = -1;
            for (int i = 0; i < n; i++) {
                if (matrix[i][col].compareTo(Fraction.ZERO) == 0) continue;
                Fraction value = matrix[i][m - 1].divide(matrix[i][col]);
                if (min == null) {
                    min = value;
                    row = i;
                    continue;
                }
                if (value.compareTo(Fraction.ZERO) <= 0) {
                    continue;
                }
                if (value.compareTo(min) < 0) {
                    min = value;
                    row = i;
                }
            }

            if (row < 0) return null;

            System.out.println("col: "+col);
            System.out.println("row: "+row);
            System.out.println("min: "+min);

            Fraction[][] newMatrix = new Fraction[n + 1][m];
            for (int i = 0; i < n + 1; i++) {
                for (int j = 0; j < m; j++) {
                    if (i == row) {
                        newMatrix[i][j] = matrix[i][j].divide(matrix[row][col]);
                        continue;
                    }
                    newMatrix[i][j] = matrix[i][j].subtract(matrix[i][col].multiply(matrix[row][j].divide(matrix[row][col])));
                }
            }

            matrix = newMatrix;
            xTable[row] = col;

        }

        return new Solution(matrix, xTable);
    }

    public static Solution simplex(Fraction[][] matrix, int n, int m) {
        int basisX = 0;
        for(int i=0;i<m-1;i++) {
            if(matrix[n][i].compareTo(Fraction.ZERO) != 0) basisX = i;
        }
        basisX += 1;

        int[] xTable = new int[n];
        for(int i=0;i<n;i++) {
            xTable[i] = basisX + i;
        }

        return simplex(matrix, n, m, xTable);
    }

    public static void main(String[] args) throws IOException {
        Path path = Paths.get(file);
        Scanner in = new Scanner(path);
        int m = in.nextInt();
        int n = in.nextInt();

        Fraction[][] matrix = new Fraction[n+1][m];
        for(int i=0;i<n+1;i++) {
            for(int j=0;j<m;j++) {
                matrix[i][j] = new Fraction(in.nextInt());
            }
        }

        in.close();


        simplex(matrix, n, m);

    }
}
