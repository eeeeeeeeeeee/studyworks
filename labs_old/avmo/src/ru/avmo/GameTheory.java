package ru.avmo;

import ru.avmo.fractions.Fraction;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;
import java.util.Scanner;

public class GameTheory {
    private static final String file = "game.txt";

    private static void print(Fraction[][] matrix) {
        for(int i=0;i<matrix.length;i++) {
            for(int j=0;j<matrix[i].length;j++) {
                try {
                    System.out.printf("%4s ",matrix[i][j].toString());
                } catch (NullPointerException e) {
                    System.out.printf("---- ");
                    continue;
                }

            }
            System.out.println();
        }
    }

    public static void main(String[] args) throws IOException {
        Path path = Paths.get(file);
        Scanner in = new Scanner(path);

        int n = 2;
        int m = 2;

        Fraction[][] matrix = new Fraction[n+1][m+1];

        for(int i=0;i<n;i++) {
            for(int j=0;j<m;j++) {
                matrix[i][j] = new Fraction(in.nextInt());
            }
        }

        in.close();

        for(int i=0;i<n;i++) {
            Fraction value = null;
            for(int j=0;j<m;j++) {
                if(value == null) {
                    value = matrix[i][j];
                    continue;
                } else if(matrix[i][j].compareTo(value) < 0) {
                    value = matrix[i][j];
                }
            }
            matrix[i][m] = value;
        }

        for(int i=0;i<m;i++) {
            Fraction value = null;
            for(int j=0;j<n;j++) {
                if(value == null) {
                    value = matrix[j][i];
                    continue;
                } else if(matrix[j][i].compareTo(value) > 0) {
                    value = matrix[j][i];
                }
            }
            matrix[n][i] = value;
        }

        matrix[n][m] = Fraction.ZERO;

        print(matrix);
        System.out.println();

        Fraction a = matrix[0][m];
        for(int i=1;i<n;i++) {
            if(matrix[i][m].compareTo(a) > 0) {
                a = matrix[i][m];
            }
        }

        Fraction b = matrix[n][0];
        for(int i=1;i<m;i++) {
            if(matrix[n][i].compareTo(b) < 0) {
                b = matrix[n][i];
            }
        }

        // Седловая точка
        if(a.compareTo(b) == 0) return;
        if(n != 2 || m != 2) return;

        Fraction[] p = new Fraction[2];
        Fraction[] q = new Fraction[2];

        p[0] = (matrix[1][1].subtract(matrix[1][0])).divide(matrix[0][0].add(matrix[1][1]).subtract(matrix[1][0]).subtract(matrix[0][1]));
        p[1] = (matrix[0][0].subtract(matrix[0][1])).divide(matrix[0][0].add(matrix[1][1]).subtract(matrix[1][0]).subtract(matrix[0][1]));

        q[0] = (matrix[1][1].subtract(matrix[0][1])).divide(matrix[0][0].add(matrix[1][1]).subtract(matrix[1][0]).subtract(matrix[0][1]));
        q[1] = (matrix[0][0].subtract(matrix[1][0])).divide(matrix[0][0].add(matrix[1][1]).subtract(matrix[1][0]).subtract(matrix[0][1]));

        Fraction y = p[0].multiply(matrix[0][0]).add(p[1].multiply(matrix[1][0]));

        System.out.println("Theory analysis: ");
        System.out.println("a = "+a+"; b = "+b);
        System.out.println("p ( " +p[0]+" "+p[1]+" )");
        System.out.println("q ( " +q[0]+" "+q[1]+" )");
        System.out.println("y = "+y);

        System.out.println("\n\nPractise:");
        System.out.printf("%6s %6s %6s %6s %6s %6s %6s %6s\n","N","rA","sA","rB","sB","wA","wtA","wavgA");

        int numGames = 1000;
        Random rnd = new Random();
        Fraction wt = new Fraction(0);
        for(int i=0;i<=numGames;i++) {
            int ra = rnd.nextInt(1000);
            int sa = 1;
            if(new Fraction(ra, 1000).compareTo(p[0])<0) sa = 0;
            int rb = rnd.nextInt(1000);
            int sb = 1;
            if(new Fraction(rb, 1000).compareTo(q[0])<0) sb = 0;

            Fraction wa = matrix[sa][sb];
            wt = wt.add(wa);

            if((i%100 == 0 || (i%10 == 0 && (i<100 || i>950)) || (i%2 == 0 && i<20)) && i>0) {
                System.out.printf("%6d %.3f %6s %.3f %6s %6s %6s %.3f\n",i,(float)ra/1000,sa,(float)rb/1000,sb,wa,wt,wt.divide(new Fraction(i)).toDouble());
            }
        }

    }
}
