package ru.avmo;

import ru.avmo.fractions.Fraction;

import javax.sound.midi.Soundbank;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class DualSimplex {
    // Путь до файла вводных данных
    private static final String file = "dualsimplex.txt";

    private static void print(Fraction[][] matrix) {
        for(int i=0;i<matrix.length;i++) {
            for(int j=0;j<matrix[i].length;j++) {
                System.out.printf("%4s ",matrix[i][j].toString());
            }
            System.out.println();
        }
    }

    public static Solution dsimplex(Fraction[][] matrix, int n, int m, int[] xTable) {
        while(true) {

            print(matrix);

            /*boolean itsOkey = true;
            for (int i = 0; i < m-1; i++) {
                if (matrix[n][i].compareTo(Fraction.ZERO) > 0) {
                    itsOkey = false;
                    break;
                }
            }

            if (itsOkey) break;*/

            int row = -1;
            for (int i = 1; i < n; i++) {
                if(matrix[i][m-1].compareTo(Fraction.ZERO) >= 0) continue;
                if(row < 0) {
                    row = i;
                    continue;
                }
                if (matrix[i][m-1].abs().compareTo(matrix[row][m-1].abs()) > 0) {
                    row = i;
                }
            }

            if(row < 0) break;

            Fraction min = null;
            int col = -1;
            for (int i = 0; i < m-1; i++) {
                if (matrix[row][i].compareTo(Fraction.ZERO) >= 0) continue;
                Fraction value = matrix[n][i].divide(matrix[row][i]);
                if (min == null) {
                    min = value;
                    col = i;
                    continue;
                }
                if (value.compareTo(min) < 0) {
                    min = value;
                    col = i;
                }
            }

            if (col < 0) break;

            System.out.println("col: "+col);
            System.out.println("row: "+row);
            System.out.println("min: "+min);

            Fraction[][] newMatrix = new Fraction[n + 1][m];
            for (int i = 0; i < n + 1; i++) {
                for (int j = 0; j < m; j++) {
                    if (i == row) {
                        newMatrix[i][j] = matrix[i][j].divide(matrix[row][col]);
                        continue;
                    }
                    newMatrix[i][j] = matrix[i][j].subtract(matrix[i][col].multiply(matrix[row][j].divide(matrix[row][col])));
                }
            }

            matrix = newMatrix;
            xTable[row] = col;

        }

        return new Solution(matrix, xTable);
    }

    public static Solution dsimplex(Fraction[][] matrix, int n, int m) {
        int basisX = 0;
        for(int i=0;i<m-1;i++) {
            if(matrix[n][i].compareTo(Fraction.ZERO) != 0) basisX = i;
        }
        basisX += 1;

        int[] xTable = new int[n];
        for(int i=0;i<n;i++) {
            xTable[i] = basisX + i;
        }



        return dsimplex(matrix, n, m, xTable);
    }

    public static void main(String[] args) throws IOException {
        Path path = Paths.get(file);
        Scanner in = new Scanner(path);
        int m = in.nextInt();
        int n = in.nextInt();

        Fraction[][] matrix = new Fraction[n+1][m];
        for(int i=0;i<n+1;i++) {
            for(int j=0;j<m;j++) {
                matrix[i][j] = new Fraction(in.nextInt());
            }
        }


        dsimplex(matrix, n, m);

    }
}
