package ru.avmo.fractions;

import java.util.ArrayList;

public class Fraction implements Comparable<Fraction> {
    private final long up, down;

    public static Fraction ZERO = new Fraction(0);
    public static Fraction MAX = new Fraction(Integer.MAX_VALUE);

    public Fraction(long up, long down) {
        Vector2i rounded = round(up, down);
        this.up = rounded.up;
        this.down = rounded.down;
    }

    public Fraction(long up) {
        this(up, 1);
    }

    private Fraction(Vector2i value) {
        this(value.up, value.down);
    }

    public Fraction subtract(Fraction fract) {
        return add(new Fraction(-fract.up, fract.down));
    }

    public Fraction divide(Fraction fract) {
        return multiply(new Fraction(fract.down, fract.up));
    }

    public Fraction abs() {
        return new Fraction(Math.abs(up), Math.abs(down));
    }

    public Fraction add(Fraction fract) {
        Vector2i value = new Vector2i(up, down);


        value.up = value.up*fract.down + fract.up * down;
        value.down *= fract.down;

        //value = round(value.up, value.down);
        return new Fraction(value);
    }

    public Fraction multiply(Fraction fract) {
        Vector2i value = new Vector2i(up*fract.up, down*fract.down);
        //value = round(value.up, value.down);
        return new Fraction(value);
    }

    public double toDouble() {
        if(up == 0 && down == 0) return 0;
        return ((double)up)/down;
    }

    public String toString() {
        if(down == 1) return Long.toString(up);
        if(up == 0) return "0";
        return up+"/"+down;
    }

    public boolean isNull() {
        if(up == 0) return true;
        return false;
    }

    public Fraction getMod() {
        if(compareTo(Fraction.ZERO)<0) return new Fraction(down+(up%down),down);
        return new Fraction(up%down,down);
    }

    private ArrayList<Long> getMult(long a) {
        ArrayList<Long> mA = new ArrayList<>();
        while(a>1) {
            long value = a;
            for(long i=2;i<a && i<Math.round(Math.pow(a,0.5)+1);i++) {
                if(a%i == 0) {
                    value = i;
                    break;
                }
            }

            mA.add(value);
            a /= value;
        }

        return mA;
    }

    private long getCommonMult(long a, long b) {
        ArrayList<Long> mA = getMult(a);
        ArrayList<Long> mB = getMult(b);

        long value = 1;

        for(long e : mA) {
            for(int i=0;i<mB.size();i++) {
                if(mB.get(i) == e) {
                    value *= e;
                    mB.remove(i);
                    break;
                }
            }
        }

        return value;
    }

    private Vector2i round(long u, long d) {
        Vector2i value = new Vector2i();

        if (u < 0 && d < 0) {
            u = -u;
            d = -d;
        }

        if(d < 0) {
            d = -d;
            u = -u;
        }

        long absUp = Math.abs(u);
        long absDown = Math.abs(d);

        long m = getCommonMult(absUp, absDown);

        if (m != 0) {
            u /= m;
            d /= m;
        }

        value.up = u;
        value.down = d;

        return value;
    }

    @Override
    public int compareTo(Fraction o) {
        /* TODO Пофиксить ошибку деления на ноль
        */

        if(o.up == 0) {
            if(up > 0) return 1;
            if(up < 0) return -1;
            if(up == 0) return 0;
        }

        if(up == 0) {
            if(o.up > 0) return -1;
            if(o.up < 0) return 1;
            if(o.up == 0) return 0;
        }

        Fraction comparing = this.divide(o);

        try {
            long digit = comparing.up / comparing.down;
            long mod = comparing.up % comparing.down;

            if(digit == 1 && mod == 0) return 0;
            if(digit >= 1) return 1;
            if(digit < 1) return -1;
        } catch (ArithmeticException e) {
            System.out.println("0 fails");
            System.out.printf("self: %d / %d \n", up, down);
            System.out.printf("other: %d / %d \n", o.up, o.down);
            System.out.printf("comparing: %d / %d \n", comparing.up, comparing.down);
            System.exit(0);
        }


        return 0;
    }
}
