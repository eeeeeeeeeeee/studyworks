package ru.avmo.fractions;

public class Vector2i {
    public long up, down;

    public Vector2i() {

    }

    public Vector2i(long up, long down) {
        this.up = up;
        this.down = down;
    }
}
