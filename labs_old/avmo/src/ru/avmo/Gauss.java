package ru.avmo;

import ru.avmo.fractions.Fraction;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class Gauss {
    // Путь до файла вводных данных
    private static final String file = "jg.txt";
    private static boolean mainCheck = true;
    private static boolean outText = false;

    private static void print(Fraction[][] matrix, int t) {
        if(!outText) return;

        System.out.println("\nResult of step "+(t+1)+": ");
        for(int i=0;i<matrix.length;i++) {
            for(int j=0;j<matrix[i].length;j++) {
                System.out.printf("%-9s ", matrix[i][j].toString());
            }
            System.out.println();
        }
        System.out.println();
    }

    public static ArrayList<Fraction> gauss(Fraction[][] matrix, int n, int m) {
        ArrayList<Fraction> result = new ArrayList<>();

        int stepName = 0;
        if(outText) System.out.print("Прямой ход:");

        // Прямой ход
        for(int t=0;t<m;t++) {
            for(int j=t+1;j<m;j++) {

                if(mainCheck) {
                    int index = t;

                    for(int i = t+1;i<m;i++) {
                        if(matrix[i][t].abs().compareTo(matrix[index][t].abs())>0) {
                            index = i;
                        }
                    }

                    Fraction[] temp = matrix[t];
                    matrix[t] = matrix[index];
                    matrix[index] = temp;
                }

                Fraction fctr = matrix[j][t].divide(matrix[t][t]);

                for(int k=t;k<n;k++) {
                    Fraction buf = matrix[t][k].multiply(fctr);
                    matrix[j][k] = matrix[j][k].subtract(buf);
                }
            }
            print(matrix, stepName++);
        }

        // Получение единиц по главной диагонали
        for(int t=0;t<m;t++) {
            Fraction fctr = matrix[t][t];
            for(int i=t;i<n;i++) {
                matrix[t][i] = matrix[t][i].divide(fctr);
            }
        }
        print(matrix, stepName++);

        // Обратный ход
        if(outText) System.out.print("Обратный ход:");

        for(int t=m-1;t>0;t--) {
            for(int j=t-1;j>=0;j--) {
                Fraction fctr = matrix[j][t];
                if(outText) System.out.println(fctr);

                for (int k = t; k < n; k++) {
                    Fraction buf = matrix[t][k].multiply(fctr);
                    matrix[j][k] = matrix[j][k].subtract(buf);
                }
            }
            print(matrix, stepName++);
        }


        // Результаты
        if(outText) System.out.println("Results:");
        int coutF = n-1 - m;

        if(outText) {
            for (int i = coutF; i > 0; i--) {
                System.out.printf("x%d is free\n", m + i);
            }
        }

        for(int i=m-1;i>=0;i--) {
            result.add(0, matrix[i][n-1]);
            if(outText) {
                System.out.printf("x%d = %s", i + 1, matrix[i][n - 1].toString());
                for (int j = 0; j < coutF; j++) {
                    System.out.printf(" - x%d * %s", j + m + 1, matrix[i][m + j]);
                }
                System.out.println();
            }
        }

        return result;
    }

    public static void main(String[] args) throws IOException {
        Path path = Paths.get(file);
        Scanner in = new Scanner(path);

        // Точность вычисления
        // (В текущей реализации не используется)
        int opti = in.nextInt();

        int n = in.nextInt();
        int m = in.nextInt();
        Fraction[][] matrix = new Fraction[m][n];

        for(int i=0;i<m;i++) {
            for(int j=0;j<n;j++) {
                matrix[i][j] = new Fraction(in.nextInt(), 1);
            }
        }

        in.close();

        ArrayList<Fraction> res = gauss(matrix, n, m);
        for(Fraction f : res) {
            System.out.println(f);
        }
    }
}

