package ru.avmo;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class Crc8 {
    private static final String file = "crc.txt";

    public static void main(String[] args) throws IOException {
        int c = 8;
        boolean[] register = new boolean[c];

        StringBuilder strBuilder = new StringBuilder();
        Files.lines(Paths.get(file), StandardCharsets.US_ASCII).forEach(strBuilder::append);
        String str = strBuilder.toString();

        int len = str.length()*4;
        System.out.println("Size: "+len);

        boolean[] file = new boolean[len+c];
        for(int i=0;i<str.length();i++) {
            int ch = Character.getNumericValue(str.charAt(i));
            int control = 0x08;
            for(int j=0;j<4;j++) {
                file[i*4+j] = ((ch&control)/control == 1);
                control >>= 1;
            }
        }

        for(int i=0;i<len+c;i++) {
            if(file[i]) {
                System.out.printf("1");
            } else
                System.out.printf("0");
        }

        System.out.println();

        for(int i=0;i<str.length();i++) {
            System.out.print(Character.getNumericValue(str.charAt(i)));
            System.out.print(" ");
        }
        System.out.println();

        boolean code = false;
        for(int i=0;i<len+c;i++) {
            code = register[0];
            register[0]=register[1];
            register[1]=register[2];
            register[2]=register[3]^code;
            register[3]=register[4]^code;
            register[4]=register[5];
            register[5]=register[6];
            register[6]=register[7];
            register[7]=file[i]^code;
        }

        System.out.print("CRC: ");
        for(int i=0;i<8;i++) {
            if(register[i]) {
                System.out.printf("1");
            } else
                System.out.printf("0");
        }
        System.out.println();

        /*for(int i=0;i<c;i++) {
            file[len+i]=register[i];
        }

        register = new boolean[c];
        code = false;
        for(int i=0;i<len+c;i++) {
            code = register[7];
            register[7]=register[6];
            register[6]=register[5];
            register[5]=register[4]^code;
            register[4]=register[3]^code;
            register[3]=register[2];
            register[2]=register[1];
            register[1]=register[0];
            register[0]=file[i]^code;
        }

        System.out.print("CRC: ");
        for(int i=0;i<8;i++) {
            if(register[i]) {
                System.out.printf("1");
            } else
                System.out.printf("0");
        }
        System.out.println();*/
    }

}
