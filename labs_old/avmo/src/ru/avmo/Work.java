package ru.avmo;

import ru.avmo.fractions.Fraction;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Scanner;

public class Work {
    // Путь до файла вводных данных
    private static final String file = "work.txt";

    private static void print(Fraction[][] matrix) {
        for(int i=0;i<matrix.length;i++) {
            for(int j=0;j<matrix[i].length;j++) {
                System.out.printf("%4s ",matrix[i][j].toString());
            }
            System.out.println();
        }
    }

    public static void main(String[] args) throws IOException {
        Path path = Paths.get(file);
        Scanner in = new Scanner(path);
        int m = in.nextInt();
        int n = in.nextInt();

        Fraction[][] matrix = new Fraction[n+2][m+n];

        // Список X-ов
        int[] xTable = new int[m];
        for(int i=0;i<m;i++) {
            xTable[i] = i+1;
        }

        // Правая часть матрицы
        for(int i=0;i<matrix.length;i++) {
            for(int j=0;j<matrix[i].length;j++) {
                matrix[i][j] = new Fraction(0);
            }
        }

        // Верхняя часть матрицы
        for(int i=0;i<n;i++) {
            for(int j=0;j<m;j++) {

                if(j==m-1) {
                    matrix[i][0] = new Fraction(in.nextInt());
                    continue;
                }

                matrix[i][j+1] = new Fraction(in.nextInt());

            }

            matrix[i][m+i] = new Fraction(1);
        }

        // Z - строка
        for(int i=0;i<m;i++) {
            matrix[n][i] = new Fraction(-in.nextInt());
        }

        // M - строка
        for(int i=0;i<m;i++) {
            for(int j=0;j<n;j++) {
                matrix[n+1][i] = matrix[n+1][i].subtract(matrix[j][i]);
            }
        }

        in.close();

        // F - строка
        Fraction fTable[] = matrix[n];

        // Основной цикл
        while(true) {
            print(matrix);
            System.out.println();

            boolean doAgain = false;

            for(int i=1;i<matrix[n].length;i++) {
                if(matrix[n+1][i].compareTo(Fraction.ZERO)<0 ||
                        (matrix[n+1][i].compareTo(Fraction.ZERO) == 0 && matrix[n][i].compareTo(Fraction.ZERO)<0)) {
                    doAgain = true;
                }
            }

            if(!doAgain) break;

            int maxIndex = 1;
            Fraction maxM = matrix[n+1][maxIndex].abs();
            Fraction maxZ = matrix[n][maxIndex].abs();

            for(int i=2;i<m;i++) {
                int comparingM = matrix[n+1][i].compareTo(Fraction.ZERO);
                if(comparingM > 0 || comparingM == 0 && matrix[n][i].compareTo(Fraction.ZERO) > 0) continue;
                if(matrix[n+1][i].abs().compareTo(maxM) > 0 ||
                        (matrix[n+1][i].abs().compareTo(maxM) == 0 && matrix[n][i].abs().compareTo(maxZ) > 0) ) {
                    maxIndex = i;
                    maxM = matrix[n+1][i].abs();
                    maxZ = matrix[n][i].abs();
                }
            }

            int minIndex = -1;
            Fraction minV = Fraction.MAX;

            for(int i=0;i<n;i++) {
                Fraction value = matrix[i][0].divide(matrix[i][maxIndex]);

                if(value.compareTo(Fraction.ZERO) < 0) continue;
                if(value.compareTo(minV)<0) {
                    minV = value;
                    minIndex = i;
                }
            }

            if(minIndex == -1) break;

            Fraction delimeter = matrix[minIndex][maxIndex];
            System.out.println(delimeter);

            Fraction[][] newMatrix = new Fraction[n+2][m+n];
            for(int i=0;i<matrix.length;i++) {
                for(int j=0;j<matrix[i].length;j++) {
                    if(i == minIndex) {
                        continue;
                    }

                    newMatrix[i][j] = matrix[i][j].subtract( matrix[minIndex][j].divide(delimeter).multiply(matrix[i][maxIndex]) );
                }
            }

            for(int i=0;i<matrix[minIndex].length;i++) {
                newMatrix[minIndex][i] = matrix[minIndex][i].divide(delimeter);
            }

            matrix = newMatrix;
            xTable[minIndex] = maxIndex;

        }

        // Вывод результатов
        HashMap<Integer, Fraction> result = new HashMap<>();
        for(int i=1;i<n+2;i++) {
            result.put(i,new Fraction(0));
        }
        for(int i=0;i<n;i++) {
            result.put(xTable[i], matrix[i][0]);
        }

        System.out.printf("Z( ");
        for(int i=1;i<m;i++) {
            System.out.printf("%s ", result.get(i));
        }
        System.out.printf(") = ");

        Fraction resValue = Fraction.ZERO.subtract(fTable[0]);

        for(int i=1;i<m;i++) {
            System.out.printf(" %s*%s + ", result.get(i), Fraction.ZERO.subtract(fTable[i]));
            resValue = resValue.add(result.get(i).multiply(Fraction.ZERO.subtract(fTable[i])));
        }
        System.out.printf("%s = %s", fTable[0], resValue);

    }
}
