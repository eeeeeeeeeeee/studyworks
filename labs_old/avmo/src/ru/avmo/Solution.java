package ru.avmo;

import ru.avmo.fractions.Fraction;

public class Solution {
    public Fraction matrix[][];
    public int x[];
    public int log = 0;

    public Solution(Fraction[][] matrix, int[] x) {
        this.matrix = matrix;
        this.x = x;
    }

    public Solution(Fraction[][] matrix, int[] x, int log) {
        this.matrix = matrix;
        this.x = x;
        this.log = log;
    }
}
