package ru.avmo;

import ru.avmo.fractions.Fraction;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Gomory {
    // Путь до файла вводных данных
    private static final String file = "gomory.txt";

    private static void print(Fraction[][] matrix) {
        for(int i=0;i<matrix.length;i++) {
            for(int j=0;j<matrix[i].length;j++) {
                try {
                    System.out.printf("%4s ",matrix[i][j].toString());
                } catch (NullPointerException e) {
                    System.out.printf("---- ");
                    continue;
                }

            }
            System.out.println();
        }
    }

    public static Solution getNewMatrix(Fraction[][] matrix, int n, int m, int[] x, boolean change) {
        int index = 0;

        for(int i=1;i<n;i++) {
            if(matrix[i][m-1].getMod().compareTo(matrix[index][m-1].getMod())>0) {
                index = i;
            }
        }

        if(matrix[index][m-1].getMod().compareTo(Fraction.ZERO) == 0) return new Solution(matrix, x, 1);

        System.out.println("index "+index+"; X"+x[index]);

        Fraction[][] newMatrix = new Fraction[n+2][m+1];
        for(int i=0;i<n;i++) {
            for(int j=0;j<m-1;j++) {
                newMatrix[i][j] = matrix[i][j];
            }
        }

        for(int i=0;i<n;i++) {
            newMatrix[i][m-1] = new Fraction(0);
            newMatrix[i][m] = matrix[i][m-1];
        }

        for(int i=0;i<m-1;i++) {
            newMatrix[n][i] = Fraction.ZERO.subtract(matrix[index][i].getMod());
        }

        //newMatrix[n][m-2] = new Fraction(1);
        newMatrix[n][m-1] = new Fraction(1);
        newMatrix[n][m] = Fraction.ZERO.subtract(matrix[index][m-1].getMod());

        for(int i=0;i<m-1;i++) {
            newMatrix[n+1][i] = matrix[n][i];
        }

        newMatrix[n+1][m-1] = new Fraction(0);
        newMatrix[n+1][m] = matrix[n][m-1];

        n += 1;
        m += 1;

        if(change) {
            for (int i = 0; i < m; i++) {
                newMatrix[n][i] = Fraction.ZERO.subtract(newMatrix[n][i]);
            }
        }

        int xTable[] = new int[x.length+1];
        for(int i=0;i<xTable.length-1;i++) {
            xTable[i] = x[i];
        }
        xTable[xTable.length-1] = n+1;

        return new Solution(newMatrix, xTable);
    }

    public static void main(String[] args) throws IOException {
        Path path = Paths.get(file);
        Scanner in = new Scanner(path);
        int m = in.nextInt();
        int n = in.nextInt();

        int startM = m-1;
        int startN = n;

        Fraction[][] prematrix = new Fraction[n+1][m];
        for(int i=0;i<n+1;i++) {
            for(int j=0;j<m;j++) {
                prematrix[i][j] = new Fraction(in.nextInt());
            }
        }


        print(prematrix);

        Fraction[][] matrix = new Fraction[n+1][m+n];
        for(int i=0;i<=n;i++) {
            for(int j=0;j<m-1;j++) {
                matrix[i][j] = prematrix[i][j];
            }
        }

        for(int i=0;i<=n;i++) {
            matrix[i][m+n-1] = prematrix[i][m-1];
        }

        for(int i=0;i<=n;i++) {
            for(int j=m-1;j<m+n-1;j++) {
                if(j - m + 1 == i) {
                    matrix[i][j] = new Fraction(1);
                } else {
                    matrix[i][j] = new Fraction(0);
                }
            }
        }

        for(int i=0;i<m+n-1;i++) {
            matrix[n][i] = Fraction.ZERO.subtract(matrix[n][i]);
        }


        System.out.println();

        print(matrix);
        System.out.println();
        System.out.println("Simplex");

        m = n+m;

        Solution sol = Simplex.simplex(matrix, n, m);
        System.out.println("\nResult:");
        print(sol.matrix);



        sol = getNewMatrix(sol.matrix, n, m, sol.x, true);

        System.out.println("\nNew Matrix");
        print(sol.matrix);

        System.out.println("\n\nDual Simplex");

        sol = DualSimplex.dsimplex(sol.matrix,n+1,m+1,sol.x);
        System.out.println("\nResult:");
        print(sol.matrix);

        n++;
        m++;

        while (true) {
            sol = getNewMatrix(sol.matrix, n++, m++, sol.x, false);

            if(sol.log>0) {
                System.out.println("Finished");

                break;
            }

            System.out.println("\nNew Matrix");
            print(sol.matrix);

            System.out.println("Dual Simplex: ");
            sol = DualSimplex.dsimplex(sol.matrix,n,m,sol.x);

            if(sol == null) {
                System.out.println("dsada");
                break;
            }

            sol = getNewMatrix(sol.matrix, n++, m++, sol.x, false);

            if(sol.log>0) {
                System.out.println("Finished");

                break;
            }

            System.out.println("Simplex: ");
            sol = Simplex.simplex(sol.matrix,n,m,sol.x);
        }

        HashMap<Integer, Fraction> map = new HashMap<>();
        for(int i=0;i<sol.x.length;i++) {
            map.put(sol.x[i], sol.matrix[i][m-2]);
        }

        Fraction value = new Fraction(0);

        System.out.printf("F( ");
        for(int i=0;i<startM;i++) {
            System.out.printf("%s ", map.getOrDefault(i,Fraction.ZERO).toString());
            value = value.add(map.getOrDefault(i,Fraction.ZERO).multiply(matrix[startN][i]));
        }
        System.out.printf(") = ");
        System.out.println(Fraction.ZERO.subtract(value));

    }
}
