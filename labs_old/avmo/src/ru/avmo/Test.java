package ru.avmo;

import ru.avmo.fractions.Fraction;

public class Test {

    public static void main(String[] args) {
        Fraction a = new Fraction(15, 23);
        Fraction b = new Fraction(17, 23);
        System.out.println(b.compareTo(a));
    }
}
