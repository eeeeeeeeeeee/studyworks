### Защита информации
https://gitlab.com/eeeeeeeeeeee/studyworks/tree/master/labs7sem/encrypt
* Encrypt - общая библиотека
* ConsoleApp1 - PowMod
* ConsoleApp2 - Euclid
* ConsoleApp3 - Diffie–Hellman
* ConsoleApp4 - Baby-step
* ConsoleApp5 - Шифр Шамира
* ConsoleApp6 - Шифр Эль-Гамаля
* ConsoleApp7 - Шифр Вернама
* ConsoleApp8 - Шифр RSA
* ConsoleApp9 - Подпись Эль-Гамаля
* ConsoleApp10 - Подпись RSA
* ConsoleApp11 - Подпись ГОСТ
 
### Теория языков программирования
https://gitlab.com/eeeeeeeeeeee/studyworks/tree/master/labs7sem/plt/ConsoleApp1
* ContextFreeGrammar - Контекстно-свободные грамматики
* FinalStateMachine - Детерминированный конечный автомат
* PushdownAutomaton - Детерминированный конечный автомат с магазинной памятью

Курсовая работа
https://gitlab.com/eeeeeeeeeeee/studyworks/tree/master/labs7sem/plt/coursework

### Старые работы
https://gitlab.com/eeeeeeeeeeee/studyworks/tree/master/labs_old/